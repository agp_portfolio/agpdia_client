// @flow

import * as React from "react";
import { Formik } from "formik";
import { LoginPage as TablerLoginPage } from "tabler-react";
import { useHistory } from "react-router-dom";
import { Alert } from "tabler-react";
import fetchFunction from "../helpers/fetchFuction";

function LoginPage() {
  const [error, setError] = React.useState();
  const history = useHistory();

  const login = ({ email, password }) => {
    fetchData(email, password);
  };

  const fetchData = (email, password) => {
    setError();
    var [promise, abort] = fetchFunction("user/signin", "POST", {
      email: email,
      password: password,
    })
    promise.then((data) => {
      if (!data) {
        setError({ message: "El usuario y la contraseña no coinciden" });
      }else{
        sessionStorage.setItem("token", data.token);
        sessionStorage.setItem("language", data.language);
        sessionStorage.setItem("name", data.display_name);
        sessionStorage.setItem("avatar", data.avatar);
        sessionStorage.setItem("is_admin", false);
        var [promise, abort] = fetchFunction("user_infos/main_currency", "GET")
        promise.then((data) => {
          sessionStorage.setItem("main_currency_symbol", data.symbol);
          sessionStorage.setItem("main_currency_format", data.format);
        });
        history.push("/");
      }
      
    });
  };

  return (
    <>
      {error && <Alert type="login"> {error.message}</Alert>}
      <Formik
        initialValues={{
          email: "",
          password: "",
        }}
        validate={(values) => {
          // same as above, but feel free to move this into a class method now.
          let errors = {};
          if (!values.email) {
            errors.email = "Required";
          } else if (
            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
          ) {
            errors.email = "Invalid email address";
          }
          if (!values.password) {
            errors.password = "Required";
          }
          return errors;
        }}
        onSubmit={(
          values,
          { setSubmitting, setErrors /* setValues and other goodies */ }
        ) => {
          // This is launched when you push the login button
          login({ email: values.email, password: values.password });
        }}
        render={({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <TablerLoginPage
            onSubmit={handleSubmit}
            onChange={handleChange}
            onBlur={handleBlur}
            values={values}
            errors={errors}
            touched={touched}
          />
        )}
      />
    </>
  );
}

export default LoginPage;
