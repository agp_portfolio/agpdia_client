// @flow

import React from "react";
import { Redirect, useParams } from "react-router-dom";

import Infinite from "react-infinite";

import SiteWrapper from "../SiteWrapper.react";
import AccountFilter from "../components/accountGeneral/AccountFilter";
import DatePickerRow from "../components/DatePickerRow";
import ModalForm from "../components/ModalForm";
import { Container, Row, Col, Card, Button, ModalTitle } from "react-bootstrap";

//import { Card } from "tabler-react";
import processAccountRecordsList from "../helpers/processAccountRecordsList";
import fetchFunction from "../helpers/fetchFuction";

import AccountAccountCard from "../components/accountAccounts/AccountAccountCard";
import AccountAnalysisGraphs from "../components/accountAnalysis/graphs/AccountAnalysisGraphs";
import AccountRecordDate from "../components/accountRecords/AccountRecordDate";
import AccountRecordTotal from "../components/accountRecords/AccountRecordTotal";
import AccountRecordForm from "../components/accountRecords/AccountRecordForm";
import AccountRecordsList from "../components/accountRecords/AccountRecordsList";

import backButtonWithModal from "../helpers/backButtonWithModal";
import moment from "moment";

function AccountAccountsDetails() {
  const [error, setError] = React.useState({});
  const [modalTitle, setModalTitle] = React.useState("");
  const [records, setRecords] = React.useState(false);
  const [recordsData, setRecordsData] = React.useState();
  const [concepts, setConcepts] = React.useState();
  const [conceptsInfo, setConceptsInfo] = React.useState();
  const [accounts, setAccounts] = React.useState();
  const [vehicles, setVehicles] = React.useState();
  const [houses, setHouses] = React.useState();
  const [selectedItems, setSelectedItem] = React.useState([]);
  const [itemsToForm, setItemsToForm] = React.useState([]);
  const [allRecords, setAllRecords] = React.useState([]);
  const [section, setSection] = React.useState("graphs");
  const [modalType, setModalType] = React.useState();
  const [currencies, setCurrencies] = React.useState();
  const { id } = useParams();
  const [update, setUpdate] = React.useState(false);

  var modalRef = React.useRef();

  const updateSignal = () => {
    setUpdate(!update);
  };

  const fetchCurrencies = () => {
    var [promise, abort] = fetchFunction("currencies/", "GET")
    promise.then((data) => {
      if (!data) {
        // No action
      } else {
        setCurrencies(data);
      }
    });
  };

  const changeSection = (section) => {
    setSection(section);
  };

  React.useEffect(() => {
    if (!currencies) {
      fetchCurrencies();
    }
  }, []);

  return (
    <SiteWrapper>
      <Container>
        <br></br>
        <Row>
          <Col lg={12}>
            <AccountAccountCard
              props={{
                id: id,
                update: update,
                currencies: currencies,
                section: section,
              }}
              updateSignal={updateSignal}
              changeSection={changeSection}
            />
          </Col>
        </Row>
        <Row>
          <Col lg={12}>
            {section == "graphs" && (
              <AccountAnalysisGraphs
              props={{
                hasDateFilter: true,
                update: update,
                filter: {
                  start_date: undefined,
                  end_date: undefined,
                  concepts:undefined,
                  accounts: [id],
                  types: undefined,
                  note: undefined,
                  groups: undefined,
                  subgroups: undefined,
                  vehicles: undefined,
                  houses: undefined,
                  stock_graph: undefined,
                  account_graph: true,
                },
              }}
            />
            )}
            <br></br>
            {section == "records" && (
              <AccountRecordsList
                props={{ filter: { accounts: [id] }, update: update }}
                updateSignal={updateSignal}
              />
            )}
          </Col>
        </Row>
      </Container>
    </SiteWrapper>
  );
}

export default AccountAccountsDetails;
