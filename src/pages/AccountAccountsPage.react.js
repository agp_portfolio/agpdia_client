// @flow

import React from "react";

import SiteWrapper from "../SiteWrapper.react";
import ModalForm from "../components/ModalForm";
import { Container, Row, Col, Card, Button, ModalTitle } from "react-bootstrap";
//import { Card } from "tabler-react";
import fetchFunction from "../helpers/fetchFuction";

import AccountAccount from "../components/accountAccounts/AccountAccount";
import AccountAccountTotal from "../components/accountAccounts/AccountAccountTotal";
import Infinite from "react-infinite";

function AccountAccounts() {
  const [error, setError] = React.useState({});
  const [accounts, setAccounts] = React.useState();
  const [currencies, setCurrencies] = React.useState();
  const [totalAmount, setTotalAmount] = React.useState();

  const fetchAccounts = () => {
    var [promise, abort] = fetchFunction("accounts/aggs/accounts/list", "POST")
    promise.then((data) => {
      if (!data) {
        setAccounts(
          <AccountAccountTotal
            key={new Date().getTime()}
            props={{
              amount: totalAmount,
              currencies: currencies,
            }}
            refetchAccounts={refetchAccounts}
          />
        );
      }else{
        var accountsTable = [];
          var total = 0;
          var total_all = 0;
          data.forEach((account) => {
            var blurred = true
            if(!["stock"].includes(account.statistics)){
              total += account.amount;
              blurred = false
            }
            total_all += account.amount;
            accountsTable.push(
              <AccountAccount key={account.id} props={account} blurred={blurred} />
            );
          });
          accountsTable.unshift(
            <AccountAccountTotal
              key={new Date().getTime()}
              props={{
                amount: total,
                amount_all: total_all,
                currencies: currencies,
              }}
              refetchAccounts={refetchAccounts}
            />
          );
          setTotalAmount(total);
          setAccounts(accountsTable);
      }
    });
  };

  const fetchCurrencies = () => {
    var [promise, abort] = fetchFunction("currencies/", "GET")
    promise.then((data) => {
      if (!data) {
        setCurrencies();
      }else{
        setCurrencies(data);
      }
    });
  };

  const refetchAccounts = () => {
    fetchAccounts();
  };

  React.useEffect(() => {
    // if (!accounts) {
    //   fetchAccounts();
    // }
    if (!currencies) {
      fetchCurrencies();
    }
  }, []);

  React.useEffect(() => {
    if (!currencies) return;
    if (!accounts) {
      fetchAccounts();
    }
  }, [currencies]);

  return (
    <SiteWrapper>
      <Container>
      <br></br>
        <Row>
          <Col lg={12}>
            {!accounts ? (
              <h1>Loading...</h1>
            ) : (
              <div>
                <Infinite
                  elementHeight={60} // The height of the tallest element in the list
                  useWindowAsScrollContainer
                >
                  {accounts}
                </Infinite>
              </div>
            )}
          </Col>
        </Row>
      </Container>
    </SiteWrapper>
  );
}

export default AccountAccounts;
