// @flow

import React from "react";

import Infinite from "react-infinite";

import SiteWrapper from "../SiteWrapper.react";
import InventoryItemFilter from "../components/inventoryItems/InventoryItemFilter";
import DatePickerRow from "../components/DatePickerRow";
import ModalForm from "../components/ModalForm";
import { Container, Row, Col, Card, Button, ModalTitle } from "react-bootstrap";

import InventoryItemsList from "../components/inventoryItems/InventoryItemsList";

import backButtonWithModal from "../helpers/backButtonWithModal";

function InventoryItems() {
  const [error, setError] = React.useState({});
  const [locationsFilter, setLocationsFilter] = React.useState();
  const [groupsFilter, setGroupsFilter] = React.useState();
  const [stateFilter, setStateFilter] = React.useState();
  const [noteFilter, setNoteFilter] = React.useState();

  var modalRef = React.useRef();

  const recordsFilterHandler = (locations, groups, state, note) => {
    setLocationsFilter(locations);
    setGroupsFilter(groups);
    setStateFilter(state);
    setNoteFilter(note);
  };

  return (
    <SiteWrapper>
      <Container>
      <br></br>
        <Row>
          <Col lg={3}>
            <InventoryItemFilter parentCallBack={recordsFilterHandler} />
          </Col>
          <Col lg={9}>
            <InventoryItemsList
              props={{
                filter: {
                  locations: locationsFilter,
                  groups: groupsFilter,
                  state: stateFilter,
                  note: noteFilter,
                },
              }}
            />
          </Col>
        </Row>
      </Container>
    </SiteWrapper>
  );
}

export default InventoryItems;
