// @flow

import React from "react";

import { Container, Row, Col, Card, Button, ModalTitle } from "react-bootstrap";
import TravelFilter from "../components/travelPlaces/TravelFilter";
import SiteWrapper from "../SiteWrapper.react";
import DatePickerRow from "../components/DatePickerRow";
import TravelMenuCard from "../components/travelPlaces/TravelMenuCard";
import TravelMap from "../components/travelPlaces/TravelMap";
import TravelRecordsList from "../components/travelPlaces/TravelRecordsList";
import fetchFunction from "../helpers/fetchFuction";

function Travel() {
  const [dateFilter, setDateFilter] = React.useState({});
  // const [section, setSection] = React.useState("worldMap");
  const [section, setSection] = React.useState("worldMap");

  const [continentsFilter, setContinentsFilter] = React.useState();
  const [countriesFilter, setCountriesFilter] = React.useState();
  // const [regionsFilter, setRegionsFilter] = React.useState();
  const [noteFilter, setNoteFilter] = React.useState();
  const [records, setRecords] = React.useState([]);
  const [continents, setContinents] = React.useState();
  const [countries, setCountries] = React.useState();
  const [regions, setRegions] = React.useState();
  // const [update, setUpdate] = React.useState(false); //Necesario unicamente si hay una lista debajo del mapa editable.

  // const updateSignal = () => {
  //   setUpdate(!update);
  // };

  const fetchTravels = (note) => {
    var filter = {
      start_date: dateFilter.start,
      end_date: dateFilter.end,
      // countryCode: country ? country : countriesFilter,
      // continentCode: continent ? continent :  continentsFilter,
      // regionCode: regionsFilter,
      note: note != undefined ? note : noteFilter,
    };

    var [promise, abort] = fetchFunction("travels/aggs/travels/list", "POST", filter)
    promise.then((data) => {
      if (!data) {
        setRecords([]);
      } else {
        // var continents = data.map(item => {return {name: item.continent, id: item.continentCode}}).filter((obj, pos, arr) => {
        //   return arr.map(mapObj => mapObj['id']).indexOf(obj['id']) === pos;
        // });
        // var countries = data.map(item => {return {name: item.country, id: item.countryCode}}).filter((obj, pos, arr) => {
        //   return arr.map(mapObj => mapObj['id']).indexOf(obj['id']) === pos;
        // });
        // var regions = data.map(item => {return {name: item.region, id: item.regionCode}}).filter((obj, pos, arr) => {
        //   return arr.map(mapObj => mapObj['id']).indexOf(obj['id']) === pos;
        // });
        setRecords(data);
      }
    });
  };
  const fetchContinents = () => {
    var [promise, abort] = fetchFunction("continents/", "GET")
    promise.then((data) => {
      if (!data) {
        setContinents();
      } else {
        var continentSelect = [] 
        data.forEach((continent) =>{
          continentSelect.push({
            label: continent.name,
            value: continent.code
          })
        })
        setContinents(continentSelect);
      }
    });
  };

  const fetchCountries = () => {
    var [promise, abort] = fetchFunction("countries/", "GET")
    promise.then((data) => {
      if (!data) {
        setCountries();
      } else {
        var countrySelect = [] 
        data.forEach((country) =>{
          countrySelect.push({
            parent: country.continent,
            label: country.name,
            value: country.code
          })
        })
        setCountries(countrySelect);
      }
    });
  };
  const fetchRegions = () => {
    var [promise, abort] = fetchFunction("regions/", "GET")
    promise.then((data) => {
      if (!data) {
        setRegions();
      } else {
        var regionSelect = [] 
        data.forEach((region) =>{
          regionSelect.push({
            parent: region.country,
            label: region.name,
            value: region.code
          })
        })
        setRegions(regionSelect);
      }
    });
  };
  const dateHandler = (startDate, endDate) => {
    setDateFilter({
      start: new Date(startDate).setHours(0, 0, 0, 0),
      end: new Date(endDate).setHours(23, 59, 59, 999),
    });
  };

  const travelFilterHandler = (
    // continent,
    // country,
    // region,
    note,
  ) => {
    if(!dateFilter.start) return
    // setContinentsFilter(continent);
    // setCountriesFilter(country);
    // setRegionsFilter(region);
    setNoteFilter(note);
    fetchTravels(note);
  };

  const changeSection = (section) => {
    setSection(section);
  };

  React.useEffect(() => {
    fetchContinents();
    fetchCountries();
    fetchRegions();
  }, []);
  
  React.useEffect(() => {
    if(!dateFilter.start) return
    fetchTravels()
  }, [dateFilter]);
  return <SiteWrapper> 
    <Container>
        <br></br>
        <Row>
          <Col lg={3}>
            <Card>
              <Card.Body
                style={{
                  display: "flex",
                  padding: "10px",
                }}
              >
                <DatePickerRow parentCallBack={dateHandler} initialFilterDate={false}/>
              </Card.Body>
            </Card>
            <TravelFilter
              props={{
                continents: continents,
                countries: countries,
                // regions: regions,
              }}
              parentCallBack={travelFilterHandler}
            />
          </Col>
          <Col lg={9} className="detailsTable">
            <TravelMenuCard
              props={{
                section: section,
              }}
              changeSection={changeSection}
            />
            {["worldMap","europeMap","spainMap"].includes(
              section
            ) && (
              <TravelMap
                props={{
                  // update: update,
                  filter: {
                    start_date: dateFilter.start,
                    end_date: dateFilter.end,
                    // continents: continentsFilter,
                    // countries: countriesFilter,
                    // regions: regionsFilter,
                    note: noteFilter,
                  },
                  map: section,
                  records: records,
                }}
              />
            )}
            {["records"].includes(section) && (
              <TravelRecordsList
                props={{
                  // update: update,
                  filter: {
                    start_date: dateFilter.start,
                    end_date: dateFilter.end,
                    // continents: continentsFilter,
                    // countries: countriesFilter,
                    // regions: regionsFilter,
                    note: noteFilter,
                  },
                  records: records,
                  continents:continents,
                  countries:countries,
                  regions: regions,
                }}
                refetchTravels={()=>fetchTravels()}
              />
            )}
          </Col>
          <br></br>
        </Row>
      </Container>
  </SiteWrapper>;
}

export default Travel;
