import ForgotPasswordPage from "./ForgotPasswordPage.react";
import LoginPage from "./LoginPage.react";
import RegisterPage from "./RegisterPage.react";
import Empty from "./Empty.react";
import DashboardPage from "./DashboardPage.react";
import AccountAccountsPage from "./AccountAccountsPage.react";
import AccountAccountsDetailsPage from "./AccountAccountsDetailsPage.react";
import AccountAnalyticsPage from "./AccountAnalyticsPage.react";
import AccountDashboardPage from "./AccountDashboardPage.react";
import AccountRecordsPage from "./AccountRecordsPage.react";
import InventoryPage from "./InventoryPage.react";
import TravelPage from "./TravelPage.react";
import ConfigurationDropdownsAccountingCategoriesPage from "./ConfigurationDropdownsAccountingCategoriesPage.react";
import ConfigurationDropdownsAccountingConceptsPage from "./ConfigurationDropdownsAccountingConceptsPage.react";
import ConfigurationDropdownsInventoryGroupsPage from "./ConfigurationDropdownsInventoryGroupsPage.react";
import ConfigurationDropdownsInventoryLocationsPage from "./ConfigurationDropdownsInventoryLocationsPage.react";
import ConfigurationDropdownsInventoryInventoriesPage from "./ConfigurationDropdownsInventoryInventoriesPage.react";
import ConfigurationDropdownsHousesPage from "./ConfigurationDropdownsHousesPage.react";
import ConfigurationDropdownsVehiclesPage from "./ConfigurationDropdownsVehiclesPage.react";
import ConfigurationDropdownsAccountAlertsPage from "./ConfigurationDropdownsAccountAlertsPage.react"
import ProfilePage from "./ProfilePage.react";
import Error400 from "./400.react";
import Error401 from "./401.react";
import Error403 from "./403.react";
import Error404 from "./404.react";
import Error500 from "./500.react";
import Error503 from "./503.react";

export {
  ForgotPasswordPage,
  LoginPage,
  RegisterPage,
  Error400,
  Error401,
  Error403,
  Error404,
  Error500,
  Error503,
  Empty,
  DashboardPage,
  AccountAccountsPage,
  AccountAccountsDetailsPage,
  AccountAnalyticsPage,
  AccountDashboardPage,
  AccountRecordsPage,
  InventoryPage,
  TravelPage,
  ConfigurationDropdownsAccountingCategoriesPage,
  ConfigurationDropdownsAccountingConceptsPage,
  ConfigurationDropdownsHousesPage,
  ConfigurationDropdownsVehiclesPage,
  ConfigurationDropdownsInventoryGroupsPage,
  ConfigurationDropdownsInventoryLocationsPage,
  ConfigurationDropdownsInventoryInventoriesPage,
  ConfigurationDropdownsAccountAlertsPage,
  ProfilePage,
};
