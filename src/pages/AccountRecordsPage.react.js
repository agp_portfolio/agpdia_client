// @flow

import React from "react";

import Infinite from "react-infinite";

import SiteWrapper from "../SiteWrapper.react";
import AccountFilter from "../components/accountGeneral/AccountFilter";
import DatePickerRow from "../components/DatePickerRow";
import ModalForm from "../components/ModalForm";
import { Container, Row, Col, Card, Button, ModalTitle } from "react-bootstrap";
//import { Card } from "tabler-react";
import processAccountRecordsList from "../helpers/processAccountRecordsList";

import AccountRecord from "../components/accountRecords/AccountRecord";
import AccountRecordDate from "../components/accountRecords/AccountRecordDate";
import AccountRecordTotal from "../components/accountRecords/AccountRecordTotal";
import AccountRecordsList from "../components/accountRecords/AccountRecordsList";

import backButtonWithModal from "../helpers/backButtonWithModal";

function AccountRecords() {
  const [error, setError] = React.useState({});
  const [dateFilter, setDateFilter] = React.useState({});
  const [conceptsFilter, setConceptsFilter] = React.useState();
  const [accountsFilter, setAccountsFilter] = React.useState();
  const [typesFilter, setTypesFilter] = React.useState();
  const [noteFilter, setNoteFilter] = React.useState();
  const [groupsFilter, setGroupsFilter] = React.useState();
  const [subgroupsFilter, setSubgroupsFilter] = React.useState();
  const [vehiclesFilter, setVehiclesFilter] = React.useState();
  const [housesFilter, setHousesFilter] = React.useState();


  var modalRef = React.useRef();
  const updateSignal = () => {
    // setUpdate(!update);
  };
  const dateHandler = (startDate, endDate) => {
    setDateFilter({
      start: new Date(startDate).setHours(0, 0, 0, 0),
      end: new Date(endDate).setHours(23, 59, 59, 999),
    });
  };
  const recordsFilterHandler = (concepts, accounts, types, note, groups, subgroups, vehicles, houses) => {
    setConceptsFilter(concepts);
    setAccountsFilter(accounts);
    setTypesFilter(types);
    setNoteFilter(note);
    setGroupsFilter(groups);
    setSubgroupsFilter(subgroups);
    setVehiclesFilter(vehicles);
    setHousesFilter(houses);
  };

  return (
    <SiteWrapper>
      <Container>
        <br></br>
        <Row>
          <Col lg={3}>
            <Card>
              <Card.Body
                style={{
                  display: "flex",
                  padding: "10px",
                }}
              >
                <DatePickerRow parentCallBack={dateHandler} initialFilterDate={true}/>
              </Card.Body>
            </Card>
            <AccountFilter toShow={["groups","subgroups","types","concepts","accounts","note"]} parentCallBack={recordsFilterHandler} />
          </Col>
          <Col lg={9}>
            <AccountRecordsList
              props={{
                hasDateFilter:true,
                filter: {
                  start_date: dateFilter.start,
                  end_date: dateFilter.end,
                  concepts: conceptsFilter,
                  accounts: accountsFilter,
                  types: typesFilter,
                  note: noteFilter,
                  groups: groupsFilter,
                  subgroups: subgroupsFilter,
                  houses: vehiclesFilter,
                  houses: housesFilter,
                },
                toShow:["all"],
              }}
              updateSignal={updateSignal}
            />
          </Col>
        </Row>
      </Container>
    </SiteWrapper>
  );
}

export default AccountRecords;
