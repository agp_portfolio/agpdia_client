const texts = {
  conceptsLabel: { ES: "Conceptos", EN: "Concepts" },
  accountsLabel: { ES: "Cuentas", EN: "Accounts" },
  typeLabel: { ES: "Tipo", EN: "Type" },
  typeExpense: { ES: "Gasto", EN: "Expense" },
  typeIncome: { ES: "Ingreso", EN: "Income" },
  typeInfo: { ES: "Info", EN: "Info" },
  typeTransfer: { ES: "Transferencia", EN: "Transfer" },
  noteFilterLabel: { ES: "Filtro por nota", EN: "Note Filter" },
  noteFilterPlaceholder:{ ES: "Texto a buscar...", EN: "Text to search..." },
  records:{ ES: "Registros", EN: "Records" },
};

export default texts