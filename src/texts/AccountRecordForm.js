const texts = {
  typeSelectExpense: { ES: "Gasto", EN: "Expense" },
  typeSelectIncome: { ES: "Ingreso", EN: "Income" },
  typeSelectTransfer: { ES: "Transferencia", EN: "Transfer" },
  accountSelect: { ES: "Cuenta", EN: "Account" },
  vehicleSelect: { ES: "Vehículo", EN: "Vehicle" },
  houseSelect: { ES: "Vivienda", EN: "House" },
  amountInput: { ES: "Importe", EN: "Amount" },
  currencySelect: { ES: "Moneda", EN: "Currency" },
  conceptSelect: { ES: "Concepto", EN: "Concept" },
  categorySelect: { ES: "Categoría", EN: "Category" },
  consumptionSelect: { ES: "Consumo", EN: "Consumption" },
  milometerSelect: { ES: "Cuentakilómetros", EN: "Milometer" },
  dateSelect: { ES: "Fecha", EN: "Date" },
  noteTextArea: {ES:"Nota", EN: "Note"}
};

export default texts