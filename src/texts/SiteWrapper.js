const texts = {
    dashboardLink: { ES: "Panel de Control", EN: "Dashboard" },
    accountDropdown: { ES: "Cuenta", EN: "Account" },
    accountDashboardLink: { ES: "Panel de Control", EN: "Dashboard" },
    accountAcountaLink: { ES: "Cuentas", EN: "Accounts" },
    accountRecordsLink: { ES: "Registros", EN: "Records" },
    accountAnalyticsLink: { ES: "Análisis", EN: "Analytics" },
    inventoryDasboardLink: { ES: "Inventario", EN: "Inventory" },
    travelDasboardLink: { ES: "Diario de viaje", EN: "Travel journal" },
    profileLink: { ES: "Perfil", EN: "Profile" },
    configurationLink: { ES: "Configuración", EN: "Configuration" },
    signOutButton: { ES: "Cerrar sesion", EN: "Sign out" },
  };
  
  export default texts