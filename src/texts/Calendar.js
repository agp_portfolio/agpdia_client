const texts = {
  daysOfWeek: {
    ES: ["do", "lu", "ma", "mi", "ju", "vi", "sa"],
    EN: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
  },
  monthNames: {
    ES: [
      "Enero",
      "Febrero",
      "Marzo",
      "Abril",
      "Mayo",
      "Junio",
      "Julio",
      "Agosto",
      "Septiembre",
      "Octubre",
      "Noviembre",
      "Diciembre",
    ],
    EN: [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ],
  },
  days: {
    today: { ES: "Hoy", EN: "Today" },
    yesterday: { ES: "Ayer", EN: "Yesterday" },
  },
  separation: { ES: "de", EN: "of" },
  ranges: {
    last30d: { ES: "Últimos 30 días", EN: "Last 30 days" },
    thisWeek: { ES: "Esta semana", EN: "This week" },
    thisMonth: { ES: "Este mes", EN: "This month" },
    thisYear: { ES: "Este año", EN: "This year" },
    untilToday: { ES: "Hasta hoy", EN: "Until today" },
  },
  cancelButton: { ES: "Cancelar", EN: "Cancel" },
  applyButton: { ES: "Aceptar", EN: "Apply" },
  customRangeLabel: { ES: "Rango personalizado", EN: "Custonm Range" },
  weekDays: {ES: ["Do","Lu","Ma","Mi","Ju","Vi","Sá"] , EN: ["Sun.", "Mon.","Tues.","Wed.","Thurs.","Fri.","Sat."]}
};

export default texts;
