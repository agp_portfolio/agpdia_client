const dynamicColors = (arrayLength) => {
  var arrayColors = [];
  for (let i = 0; i < arrayLength; i++) {
    arrayColors.push([RGBColor(),RGBColor()]);
  }
  return arrayColors
  // var min = 70;
  // var max = 220;
  // var arrayColors = [];
  // var r = min;
  // var g = max;
  // var b = min;
  
  // if (arrayLength <= 6) {
  //   var escalon = (max - min);
  // } else {
  //   var escalon = Math.ceil((max - min) / (((arrayLength / 3) - 1) / 2));
  //   console.log(escalon)
  // }
  // for (let i = 0; i < arrayLength; i++) {
  //   if (!(r <= min) && (g >= max) && (b <= min)) {
  //     r -= escalon;
  //   } else if ((r <= min) && (g >= max) && !(b >= max)) {
  //     b += escalon;
  //   } else if ((r <= min) && !(g <= min) && (b >= max)) {
  //     g -= escalon;
  //   } else if (!(r >= max) && (g <= min) && (b >= max)) {
  //     r += escalon;
  //   } else if ((r >= max) && (g <= min) && !(b <= min)) {
  //     b -= escalon;
  //   } else if ((r >= max) && !(g >= max) && (b <= min)) {
  //     g += escalon;
  //   }
  //   arrayColors.push(["rgb(" + r + "," + g + "," + b + ", 0.6)", "rgb(" + r + "," + g + "," + b + ", 0.3)"]);
  // }
  // return arrayColors;
};

function numberGenerator(number){
	return (Math.random()*number).toFixed(0);
}

function RGBColor(){
	var color = "("+numberGenerator(255)+"," + numberGenerator(255) + "," + numberGenerator(255) +")";
	return "rgb" + color;
}

export default dynamicColors;
