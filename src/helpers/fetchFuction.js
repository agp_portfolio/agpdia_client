import Swal from "sweetalert2";

const fetchFunction = (particularUrl, method, body) => {
  const token = sessionStorage.getItem("token") || "";
  const controller = new AbortController();
  const { signal } = controller; // get signal

  const params = {
    method: method,
    mode: "cors",
    headers: {
      "Content-type": "application/json",
      Authorization: `Bearer ${token} `,
    },
    body: JSON.stringify(body),
    signal, //Para poder abortar la funcion
  };
  const serverUrl = window.location.href.split(':');
  var react_app_api_ip = process.env.REACT_APP_API_IP
  const react_app_api_port = process.env.REACT_APP_API_PORT 
  // if (environment.sameServerBackAndFront) {
    react_app_api_ip = serverUrl[1]
  // }
  const  react_app_api_url = serverUrl[0] + ':' + react_app_api_ip + ':' + react_app_api_port + "/api";

  const promise = fetch(react_app_api_url +`/${particularUrl}`, params)
    .then((response)=> {
      if(response.status == "404"){
        console.log('La consulta no ha devuelto datos')
        return false
      }else if(response.status == "400"){
        Swal.fire({
          text: `Ha habido un problema con la consulta`,
          icon: "error",
          timer: "2000",
        });
        
        response.json().then((data) => {console.log('Problemas con la consulta: ' + data.message)})

        return false
      }
      return response.json()
    })
    .catch((err) => {
      if(err.toString().includes('The user aborted a request.') || err.toString().includes('The operation was aborted.')){
        console.log("El usuario a abortado el request devido a que necesita uno más reciente");
      }else{
        Swal.fire({
          text: `Ha habido un problema con el servidor: ${err}`,
          icon: "error",
          // timer: "2000",
        });
        console.log("Problemas de conexion con es servidor: " + err);
      }
      

    });

  return [
    promise,
    controller.abort.bind(controller), // use bind to save controller context
  ];
};

export default fetchFunction;
