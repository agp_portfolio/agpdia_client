const numberFormater = (amount, decimals) => {
  if (amount == undefined) return;

  return amount
    .toFixed(decimals)
    .replace(".", ",")
    .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
};

export default numberFormater;
