import moment from "moment";

const dateDiff = (startdate, enddate, alreadyFormated) => {
  //define moments for the startdate and enddate
  var startdateMoment = moment(startdate);
  var enddateMoment = moment(enddate);

  if (startdateMoment.isValid() === true && enddateMoment.isValid() === true) {
    //getting the difference in years
    var years = enddateMoment.diff(startdateMoment, "years");

    //moment returns the total months between the two dates, subtracting the years
    var months = enddateMoment.diff(startdateMoment, "months") - years * 12;

    //to calculate the days, first get the previous month and then subtract it
    startdateMoment.add(years, "years").add(months, "months");
    var days = enddateMoment.diff(startdateMoment, "days");
    if (alreadyFormated) {
      return `${years ? `${years}A `:''}${months ? `${months}M `:''} ${days ? `${days}D`:''}`
    } else {
      return {
        years: years,
        months: months,
        days: days,
      };
    }
  } else {
    return undefined;
  }
};

export default dateDiff;
