import React from "react";
import AccountRecord from "../components/accountRecords/AccountRecord";
import AccountRecordDate from "../components/accountRecords/AccountRecordDate";
import AccountRecordTotal from "../components/accountRecords/AccountRecordTotal";

const processAccountRecordsList = (data, mainCurrency, p_recordsCheckBoxHandler,p_showModalHandler) => {
  const recordsCheckBoxHandler = (objectId, isChecked) => {
    p_recordsCheckBoxHandler(objectId, isChecked);
  };
  const showModalHandler = (objectId, toEdit) => {
    p_showModalHandler(objectId, toEdit)
  };

  var recordsTable = [];
  if (!data.userAccountRecords.length) {
    recordsTable.push(<AccountRecordTotal key={"ads"} />);
    return recordsTable;
  }
  const processedData = data.userAccountRecords.map((element) => {
    const accountRecordProps = {
      id: element.id,
      date: element.date,
      concept: element.concept.name,
      account: element.account.name,
      category: element.concept.category.name,
      note: element.note,
      amount: element.amount,
      format: element.account.currency.format,
      symbol: element.account.currency.info.symbol,
      isMain: element.account.currency.isMain,
      exchange: element.account.currency.exchange,
      mainFormat: mainCurrency.format,
      mainSymbol: mainCurrency.symbol,
    };
    return accountRecordProps;
  });

  var previousDate = null;
  var diarySum = 0;
  var totalSum = 0;
  var dateKey = 0;
  processedData.forEach((element) => {
    const date = new Date(+element.date).toDateString();
    if (previousDate && date !== previousDate) {
      recordsTable.push(
        <AccountRecordDate
          key={dateKey}
          props={{
            date: previousDate,
            amount: diarySum,
            mainFormat: mainCurrency.format,
            mainSymbol: mainCurrency.symbol,
          }}
        />
      );
      dateKey++;
      diarySum = 0;
    }
    previousDate = date;
    if (element.isMain) {
      diarySum += element.amount;
      totalSum += element.amount;
    } else {
      diarySum += element.amount * element.exchange;
      totalSum += element.amount * element.exchange;
    }

    recordsTable.push(
      <AccountRecord
        key={element.id}
        props={element}
        recordsCheckBoxHandler={recordsCheckBoxHandler}
        showModalHandler={showModalHandler}
      />
    );
  });
  recordsTable.push(
    <AccountRecordDate
      key={dateKey}
      props={{
        date: previousDate,
        amount: diarySum,
        mainFormat: mainCurrency.format,
        mainSymbol: mainCurrency.symbol,
      }}
    />
  );
  recordsTable.push(
    <AccountRecordTotal
      key={dateKey + 1}
      props={{
        amount: totalSum,
        mainFormat: mainCurrency.format,
        mainSymbol: mainCurrency.symbol,
      }}
    />
  );
  recordsTable.reverse();
  return recordsTable;
};

export default processAccountRecordsList;
