import { lang } from "moment";

const dateLanguage = (label, language) => {
  var temp;
  if (language == "ES") {
    temp = label
    .replace("Jan", "Ene")
    // .replace("Feb", "Feb")
    // .replace("Mar", "Mar")
    .replace("Apr", "Abr")
    // .replace("May", "May")
    // .replace("Jun", "Jun")
    // .replace("Jul", "Jul")
    .replace("Aug", "Ago")
    // .replace("Sep", "Sep")
    // .replace("Oct", "Oct")
    // .replace("Nov", "Nov")
    .replace("Dec", "Dic") 
  }

  return temp;
};


export default dateLanguage;
