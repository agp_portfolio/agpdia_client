// @flow

import * as React from "react";
import { NavLink, withRouter } from "react-router-dom";

import {
  Site,
  Nav,
  // Grid,
  // List,
  // Button,
  RouterContextProvider,
} from "tabler-react";

// import type { NotificationProps } from "tabler-react";

import { useHistory } from "react-router-dom";

import logoImg from "./img/logo.png";

import accountImg from "./img/account.jpg";
import travelImg from "./img/travel.png";
import texts from "./texts/SiteWrapper";
import fetchFunction from "./helpers/fetchFuction";
import dateFormater from "./helpers/dateFormater";

function importAll(r) {
  let images = {};
  r.keys().map((item, index) => {
    images[item.replace("./", "")] = r(item);
  });
  return images;
}
const images = importAll(
  require.context("./img/avatars", false, /\.(png|jpe?g|svg)$/)
);
// import inventoryImg from './img/inventory.jpg'
// import travelImg from './img/travel.png'

// type State = {|
//   notificationsObjects: Array < NotificationProps >,
// |};

// type subNavItem = {|
//   +value: string,
//   +to ?: string,
//   +icon ?: string,
//   +LinkComponent ?: React.ElementType,
//   +useExact ?: boolean,
// |};

// type navItem = {|
//   +value: string,
//   +to ?: string,
//   +icon ?: string,
//   +active ?: boolean,
//   +LinkComponent ?: React.ElementType,
//   +subItems ?: Array < subNavItem >,
//   +useExact ?: boolean,
// |};

function SiteWrapper(props) {
  const language = sessionStorage.getItem("language");
  const name = sessionStorage.getItem("name");
  const isAdmin = sessionStorage.getItem("is_admin");
  const avatar = sessionStorage.getItem("avatar");

  //const { to, staticContext, ...rest } = props;
  const [notificationsObjects, setNotificationsObjects] = React.useState([]);
  const [unreadCount, setUnreadCount] = React.useState(1);
  const history = useHistory();
  const [messages, setMessages] = React.useState();

  const markMessagesAsRead = () => {
    var [promise, abort] = fetchFunction("alerts/messages/", "PUT");
    // un PUT no devuelve data
  };
  const fetchMessages = () => {
    var [promise, abort] = fetchFunction("alerts/messages/", "GET");
    promise.then((data) => {
      if (!data) {
        // setMessages();
      } else {
        setMessages(data);
      }
    });
  };

  React.useEffect(() => {
    if (!messages) {
      fetchMessages();
    } else {
      var notifications = [];
      messages.forEach((message) => {
        notifications.push({
          unread: message.unread,
          avatarURL: message.section == "accounting" ? accountImg : travelImg,
          message: (
            <React.Fragment>
              <strong>
                {message.message.length > 15
                  ? message.message.substr(0, 13) + "..."
                  : message.message}
              </strong>
            </React.Fragment>
          ),
          time: `Aprox. ${dateFormater(message.message_date, "show")}`,
        });
      });
      setNotificationsObjects(notifications || []);
    }
  }, [messages]);
  React.useEffect(() => {
    const interval = setInterval(() => {
      fetchMessages();
    }, 60000);
    return () => clearInterval(interval);
  }, []);

  React.useEffect(() => {
    setUnreadCount(notificationsObjects.reduce((a, v) => a || v.unread, false));
  }, [notificationsObjects]);

  const singOut = () => {
    Object.keys(sessionStorage).forEach((element) => {
      sessionStorage.removeItem(element);
    });

    history.push("/login");
  };
  return (
    <Site.Wrapper
      headerProps={{
        href: "/dashboard",
        alt: "AGPdia",
        imageURL: logoImg,
        navItems: <Nav.Item type="div" className="d-none d-md-flex"></Nav.Item>,
        notificationsTray: notificationsObjects.length
          ? {
              notificationsObjects,
              markAllAsRead: () => {
                const notifications = notificationsObjects.map((v) => ({
                  ...v,
                  unread: false,
                }));
                markMessagesAsRead();
                setNotificationsObjects(notifications);
              },
              unread: unreadCount,
            }
          : false,
        accountDropdown: {
          avatarURL: images[avatar],
          name: name,
          description: isAdmin == "true" ? "Admin" : "User",
          options: [
            // {
            //   icon: "user",
            //   value: texts.profileLink[language],
            //   onClick: () => history.push("/profile"),
            // },
            {
              icon: "cogs",
              value: texts.configurationLink[language],
              onClick: () =>
                history.push("/configuration/dropdowns/accounting_categories"),
            },
            { isDivider: true },
            {
              icon: "sign-out",
              value: texts.signOutButton[language],
              onClick: () => singOut(),
            },
          ],
        },
      }}
      navProps={{
        itemsObjects: [
          {
            value: texts.dashboardLink[language],
            to: "/dashboard",
            icon: "image",
            // LinkComponent: withRouter(NavLink),
            useExact: true,
          },
          {
            value: texts.accountAcountaLink[language],
            to: "/account/accounts",
            icon: "credit-card",
            // LinkComponent: withRouter(NavLink),
            useExact: true,
          },
          {
            value: texts.accountRecordsLink[language],
            to: "/account/records",
            icon: "tasks",
            // LinkComponent: withRouter(NavLink),
            useExact: true,
          },
          {
            value: texts.accountAnalyticsLink[language],
            to: "/account/analytics",
            icon: "bar-chart",
            // LinkComponent: withRouter(NavLink),
            useExact: true,
          },
          // {
          //   value: texts.accountDropdown[language],
          //   icon: "credit-card",
          //   subItems: [
          //     {
          //       value: texts.accountDashboardLink[language],
          //       to: "/account/dashboard",
          //       // LinkComponent: withRouter(NavLink),
          //     },
          //     {
          //       value: texts.accountAcountaLink[language],
          //       to: "/account/accounts",
          //       // LinkComponent: withRouter(NavLink),
          //     },
          //     {
          //       value: texts.accountRecordsLink[language],
          //       to: "/account/records",
          //       // LinkComponent: withRouter(NavLink),
          //     },
          //     {
          //       value: texts.accountAnalyticsLink[language],
          //       to: "/account/analytics",
          //       // LinkComponent: withRouter(NavLink),
          //     },
          //   ],
          // },
          {
            value: texts.inventoryDasboardLink[language],
            to: "/inventory/dashboard",
            icon: "tags",
            // LinkComponent: withRouter(NavLink),
          },
          {
            value: texts.travelDasboardLink[language],
            to: "/travel/dashboard",
            icon: "map",
            // LinkComponent: withRouter(NavLink),
          },
        ],
      }}
      routerContextComponentType={withRouter(RouterContextProvider)}
      footerProps={
        {
          // copyright: (
          //   <React.Fragment>
          //     Copyright © 2019
          //     <a href="."> Adrián Gómez</a>.
          //     All rights reserved.
          //   </React.Fragment>
          // ),
        }
      }
    >
      {props.children}
    </Site.Wrapper>
  );
}

export default SiteWrapper;
