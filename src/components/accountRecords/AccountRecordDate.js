import React from "react";

import { Col, Row } from "react-bootstrap";
import currencyFormater from "../../helpers/currencyFormater";
import textsCalendar from "../../texts/Calendar";

const AccountRecordDate = ({ props }) => {
  const main_currency_symbol = sessionStorage.getItem("main_currency_symbol");
  const main_currency_format = sessionStorage.getItem("main_currency_format");
  const language = sessionStorage.getItem("language");
  const today = new Date();
  const yesterday = new Date(Date.now() - 86400000);
  const actualDate = new Date(props.date);
  const styleBig = {
    margin: "0.66rem",
    fontSize: "18px",
    fontWeight: "600",
    height: "30px",
    display: "flex",
    alignItems: "center",
  };
  const styleSmall = {
    margin: "0.66rem",
    fontSize: "16px",
    fontWeight: "600",
    height: "30px",
    display: "flex",
    alignItems: "center",
  };
  var formatedDate =
    "" + textsCalendar.weekDays[language][actualDate.getDay()] + ". ";

  if (new Date(props.date).toDateString() === today.toDateString()) {
    formatedDate += textsCalendar.days.today[language];
  } else if (new Date(props.date).toDateString() === yesterday.toDateString()) {
    formatedDate += textsCalendar.days.yesterday[language];
  } else {
    const day = actualDate.getDate();
    if (day < 10) {
      formatedDate += "0" + day;
    } else {
      formatedDate += day;
    }
    const month = actualDate.getMonth();
    var monthName = textsCalendar.monthNames[language][month];
    formatedDate += " " + textsCalendar.separation[language] + " " + monthName;
    const year = actualDate.getFullYear();
    if (today.getFullYear() !== year) {
      formatedDate += " (" + year + ")";
      // formatedDate += " " + textsCalendar.separation[language] + " " + year;
    }
    //formatedDate = actualDate
  }
  return (
    <>
      <Row style={styleBig} className="d-none d-md-flex" >
        <Col xs={8}>{formatedDate}</Col>
        <Col xs={4} style={{ textAlign: "right" }}>
          {currencyFormater(
            props.amount,
            main_currency_format,
            main_currency_symbol
          )}
        </Col>
      </Row>
      <Row style={styleSmall} className="d-md-none">
        <Col xs={8}>{formatedDate}</Col>
        <Col xs={4} style={{ textAlign: "right" }}>
          {currencyFormater(
            props.amount,
            main_currency_format,
            main_currency_symbol
          )}
        </Col>
      </Row>
    </>
  );
};

export default AccountRecordDate;
