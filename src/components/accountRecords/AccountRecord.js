import React from "react";

import { Card, Col } from "react-bootstrap";
import { Form } from "tabler-react";
import currencyFormater from "../../helpers/currencyFormater";

const AccountRecord = ({ props, recordsCheckBoxHandler, showModalHandler }) => {
  const main_currency_symbol = sessionStorage.getItem("main_currency_symbol");
  const main_currency_format = sessionStorage.getItem("main_currency_format");
  
  const cardStyle = { 
    height:'60px',
    marginBottom: "0.1rem", 
    borderLeft: "5px solid " + props.account_color
    
  };
  const switchStyle = {
    fontSize: "14px",
    display: "flex",
    alignItems: "center",
    //justifyContent: "center",
    fontWeight: "600",
    //textAlign: "left",
    padding: "0.75rem",
  };
  const conceptStyle = {
    fontSize: "14px",
    // display: "flex",
    alignItems: "center",
    //justifyContent: "center",
    fontWeight: "600",
    //textAlign: "left",
    padding: "0.75rem",
    wordBreak: "break-word",
    hyphens: "auto",
    overflow:'hidden',
    whiteSpace: 'nowrap', /* Don't forget this one */
    textOverflow: 'ellipsis',
  };
  const accountStyle = {
    fontSize: "12px",
    // display: "flex",
    alignItems: "center",
    //justifyContent: "center",
    //textAlign: "center",
    padding: "0.75rem",
    overflow:'hidden',
    whiteSpace: 'nowrap', /* Don't forget this one */
    textOverflow: 'ellipsis',
  };
  const categoryStyle = {
    fontSize: "12px",
    // display: "flex",
    alignItems: "center",
    //justifyContent: "center",
    textAlign: "center",
    padding: "0.75rem",
    overflow:'hidden',
    whiteSpace: 'nowrap', /* Don't forget this one */
    textOverflow: 'ellipsis',
  };
  const noteStyle = {
    fontSize: "12px",
    // display: "flex",
    alignItems: "center",
    padding: "0.75rem",
    overflow:'hidden',
    whiteSpace: 'nowrap', /* Don't forget this one */
    textOverflow: 'ellipsis',
    wordBreak: "break-word",
    hyphens: "auto",
    overflow:'hidden',
    whiteSpace: 'nowrap', /* Don't forget this one */
    textOverflow: 'ellipsis',
  };
  const amountStyle = {
    fontSize: "18px",
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0.75rem",
  };
  const handleChange = (e) => {
    recordsCheckBoxHandler(props.id, e.target.checked,props.transfer_oid);
  };
  const handleClick = (e) => {
    showModalHandler(props.id, false);
  };


  return (
    <Card style={cardStyle}>
      <Card.Body style={{ padding: "0px 10px", display: "flex" }}>
        <Col md={1} xs={2} style={switchStyle}>
          <Form.Switch
            name={props.id + "_switch"}
            value={props.id}
            onChange={handleChange}
            checked = {props.isChecked}
          />
        </Col>
        <Col
          md={11}
          xs={10}
          style={{ display: "flex", padding: "0px", cursor: "pointer", alignItems:"center"  }}
          onClick={handleClick}
        >
          <Col md={2} xs={6} style={conceptStyle} title={props.concept}>
            {props.concept}
          </Col>
          <Col md={2} className="d-none d-md-flex" style={accountStyle} title={props.account}>
            {props.account}
          </Col>
          <Col md={2} className="d-none d-md-flex" style={categoryStyle} title={props.category}>
            {props.category}
          </Col>
          <Col md={4} className="d-none d-md-flex" style={noteStyle}  title={props.note}>
            {props.note}
          </Col>
          {/* <Col md={1} className="d-none d-md-flex"></Col> */}
          <Col md={2} xs={6} style={amountStyle}>
            <div>
              <p style={{ marginBottom: "0px" }}>
                <strong
                  style={{
                    color:
                      props.amount > 0
                        ? "#5eba00"
                        : props.amount == 0
                        ? "#54c8ff"
                        : "#FF0000",
                  }}
                >
                  {currencyFormater(
                    props.amount,
                    props.currency_format,
                    props.currency_symbol
                  )}
                </strong>
              </p>
              {!props.has_main_currency && (
                <p style={{ marginBottom: "0px", fontSize: "12px" }}>
                  ~{" "}
                  {currencyFormater(
                    props.amount * props.exchange,
                    main_currency_format,
                    main_currency_symbol
                  )}
                </p>
              )}
            </div>
          </Col>
        </Col>
      </Card.Body>
    </Card>
  );
};

export default AccountRecord;
