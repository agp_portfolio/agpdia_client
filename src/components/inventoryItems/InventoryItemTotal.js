import React from "react";

import { Card, Col, Row } from "react-bootstrap";
import currencyFormater from "../../helpers/currencyFormater";
import { Form, Button } from "tabler-react";

import InventoryItemForm from "../inventoryItems/InventoryItemForm";
import ModalForm from "../ModalForm";
// import Swal from "sweetalert2";
import backButtonWithModal from "../../helpers/backButtonWithModal";

const InventoryItemTotal = ({
  props,
  refetchItems,
  showModalHandler,
  refetchAfterDelete,
}) => {
  const main_currency_symbol = sessionStorage.getItem("main_currency_symbol");
  const main_currency_format = sessionStorage.getItem("main_currency_format");

  var modalRef = React.useRef();

  const amountStyle = {
    display: "flex",
    alignItems: "center",
    fontSize: "18px",
    justifyContent: "center",
    // padding: "0.75rem",
    textAlign: "right",
  };
  const switchStyle = {
    fontSize: "14px",
    display: "flex",
    alignItems: "center",
    fontWeight: "600",
    padding: "0.75rem",
  };
  const infoStyle = {
    fontSize: "14px",
    // display: "flex",
    alignItems: "center",
    fontWeight: "600",
    padding: "0.25rem",
    textAlign: "center",
  };
  const selectedTextStyle = {
    fontSize: "14px",
    display: "flex",
    alignItems: "center",
    fontWeight: "600",
    padding: "0.25rem",
    textAlign: "center",
  };

  // const handleClick = (toAdd) => {
  //   backButtonWithModal.neutralizeBack(modalRef.current);
  //   modalRef.current.handleShow();
  //   modalRef.current.task('objectId', toAdd);
  // };

  const handleClick = (toAdd) => {
    showModalHandler(undefined, toAdd);
  };

  const modalSaveHandler = () => {
    modalRef.current.handleHide();
    refetchItems();
  };

  return (
    <Card
      className="item-record-total"
      style={{ marginBottom: "0.1rem", background: "ghostwhite" }}
    >
      <Card.Body style={{ padding: "0px 10px", display: "flex" }}>
        {/* <Col md={9} xs={4}>
          {props ? (
            <Row style={{ height: "100%" }}>
              <Col md={4} xs={6} style={switchStyle}></Col> 
              <Col md={8} xs={6} style={{ margin: "auto" }}>
                <Row style={{ height: "100%" }}>*/}
        <Col md={3} xs={0} style={infoStyle}></Col>
        <Col md={6} xs={7} style={infoStyle}>
          <Button
            pill
            outline
            color="primary"
            onClick={() => handleClick(true)}
          >
            Añadir
          </Button>
        </Col>
        {/* </Row>
              </Col>
            </Row>
          ) : (
            ""
          )}
        </Col> */}
        <Col md={3} xs={5} style={amountStyle}>
          <div style={{ width: "inherit" }}>
            {props && props.amount != undefined && props.elements ? (
              <strong>
                {currencyFormater(
                  props.amount,
                  main_currency_format,
                  main_currency_symbol
                )}
              </strong>
            ) : (
              <strong>No hay elementos</strong>
            )}
          </div>
        </Col>
      </Card.Body>
      {/* <ModalForm
        ref={modalRef}
        props={{
          title: 'Añadir',
        }}
      >
        <InventoryItemForm
          props={{
            objectId: undefined,
            currencies: props.currencies,
            inventories: props.inventories,
            locations: props.locations,
            groups: props.groups,
            modalType: 'Add',
          }}
          modalSaveHandler={modalSaveHandler}
        />
      </ModalForm> */}
    </Card>
  );
};

export default InventoryItemTotal;
