import React from "react";

import { Card, Col, Row } from "react-bootstrap";
import { Icon } from "tabler-react";
import { useHistory } from "react-router-dom";
import dateFormater from "../../helpers/dateFormater";
import currencyFormater from "../../helpers/currencyFormater";
import defaultImage from "../../img/inventories_images/default.png";
import Swal from "sweetalert2";
import fetchFunction from "../../helpers/fetchFuction";
import dateDiff from "../../helpers/dateDiff";

function importAll(r) {
  let images = {};
  r.keys().map((item, index) => {
    images[item.replace("./", "")] = r(item);
  });
  return images;
}
const images = importAll(
  require.context("../../img/accounts_images", false, /\.(png|jpe?g|svg)$/)
);

const InventoryItem = ({ props, refetchItems, showModalHandler }) => {
  const main_currency_symbol = sessionStorage.getItem("main_currency_symbol");
  const main_currency_format = sessionStorage.getItem("main_currency_format");
  const history = useHistory();
  var color;
  switch (props.state) {
    case "good":
      color = "green";
      break;
    case "regular":
      color = "orange";
      break;
    case "bad":
      color = "red";
      break;
    case "disposed":
      color = "gray";
      break;

    default:
      break;
  }
  const cardStyle = {
    marginBottom: "0.1rem",
    borderLeft: "5px solid " + color,
    color: props.state == "disposed" && "#80808094",
  };
  const nameStyle = {
    fontSize: "18px",
    // display: "flex",
    alignItems: "center",
    //justifyContent: "center",
    fontWeight: "600",
    //textAlign: "left",
    // paddingTop: "1rem",
    wordBreak: "break-word",
    hyphens: "auto",
    overflow: "hidden",
    whiteSpace: "nowrap" /* Don't forget this one */,
    textOverflow: "ellipsis",
  };
  const inventoryStyle = {
    fontSize: "15px",
    // display: "flex",
    alignItems: "center",
    //justifyContent: "center",
    fontWeight: "600",
    //textAlign: "left",
    // paddingTop: "0.75rem",
    wordBreak: "break-word",
    hyphens: "auto",
  };
  const symbolStyle = {
    fontWeight: "bold",
    display: "flex",
    alignItems: "center",
    height: "200px",
    // padding: "0.75rem",
  };
  const otherStyle = {
    fontSize: "14px",
    // display: "flex",
    alignItems: "center",
    //justifyContent: "center",
    // fontWeight: "600",
    //textAlign: "left",
    // paddingTop: "0.75rem",
    wordBreak: "break-word",
    hyphens: "auto",
    overflow: "hidden",
    whiteSpace: "nowrap" /* Don't forget this one */,
    textOverflow: "ellipsis",
  };

  const dateStyle = {
    fontSize: "14px",
    // display: "flex",
    alignItems: "center",
    //justifyContent: "center",
    // fontWeight: "600",
    //textAlign: "left",
    paddingTop: "1rem",
    // wordBreak: "break-word",
    // hyphens: "auto",
    flexDirection:"column",
  };
  const noteStyle = {
    fontSize: "14px",
    // display: "flex",
    alignItems: "center",
    //justifyContent: "center",
    // fontWeight: "600",
    //textAlign: "left",
    paddingBottom: "1rem",
    wordBreak: "break-word",
    hyphens: "auto",
    overflow: "hidden",
    whiteSpace: "nowrap" /* Don't forget this one */,
    textOverflow: "ellipsis",
  };

  const amountStyle = {
    fontSize: "15px",
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    // padding: "0.75rem",
    // marginTop: "35px",
  };
  const topIconStyle = {
    textAlign: "end",
    marginTop: "1rem",
  };
  const leftIconStyle = {
    textAlign: "start",
    marginTop: "1rem",

  };
  const handleClick = (e) => {
    showModalHandler(props.id, false);
  };

  const deleteItem = () => {
    Swal.fire({
      title: "¿Estas seguro de que proceder con la eliminación?",
      // text: `${props.selected.length} ${
      //   props.selected.length == 1 ? " registro" : " registros"
      // } a eliminar`,
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, !Borrar!",
    }).then((result) => {
      if (!result.value) return;
      var [promise, abort] = fetchFunction(
        "inventories/items/" + props.id,
        "DELETE"
      );
      promise.then((data) => {
        if (!data) {
          Swal.fire({
            text: `No se han encontrado los elementos a eliminar o estan en uso`,
            icon: "error",
            timer: "2000",
          });
        } else {
          Swal.fire({
            text: `El elementos se ha eliminado`,
            icon: "success",
            timer: "1000",
          });
          refetchItems();
        }
      });
    });
  };

  return (
    <Col md={6}>
      <Row>
        <Card style={cardStyle}>
          <Card.Body style={{ padding: "0px 10px", display: "flex" }}>
            <Col md={12} xs={12} onClick={handleClick}>
              <Row style={{flexWrap: "nowrap"}}>
              <Col xs={1} md={2} style={leftIconStyle}>
                  <Icon
                    name="copy"
                    onClick={(e) => {
                      e.stopPropagation();
                      showModalHandler(props.id, true);
                    }}
                  />
                </Col>
                <Col
                  xs={10} md={8}
                  className="d-flex m-auto"
                  style={dateStyle}
                >
                  {dateFormater(props.acquisition_date, "show")}
                  {props.dispose_date
                    ? "  →  " +
                      dateFormater(props.dispose_date, "show") +
                      ` (~ ${dateDiff(
                        props.acquisition_date,
                        props.dispose_date,
                        true
                      )})`
                    : ` (~ ${dateDiff(
                        props.acquisition_date,
                        new Date(),
                        true
                      )})`}
                </Col>
                <Col xs={1} md={2} style={topIconStyle}>
                  <Icon
                    name="times"
                    onClick={(e) => {
                      e.stopPropagation();
                      deleteItem();
                    }}
                  />
                </Col>
              </Row>
              <br/>
              <Row>
                <Col md={12} style={symbolStyle}>
                  {props.image ? (
                    <img
                      src={props.image}
                      style={{
                        margin: "auto",
                        maxHeight: "200px",
                        // width: "500px",
                        objectFit: "contain",
                        opacity: props.state == "disposed" && "0.4",
                      }}
                    />
                  ) : (
                    <img
                      src={defaultImage}
                      style={{
                        margin: "auto",
                        maxHeight: "200px",
                        // width: "500px",
                        objectFit: "contain",
                        opacity: props.state == "disposed" && "0.4",
                      }}
                    />
                  )}
                </Col>

                {/* <Col
                      md={1}
                      xs={0}
                      className="d-none d-md-flex"
                      style={iconStyle}
                    >
                      {props.has_invoice && <Icon name="paperclip" />}
                    </Col> */}
              </Row>
              <br/>
              <Row>
                <Col md={9} style={nameStyle} title={props.name}>
                  {props.name}
                </Col>
                <Col md={3} className="d-none d-md-flex" style={amountStyle}>
                  <div>
                    <p style={{ marginBottom: "0px" }}>
                      <strong>
                        {currencyFormater(
                          props.acquisition_price,
                          props.currency_format,
                          props.currency_symbol
                        )}{" "}
                      </strong>
                    </p>
                    {!props.has_main_currency && (
                      <p style={{ marginBottom: "0px", fontSize: "12px" }}>
                        ~{" "}
                        {currencyFormater(
                          props.initial_amount * props.exchange,
                          main_currency_format,
                          main_currency_symbol
                        )}
                      </p>
                    )}
                  </div>
                </Col>
              </Row>

              <Row>
                <Col md={7} style={inventoryStyle} title={props.inventory}>
                  {props.inventory ? (
                    "Inv. " + props.inventory
                  ) : (
                    <span>&nbsp;&nbsp;</span>
                  )}
                </Col>
                <Col md={5} style={otherStyle} title={props.location}>
                  {props.location ? (
                    "Loc. " + props.location
                  ) : (
                    <span>&nbsp;&nbsp;</span>
                  )}
                </Col>
              </Row>
              <Row>
                <Col md={7} style={otherStyle} title={props.brand}>
                  {props.brand ? (
                    "Mrc. " + props.brand
                  ) : (
                    <span>&nbsp;&nbsp;</span>
                  )}
                </Col>
                <Col md={5} style={otherStyle} title={props.acquisition_place}>
                  {props.acquisition_place ? (
                    "Lug. " + props.acquisition_place
                  ) : (
                    <span>&nbsp;&nbsp;</span>
                  )}
                </Col>
              </Row>
              <Row>
                <Col md={7} style={otherStyle} title={props.identifier}>
                  {props.identifier ? (
                    "Id. " + props.identifier
                  ) : (
                    <span>&nbsp;&nbsp;</span>
                  )}
                </Col>
                <Col md={5} style={otherStyle} title={props.group}>
                  {props.group ? (
                    "Grp. " + props.group
                  ) : (
                    <span>&nbsp;&nbsp;</span>
                  )}
                </Col>
              </Row>
              <Row>
                <Col md={12} style={noteStyle} title={props.note}>
                  {props.note ? "Nt. " + props.note : <span>&nbsp;&nbsp;</span>}
                </Col>
                
              </Row>
            </Col>
          </Card.Body>
        </Card>
      </Row>
    </Col>
  );
};

export default InventoryItem;
