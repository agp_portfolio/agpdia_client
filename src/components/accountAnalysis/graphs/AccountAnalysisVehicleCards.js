// @flow

import React, { useEffect } from "react";
import fetchFunction from "../../../helpers/fetchFuction";
import { Col, Row, Card } from "react-bootstrap";
import AccountDetailsCard from "../AccountAnalysisCard";
import { Doughnut, Line, Bar } from "react-chartjs-2";
import currencyFormater from "../../../helpers/currencyFormater";
import numberFormater from "../../../helpers/numberFormater";
import { Form, Button } from "tabler-react";
import Select from "react-select";
import moment from "moment";

import { StampCard } from "tabler-react";

function AccountAnalytics({ props, updateSignal }) {
  const language = sessionStorage.getItem("language");
  const [data, setData] = React.useState([]);
  const [columns, setColumns] = React.useState([]);
  const [initialExpanded, setInitialExpanded] = React.useState();
  const [prevProps, setPrevProps] = React.useState({});
  const [firstRender, setFirstRender] = React.useState(false);
  const [cardData, setCardData] = React.useState();
  const [dateGroup, setDateGroup] = React.useState("month");
  const [groupedBy, setGroupedBy] = React.useState("account_id");
  const [datasets, setDatasets] = React.useState([]);
  const [chartVisible, setChartVisible] = React.useState(false);
  const cardStyle = {
    marginBottom: "0.1rem",
    // borderLeft: "5px solid " + graphData.color,
  };
  React.useEffect(() => {
    if (!firstRender) return;
    if (JSON.stringify(props) == JSON.stringify(prevProps)) return;
    if (props.hasDateFilter && !props.filter.start_date && !props.filter.year)
      return;
    setPrevProps(props);
  }, [props]);

  React.useEffect(() => {
    // console.log(props);
    if (JSON.stringify(prevProps) == JSON.stringify({})) return;
    fetchData();
  }, [prevProps]);

  React.useEffect(() => {
    setFirstRender(true);
  }, []);

  const fetchData = () => {
    var filter = {
      start_date: props.filter.start_date,
      end_date: props.filter.end_date,
      concepts: props.filter.concepts,
      accounts: props.filter.accounts,
      type: props.filter.types,
      note: props.filter.note,
      group: props.filter.groups,
      subgroup: props.filter.subgroups,
      vehicles: props.filter.vehicles,
      grouped_by: groupedBy, //account_id/category_id/concept_id
      date_group: dateGroup, //day/month/year
      stock_graph: props.filter.stock_graph, //day/month/year
    };

    // setRecords(false);
    var [promise, abort] = fetchFunction("accounts/aggs/analysis/vehicleCards", "POST", filter)
    promise.then((data) => {
      if (!data) {
        setCardData();
      } else {
        /* Need to destroy (hide in our case) before create (change data for us) to prevent 
        ERROR TypeError: Cannot read property '¿?' of undefined
        at ChartElement.getPixelForValue*/
        setCardData(data);
      }
    });
  };

  return (
    <>
      {window.innerWidth > 500 && (
        <>
          <Row cards={true}>
            <Col xs={12} sm={12} lg={12}>
              <br></br>
              <p style={{textAlign: 'center', fontWeight:'bold'}}>Estos paneles resumen solo atienden al filtro de vehículos, ni tiempo ni categorias.</p>
            </Col>
            <Col xs={12} sm={4} lg={4}>
              <StampCard
                color="purple"
                icon="car"
                header={
                  <a href="#">
                    {cardData && numberFormater(cardData.kilometer_price, 2)} €{" "}
                    <small>/ km</small>
                  </a>
                }
                footer="Precio Km"
              />
            </Col>
            <Col xs={12} sm={4} lg={4}>
              <StampCard
                color="blue"
                icon="tint"
                header={
                  <a href="#">
                    {cardData && numberFormater(cardData.consumption, 2)} l{" "}
                    <small>/ 100km</small>
                  </a>
                }
                footer="Consumo"
              />
            </Col>
            <Col xs={12} sm={4} lg={4}>
              <StampCard
                color="yellow"
                icon="tachometer"
                header={
                  <a href="#">
                    {cardData && numberFormater(cardData.total_kilometers, 0)}{" "}
                    km
                  </a>
                }
                footer="Km recorridos"
              />
            </Col>
            <Col xs={12} sm={4} lg={4}>
              <StampCard
                color="red"
                icon="eur"
                header={
                  <a href="#">
                    {cardData && numberFormater(cardData.total_amount, 2)} €
                  </a>
                }
                footer="Coste total"
              />
            </Col>
            <Col xs={12} sm={4} lg={4}>
              <StampCard
                color="grey"
                icon="wrench"
                header={
                  <a href="#">
                    {cardData && numberFormater(cardData.total_amount_mtto, 2)}{" "}
                    €
                  </a>
                }
                footer="Coste combustible y mtto"
              />
            </Col>
            <Col xs={12} sm={4} lg={4}>
              <StampCard
                color="orange"
                icon="key"
                header={
                  <a href="#">
                    {cardData && numberFormater(cardData.total_amount_pay, 2)} €
                  </a>
                }
                footer="Financiación"
              />
            </Col>
          </Row>
        </>
      )}
    </>
  );
}

export default AccountAnalytics;
