// @flow

import React, { useEffect } from "react";
import Table from "../Table";
import fetchFunction from "../../../helpers/fetchFuction";
import { Col, Row, Card } from "react-bootstrap";
import AccountDetailsCard from "../AccountAnalysisCard";
import { Doughnut, Line, Bar } from "react-chartjs-2";
import Highcharts, { map } from "highcharts";
import HighchartsReact from "highcharts-react-official";
import currencyFormater from "../../../helpers/currencyFormater";
import dateLanguage from "../../../helpers/dateLanguage";
import colorsArray from "../../../helpers/colorsArray";
import { Form, Button } from "tabler-react";
import Select from "react-select";
import moment from "moment";
require("highcharts/modules/exporting")(Highcharts);
require("highcharts/modules/export-data")(Highcharts);

function AccountAnalytics({ props, updateSignal, parentCallBack }) {
  const language = sessionStorage.getItem("language");
  const [data, setData] = React.useState([]);
  const [columns, setColumns] = React.useState([]);
  const [initialExpanded, setInitialExpanded] = React.useState();
  const [prevProps, setPrevProps] = React.useState({});
  const [firstRender, setFirstRender] = React.useState(false);
  const [graphData, setGraphData] = React.useState();
  const [dateGroup, setDateGroup] = React.useState("month");
  const [groupedBy, setGroupedBy] = React.useState("concept_id");
  const [datasets, setDatasets] = React.useState([]);
  const [chartVisible, setChartVisible] = React.useState(false);
  const [invoiceType, setSetInvoiceType] = React.useState();
  Highcharts.setOptions({
    lang: {
      thousandsSep: ".",
      decimalPoint: ",",
      months: [
        "Enero",
        "Febrero",
        "Marzo",
        "Abril",
        "Mayo",
        "Junio",
        "Julio",
        "Agosto",
        "Septiembre",
        "Octubre",
        "Noviembre",
        "Diciembre",
      ],
      weekdays: [
        "Domingo",
        "Lunes",
        "Martes",
        "Miércoles",
        "Jueves",
        "Viernes",
        "Sábado",
      ],
      shortMonths: [
        "Ene",
        "Feb",
        "Mar",
        "Abr",
        "May",
        "Jun",
        "Jul",
        "Ago",
        "Sep",
        "Oct",
        "Nov",
        "Dic",
      ],
      resetZoom: "Reiniciar zoom",
    },
  });
  const cardStyle = {
    marginBottom: "0.1rem",
    // borderLeft: "5px solid " + graphData.color,
  };
  React.useEffect(() => {
    // if (!firstRender) return;
    if (JSON.stringify(props) == JSON.stringify(prevProps)) return;
    if (props.hasDateFilter && !props.filter.start_date && !props.filter.year)
      return;
    setPrevProps(props);
  }, [props]);

  React.useEffect(() => {
    // console.log(props)
    if (!firstRender) return;
    if (JSON.stringify(prevProps) == JSON.stringify({})) return;
    // fetchGraph();
  }, [prevProps]);

  const fetchGraph = () => {
    var start_date = props.filter.start_date;
    var end_date = props.filter.end_date;

    if (props.filter.account_graph) {
      switch (dateGroup) {
        case "day":
          start_date = moment().subtract(1, "month");
          break;
        case "month":
          start_date = moment().subtract(1, "year");
          break;
        case "year":
          start_date = moment(new Date("2015-01-01"));
          break;
      }
      end_date = moment();
    }
    var filter = {
      start_date: start_date,
      end_date: end_date,
      concepts: props.filter.concepts,
      accounts: props.filter.accounts,
      type: props.filter.types,
      note: props.filter.note,
      group: props.filter.groups,
      subgroup: props.filter.subgroups,
      grouped_by: groupedBy, //account_id/category_id/concept_id
      date_group: dateGroup, //day/month/year
      stock_graph: props.filter.stock_graph, //day/month/year
    };
    // setRecords(false);
    var [promise, abort] = fetchFunction(
      "accounts/aggs/analysis/invoiceGraph",
      "POST",
      filter
    );
    promise.then((data) => {
      if (!data) {
        setGraphData([]);
      } else {
        /* Need to destroy (hide in our case) before create (change data for us) to prevent 
        ERROR TypeError: Cannot read property '¿?' of undefined
        at ChartElement.getPixelForValue*/
        setGraphData(data);
        setChartVisible(false);
        buildDatasets(data);
        setChartVisible(true);
      }
    });
  };

  const buildDatasets = (data) => {
    var temp_datasets = [];
    var index = 0;
    Object.keys(data.datasets).forEach((key) => {
      var color, showLine;
      if (data && data.datasets[key].color) {
        color = data.datasets[key].color;
        showLine = true;
      } else {
        color = colorsArray[invoiceType][(index - 1) % 5];
        showLine = false;
      }

      var temp_data = {
        name: data && data.datasets[key].name, //Para highcharts
        data:
          data &&
          data.label.map((k, i) => [
            moment(k).unix() * 1000,
            data.datasets[key].balance[i],
          ]),
        marker: {
          enabled: groupedBy == "account_id" ? false : true,
          radius: 3,
        },
        lineWidth: showLine ? 1 : 0, // fill: false,
        // visible: showLine,
        type: data && data.datasets[key].type,
        color: color,
        yAxis: data && data.datasets[key].yAxisID,
        tooltip: {
          valueSuffix:
            data && data.datasets[key].yAxisID == "amount"
              ? " " + data.symbol
              : " " + data.units,
        },
        //borderColor: data && data.datasets[key].color + "33",
      };
      //   {
      //     label: data && data.datasets[key].name,
      //     data: data && data.datasets[key].balance,
      //     fill: false,
      //     type: data && data.datasets[key].type,
      //     showLine: showLine,
      //     backgroundColor: color,
      //     borderColor: color,
      //     yAxisID: data && data.datasets[key].yAxisID,
      //     //borderColor: data && data.datasets[key].color + "33",
      //   };
      temp_datasets.push(temp_data);
      index += 1;
    });
    setDatasets(temp_datasets);
  };

  useEffect(() => {
    if (props.filter.account_graph) {
      fetchGraph();
    }

    setFirstRender(true);
  }, []);

  useEffect(() => {
    if (!firstRender) return;
    fetchGraph();
  }, [groupedBy]);

  useEffect(() => {
    if (!firstRender) return;
    //set light as principal invoice
    if (!invoiceType) {
      setSetInvoiceType("light_house");
    } else {
      parentCallBack([invoiceType]);
      fetchGraph();
    }
  }, [dateGroup, invoiceType, prevProps]);

  return (
    <Card style={cardStyle}>
      <Card.Body style={{ width: "100%", overflow: "auto" }}>
        <Col>
          {" "}
          <Row>
            <Col md={5}>
              <Form.Group>
                <Form.SelectGroup
                  value={invoiceType}
                  onChange={(e) => setSetInvoiceType(e.target.value)}
                >
                  <Form.SelectGroupItem
                    label="Agua"
                    name="setGroupedBy"
                    value="water_house"
                    checked={invoiceType == "water_house"}
                    onChange={(e) => {}}
                  />
                  <Form.SelectGroupItem
                    label="Luz"
                    name="setGroupedBy"
                    value="light_house"
                    checked={invoiceType == "light_house"}
                    onChange={(e) => {}}
                  />
                  <Form.SelectGroupItem
                    label="Gas"
                    name="setGroupedBy"
                    value="gas_house"
                    checked={invoiceType == "gas_house"}
                    onChange={(e) => {}}
                  />
                </Form.SelectGroup>
              </Form.Group>
            </Col>
            <Col md={1}></Col>
            <Col md={1}>
              {/* <Select
                onChange={(e) => {
                  setGraphType(e);
                }}
                isSearchable={false}
                value={{
                  label: graphType ? graphType.label : "Selecciona tipo",
                }}
                options={graphTypeOptions}
              /> */}
            </Col>
            <Col md={1}></Col>

            <Col md={4} style={{ float: "right" }}>
              <Form.Group>
                <Form.SelectGroup
                  value={dateGroup}
                  onChange={(e) => setDateGroup(e.target.value)}
                >
                  {/* <Form.SelectGroupItem
                    label="día"
                    name="dateGroup"
                    value="day"
                    checked={dateGroup == "day"}
                    onChange={(e) => {}}
                  /> */}
                  <Form.SelectGroupItem
                    label="mes"
                    name="dateGroup"
                    value="month"
                    checked={dateGroup == "month"}
                    onChange={(e) => {}}
                  />
                  <Form.SelectGroupItem
                    label="año"
                    name="dateGroup"
                    value="year"
                    checked={dateGroup == "year"}
                    onChange={(e) => {}}
                  />
                </Form.SelectGroup>
              </Form.Group>
            </Col>
          </Row>
          {chartVisible &&
            (!props.filter.stock_graph ||
              (props.filter.stock_graph &&
                !["concept_id", "category_id"].includes(groupedBy))) && (
              <Row>
                <Col
                  style={{
                    minHeight: window.innerWidth > 900 ? "340px" : "unset",
                  }}
                >
                  <HighchartsReact
                    highcharts={Highcharts}
                    options={{
                      chart: {
                        type: "column",
                        zoomType: "xy",
                        events: {
                          exportData: function (data) {
                            var newData = { ...data };
                            // console.log(newData.dataRows);
                            newData.dataRows = data.dataRows.map(
                              (element, e_i) => {
                                // console.log(element.length)
                                element.forEach((value, v_i) => {
                                  if (e_i >= 2 && v_i == 1 && value) {
                                    element[v_i] = currencyFormater(
                                      Number(element[v_i]),
                                      graphData && graphData.format,
                                      graphData && graphData.symbol
                                    );
                                  } else if (e_i >= 2 && v_i > 1 && value) {
                                    element[v_i] = graphData.units
                                      ? element[v_i] + " " + graphData.units
                                      : "";
                                  }
                                });
                              }
                            );

                            return newData;
                          },
                        },
                      },
                      title: {
                        text: "Consumo",
                      },
                      // xAxis: {
                      //   categories: graphData && graphData.label,
                      // },
                      yAxis: [
                        {
                          id: "amount",
                          labels: {
                            format: "{value} " + "€",
                            style: {
                              // color: element.labelColor
                            },
                          },
                          title: {
                            text: "", //'Balance',
                            style: {
                              // color: element.titleColor
                            },
                          },
                          min: null, // element.min ? element.min : null,
                          max: null, // element.max ? element.max : null,
                          opposite: false, // element.isRight
                          labels: {
                            formatter: function () {
                              return currencyFormater(
                                Number(this.value),
                                graphData && graphData.format,
                                graphData && graphData.symbol
                              );
                            },
                          },
                        },
                        {
                          id: "consumption",
                          labels: {
                            style: {
                              // color: element.labelColor
                            },
                          },
                          title: {
                            text: "", //'Balance',
                            style: {
                              // color: element.titleColor
                            },
                          },
                          allowDecimals: false,
                          min: null, // element.min ? element.min : null,
                          max: null, // element.max ? element.max : null,
                          opposite: true, // element.isRight
                          labels: {
                            formatter: function () {
                              return graphData.units
                                ? this.value + " " + graphData.units
                                : "";
                            },
                          },
                        },
                      ],

                      xAxis: {
                        // title: {text: 'Fecha'},
                        type: "datetime",
                        // dateTimeLabelFormats: {
                        //   // don't display the dummy year
                        //   month: "%e. %b",
                        //   year: "%b",
                        // },
                        // labels:{
                        //   formatter: function() {
                        //     return Highcharts.dateFormat(this.dateTimeLabelFormat, this.value);
                        //   }
                        // }
                      },

                      legend: {
                        maxHeight: 60,
                        // enabled:false
                        //       layout: 'vertical',
                        //       align: 'right',
                        //       verticalAlign: 'middle'
                      },
                      plotOptions: {
                        series: {
                          label: {
                            connectorAllowed: false,
                          },
                          // marker: {
                          //   enabled: false,
                          // },
                        },
                      },
                      tooltip: {
                        xDateFormat: "%d/%m/%Y",
                        // pointFormat: '{point.y} €',
                        // valueSuffix: " €",
                        shared: true,
                        crosshairs: true,
                      },
                      series: datasets,
                      responsive: {
                        rules: [
                          {
                            condition: {
                              maxWidth: 500,
                            },
                            // chartOptions: {
                            //     legend: {
                            //         layout: 'horizontal',
                            //         align: 'center',
                            //         verticalAlign: 'bottom'
                            //     }
                            // }
                          },
                        ],
                      },
                      exporting: {
                        showTable: false,
                        tableCaption: "",
                        csv: {
                          dateFormat: "%d/%m/%Y",
                        },
                      },
                      credits: {
                        enabled: false,
                      },
                    }}
                  />
                </Col>
              </Row>
            )}
          {props.filter.stock_graph &&
            ["concept_id", "category_id"].includes(groupedBy) && (
              <Row>
                <Col
                  style={{
                    minHeight: window.innerWidth > 900 ? "340px" : "unset",
                  }}
                >
                  <h1>Este gráfico no esta disponible</h1>
                </Col>
              </Row>
            )}
        </Col>
      </Card.Body>
    </Card>
  );
}

export default AccountAnalytics;
