import React, { useState, useRef } from "react";
import Select from "react-select";
import fetchFunction from "../../../helpers/fetchFuction";

import moment from "moment";
import DateRangePicker from "react-bootstrap-daterangepicker";
import textsCalendar from "../../../texts/Calendar";
import "bootstrap-daterangepicker/daterangepicker.css";
import { Col, Row, Button } from "react-bootstrap";
//import { Button } from "tabler-react";

const YearPickerRow = ({ parentCallBack }) => {
  const language = sessionStorage.getItem("language");
  const [state, setState] = useState();
  const [yearsRange, setYearsRange] = useState([]);
  const keyRef = useRef(Date.now());

  const buttonStyle = {
    color: "black",
    borderRadius: "50%",
    border: "1px solid rgb(204, 204, 204)",
    background: "white",
  };

  const handleCallback = (year) => {
    setState(year);
    parentCallBack([year[0].value,year[1].value]);
  };
 
  const fetchYearsRange = () => {
    var filter ={}
    // setRecords(false);
    var [promise, abort] = fetchFunction("accounts/aggs/analysis/yearsRange", "POST", filter)
    promise.then((data) => {
      if (!data) {
        setYearsRange([])
      } else {
        setYearsRange(data);
        if (data.length >= 5){
          handleCallback([data[4],data[0]])
        }else{
          handleCallback([data[data.length-1], data[0]])
        }
      }
    });
     
  };

  const moveRange = (isBackwards) => {
    var years = []
    yearsRange.map((year)=> years.push(year.value))
    var year_1 = state[0].value
    var year_2 = state[1].value
    var newYear_1 = year_1 + (!isBackwards ? 1 : -1)
    var newYear_2 = year_2 + (!isBackwards ? 1 : -1)
    if (!years.includes(newYear_1)){
      newYear_1 = year_1
    }
    if (!years.includes(newYear_2)){
      newYear_2 = year_2
    }
    handleCallback([{label: newYear_1, value: newYear_1},{label: newYear_2, value: newYear_2}]);
  };

  React.useEffect(() => {
    fetchYearsRange()
  }, []);

  // var label = start.format("DD/MM/YYYY") + " - " + end.format("DD/MM/YYYY");
  // if (start.format("DD/MM/YYYY") == end.format("DD/MM/YYYY")) {
  //   var label = start.format("DD/MM/YYYY");
  // }
  return (
    <Row style={{ margin: "auto" }}>
      <Button size="sm" style={buttonStyle} onClick={() => moveRange(true)}>
        {"<"}
      </Button>
      <>
      <Select
        className="date-picker-row-date-half"
        onChange={(e) => {
          var temp = [e, state[1]]
           handleCallback(temp);
        }}
        isSearchable={false}
        value={state && state[0]}
        options={yearsRange}
      ></Select>
      <Select
        className="date-picker-row-date-half"
        onChange={(e) => {
          var temp = [state[0], e]
          handleCallback(temp);
        }}
        isSearchable={false}
        value={state && state[1]}
        options={yearsRange}
      ></Select></>
      <Button size="sm" style={buttonStyle} onClick={() => moveRange(false)}>
        {">"}
      </Button>
    </Row>
  );
};

export default YearPickerRow;
