// @flow

import React, { useEffect } from "react";
import Table from "../Table";
import fetchFunction from "../../../helpers/fetchFuction";
import { Col, Row, Card } from "react-bootstrap";
import AccountDetailsCard from "../AccountAnalysisCard";

function AccountAnalytics({ props, updateSignal }) {
  const [data, setData] = React.useState([]);
  const [columns, setColumns] = React.useState([]);
  const [initialExpanded, setInitialExpanded] = React.useState();
  const [prevProps, setPrevProps] = React.useState({});
  const [firstRender, setFirstRender] = React.useState(false);
  const cardStyle = {
    marginBottom: "0.1rem",
    // borderLeft: "5px solid " + graphData.color,
  };
  React.useEffect(() => {
    // if (!firstRender) return;
    if (JSON.stringify(props) == JSON.stringify(prevProps)) return;
    if (props.hasDateFilter && !props.filter.start_date && !props.filter.year)
      return;
    setPrevProps(props);
  }, [props]);

  React.useEffect(() => {
    if (!firstRender) return;
    if (JSON.stringify(prevProps) == JSON.stringify({})) return;
    fetchTable();
  }, [prevProps]);

  const setHeaders = () => {
    var columns = [
      {
        // Build our expander column
        id: "expander", // Make sure it has an ID
        Header: ({ getToggleAllRowsExpandedProps, isAllRowsExpanded }) => (
          <span {...getToggleAllRowsExpandedProps()}>
            {isAllRowsExpanded ? "⮁" : "⮀"}
          </span>
        ),
        Cell: ({ row }) =>
          // Use the row.canExpand and row.getToggleRowExpandedProps prop getter
          // to build the toggle for expanding a row
          row.canExpand ? (
            <span
              {...row.getToggleRowExpandedProps({
                style: {
                  // We can even use the row.depth property
                  // and paddingLeft to indicate the depth
                  // of the row
                  paddingLeft: `${row.depth * 2}rem`,
                },
              })}
            >
              {row.isExpanded ? "⤴" : "⤵"}
            </span>
          ) : null,
      },
      {
        Header: "Etiqueta",
        columns: [
          {
            Header: "Tot./Cat./Conc.",
            accessor: "label",
          },
        ],
      },
      {
        Header:
          "_ Año " +
          props.filter.year +
          " _________________________________________________________________________" +
          " Año " +
          props.filter.year +
          " _",
        style: { "text-align": "center" },
        columns: [
          {
            Header: "Total",
            accessor: "total",
            className: "table_tot table_fixed_width",
          },
          {
            Header: "Enero",
            accessor: "jan",
            className: "table_jan table_fixed_width",
          },
          {
            Header: "Febrero",
            accessor: "feb",
            className: "table_feb table_fixed_width",
          },
          {
            Header: "Marzo",
            accessor: "mar",
            className: "table_mar table_fixed_width",
          },
          {
            Header: "Abril",
            accessor: "apr",
            className: "table_apr table_fixed_width",
          },
          {
            Header: "Mayo",
            accessor: "may",
            className: "table_may table_fixed_width",
          },
          {
            Header: "Junio",
            accessor: "jun",
            className: "table_jun table_fixed_width",
          },
          {
            Header: "Julio",
            accessor: "jul",
            className: "table_jul table_fixed_width",
          },
          {
            Header: "Agosto",
            accessor: "ago",
            className: "table_ago table_fixed_width",
          },
          {
            Header: "Septiembre",
            accessor: "sep",
            className: "table_sep table_fixed_width",
          },
          {
            Header: "Octubre",
            accessor: "oct",
            className: "table_oct table_fixed_width",
          },
          {
            Header: "Noviembre",
            accessor: "nov",
            className: "table_nov table_fixed_width",
          },
          {
            Header: "Diciembre",
            accessor: "dic",
            className: "table_dic table_fixed_width",
          },
        ],
      },
    ];
    setColumns(columns);
  };
  const fetchTable = () => {
    setHeaders();
    var filter = {
      year: props.filter.year,
      concepts: props.filter.concepts,
      accounts: props.filter.accounts,
      type: props.filter.types,
      note: props.filter.note,
      group: props.filter.groups,
      subgroup: props.filter.subgroups,
    };
    // setRecords(false);
    var [promise, abort] = fetchFunction(
      "accounts/aggs/analysis/monthlyTable",
      "POST",
      filter
    );
    promise.then((data) => {
      if (!data) {
        setData([]);
      } else {
        setData(data);
      }
    });
  };

  useEffect(() => {
    fetchTable();
    setFirstRender(true);
  }, []);

  useEffect(() => {
    if (!data.length) return;
    var balance_index = 0;
    var expanded_object = {};
    data.map((balance) => {
      var category_index = 0;
      expanded_object[balance_index.toString()] = true;
      // balance.subRows.map((category) => {
      //   var concept_index = 0;
      //   expanded_object[
      //     balance_index.toString() + "." + category_index.toString()
      //   ] = true;
      //   // category.subRows.map((concept) => {
      //   //   expanded_object[
      //   //     balance_index.toString() +
      //   //       "." +
      //   //       category_index.toString() +
      //   //       "." +
      //   //       concept_index.toString()
      //   //   ] = true;
      //   //   concept_index += 1;
      //   // });
      //   category_index += 1;
      // });
      balance_index += 1;
    });

    setInitialExpanded(expanded_object);
  }, [data]);

  return (
    <Card style={cardStyle}>
      <Card.Body
        style={{
          width: "100%",
          overflow: "auto",
          minHeight: window.innerWidth > 900 ? "340px" : "unset",
          maxHeight: window.innerWidth > 900 ? "800px" : "unset",
        }}
      >
        {initialExpanded && (
          <Table
            columns={columns}
            data={data}
            initialState={{
              expanded: initialExpanded,
            }}
            getRowProps={(row) => ({
              className: row.index % 2 === 0 ? "even" : "odd",
            })}
            getHeaderProps={(header) => ({
              style: { "text-align": "center" },
            })}
            getCellProps={(cell) => {
              if (
                !cell.value ||
                !/^-?\d*\.?\d*\,?\d+$/i.test(cell.value.slice(0, -1))
              )
                return {};

              if (/^-\d*\.?\d*\,?\d+$/i.test(cell.value.slice(0, -1))) {
                var className = "negative";
              } else if (/^0\,?0*$/.test(cell.value.slice(0, -1))) {
                var className = "zero";
              } else {
                var className = "positive";
              }
              return {
                className: className,
              };
            }}
          />
        )}
      </Card.Body>
    </Card>
  );
}

export default AccountAnalytics;
