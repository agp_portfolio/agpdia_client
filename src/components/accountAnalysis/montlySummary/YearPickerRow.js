import React, { useState, useRef } from "react";
import Select from "react-select";
import fetchFunction from "../../../helpers/fetchFuction";

import moment from "moment";
import DateRangePicker from "react-bootstrap-daterangepicker";
import textsCalendar from "../../../texts/Calendar";
import "bootstrap-daterangepicker/daterangepicker.css";
import { Col, Row, Button } from "react-bootstrap";
//import { Button } from "tabler-react";

const YearPickerRow = ({ parentCallBack }) => {
  const language = sessionStorage.getItem("language");
  const [state, setState] = useState({
    label: moment().year(),
    value: moment().year(),
  });
  const [yearsRange, setYearsRange] = useState([]);
  const { value } = state;
  const keyRef = useRef(Date.now());

  const buttonStyle = {
    color: "black",
    borderRadius: "50%",
    border: "1px solid rgb(204, 204, 204)",
    background: "white",
  };

  const handleCallback = (year) => {
    setState(year);
    parentCallBack(year.value);
  };
 
  const fetchYearsRange = () => {
    var filter ={}
    // setRecords(false);
    var [promise, abort] = fetchFunction("accounts/aggs/analysis/yearsRange", "POST", filter)
    promise.then((data) => {
      if (!data) {
        setYearsRange([])
      } else {
        setYearsRange(data);
        if (state != data[0]){
          handleCallback(data[0])
        }
      }
    });
  };

  const moveRange = (isBackwards) => {
    var years = []
    yearsRange.map((year)=> years.push(year.value))
    var year = state.value
    var newYear = year + (!isBackwards ? 1 : -1)
    if (years.includes(newYear)){
      handleCallback({label: newYear, value: newYear});
    }
  };

  React.useEffect(() => {
    parentCallBack(value);
    fetchYearsRange();
  }, []);

  // var label = start.format("DD/MM/YYYY") + " - " + end.format("DD/MM/YYYY");
  // if (start.format("DD/MM/YYYY") == end.format("DD/MM/YYYY")) {
  //   var label = start.format("DD/MM/YYYY");
  // }
  return (
    <Row style={{ margin: "auto" }}>
      <Button size="sm" style={buttonStyle} onClick={() => moveRange(true)}>
        {"<"}
      </Button>
      <Select
        className="date-picker-row-date-half"
        onChange={(e) => {
           handleCallback(e);
        }}
        isSearchable={false}
        value={state}
        options={yearsRange}
      ></Select>
      <Button size="sm" style={buttonStyle} onClick={() => moveRange(false)}>
        {">"}
      </Button>
    </Row>
  );
};

export default YearPickerRow;
