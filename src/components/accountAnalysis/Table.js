import React, { useEffect } from 'react'
import { useTable, useExpanded } from 'react-table'


const defaultPropGetter = () => ({})

function Table({
  columns,
  data,
  initialState,
  getHeaderProps = defaultPropGetter,
  getColumnProps = defaultPropGetter,
  getRowProps = defaultPropGetter,
  getCellProps = defaultPropGetter,
  }) {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    state: { expanded },
  } = useTable(
    {
      columns,
      data,
      initialState
    },
    useExpanded, // Use the useExpanded plugin hook
  )
  return (
    <>
      <table className="analysis-table" {...getTableProps()}>
        <thead>
          {headerGroups.map(headerGroup => (
            <tr {...headerGroup.getHeaderGroupProps([
              {
                className: headerGroup.className,
                style: headerGroup.style,
              },
              getHeaderProps(headerGroup),
            ])}>
              {headerGroup.headers.map(column => (
                <th {...column.getHeaderProps([
                  {
                    className: column.className,
                    style: column.style,
                  },
                  getColumnProps(column),
                  getHeaderProps(column),
                ])}>{column.render('Header')}</th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()}>
          {rows.map((row, i) => {
            prepareRow(row)
            return (
              <tr {...row.getRowProps([
                {
                  className: row.original.className,
                  style: row.original.style,
                },
                getRowProps(row),
                getHeaderProps(row),
              ])}>
                {row.cells.map(cell => {
                  return <td {...cell.getCellProps([
                    {
                      className: cell.column.className,
                      style: cell.column.style,
                    },
                    getColumnProps(cell.column),
                    getCellProps(cell),
                    getRowProps(cell.row),
                  ])}>{cell.render('Cell')}</td>
                })}
              </tr>
            )
          })}
        </tbody>
      </table>
    </>
  )
}


export default Table
