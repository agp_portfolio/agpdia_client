import React from "react";

import { Form, Card } from "tabler-react";
import Tree from "react-animated-tree";
import { Row, Col } from "react-bootstrap";
import texts from "../../texts/AccountRecordFilter";
import fetchFunction from "../../helpers/fetchFuction";

const AccountRecord = ({ toShow, parentCallBack }) => {
  const language = sessionStorage.getItem("language");
  const [error, setError] = React.useState({});
  const [conceptsArray, setConceptsArray] = React.useState({
    info: [],
    filters: [],
    tree: [],
    visible: true,
  });
  const [accountsArray, setAccountsArray] = React.useState({
    info: [],
    filters: [],
    tree: [],
    visible: true,
  });
  const [vehiclesArray, setVehiclesArray] = React.useState({
    info: [],
    filters: [],
    tree: [],
    visible: true,
  });
  const [housesArray, setHousesArray] = React.useState({
    info: [],
    filters: [],
    tree: [],
    visible: true,
  });
  const [typesArray, setTypesArray] = React.useState({
    info: [
      { name: texts.typeExpense[language], id: "expense", visible: true },
      { name: texts.typeInfo[language], id: "info", visible: true },
      { name: texts.typeIncome[language], id: "income", visible: true },
      { name: texts.typeTransfer[language], id: "transfer", visible: true },
    ],
    filters: [],
    tree: [],
    visible: true,
  });
  const [groupArray, setGroupArray] = React.useState({
    info: [
      { name: "General", id: "general", visible: true },
      { name: "Vivienda", id: "house", visible: true },
      { name: "Vehículo", id: "vehicle", visible: true },
      { name: "Acciones", id: "stock", visible: true },
    ],
    filters: [],
    tree: [],
    visible: true,
  });
  const [subgroupArray, setSubgroupArray] = React.useState({
    info: [
      { name: "General", id: "general", visible: true },
      { name: "Nómina", id: "paysheet", visible: true },
      { name: "Agua", id: "water_house", visible: true },
      { name: "Luz", id: "light_house", visible: true },
      { name: "Gas", id: "gas_house", visible: true },
      { name: "Combustible", id: "power_source_vehicle", visible: true },
      { name: "Mantenimiento", id: "maintenance_vehicle", visible: true },
    ],
    filters: [],
    tree: [],
    visible: true,
  });
  const [houseSubgroupArray, setHouseSubgroupArray] = React.useState({
    info: [
      { name: "General", id: "general", visible: true },
      { name: "Agua", id: "water_house", visible: true },
      { name: "Luz", id: "light_house", visible: true },
      { name: "Gas", id: "gas_house", visible: true },
    ],
    filters: [],
    tree: [],
    visible: true,
  });
  const [invoiceSubgroupArray, setInvoiceSubgroupArray] = React.useState({
    info: [
      { name: "Agua", id: "water_house", visible: true },
      { name: "Luz", id: "light_house", visible: true },
      { name: "Gas", id: "gas_house", visible: true },
    ],
    filters: [],
    tree: [],
    visible: true,
  });
  const [vehicleSubgroupArray, setVehicleSubgroupArray] = React.useState({
    info: [
      { name: "General", id: "general", visible: true },
      { name: "Combustible", id: "power_source_vehicle", visible: true },
      { name: "Mantenimiento", id: "maintenance_vehicle", visible: true },
    ],
    filters: [],
    tree: [],
    visible: true,
  });
  const [noteFilter, setNoteFilter] = React.useState();
  const [accounts, setAccounts] = React.useState();
  const [concepts, setConcepts] = React.useState();
  const [vehicles, setVehicles] = React.useState();
  const [houses, setHouses] = React.useState();
  const [prevToShow, setPrevToShow] = React.useState();

  React.useEffect(() => {
    // if (!firstRender) return;
    if (JSON.stringify(toShow) == JSON.stringify(prevToShow)) return;
    setPrevToShow(toShow);
  }, [toShow]);

  const fetchAccounts = () => {
    var [promise, abort] = fetchFunction("accounts/accounts/", "GET")
    promise.then((data) => {
      if (!data) {
        // setAccounts([]);
      } else {
        setAccounts(data)
      }
    });


  };

  const fetchConcepts = () => {
    var filter = {};
    if (toShow.includes("house_concepts")) {
      filter.group = ["house"];
    }
    if (toShow.includes("vehicle_concepts")) {
      filter.group = ["vehicle"];
    }
    if (toShow.includes("stock_concepts")) {
      filter.group = ["stock"];
    }
    if (toShow.includes("invoice_concepts")) {
      filter.subgroup = ["water_house", "light_house", "gas_house"];
    }
    if (toShow.includes("paysheet_concepts")) {
      filter.subgroup = ["paysheet"];
    }
    var [promise, abort] = fetchFunction("accounts/aggs/concepts/filter_list", "POST", filter)

        if (promise.status == "404") {
          setError({
            type: "warning",
            message: "No se han encontrado cuentas para este usuario",
          });
        } else {
          promise.then((data) => {
            setConcepts(data);
          });
        }
  };
  const fetchVehicles = () => {
    var [promise, abort] = fetchFunction("vehicles/", "GET")
        if (promise.status == "404") {
          setError({
            type: "warning",
            message: "No se han encontrado vehículos para este usuario",
          });
        } else {
          promise.then((data) => {
            setVehicles(data);
          });
        }
  };

  const fetchHouses = () => {
    var [promise, abort] = fetchFunction("houses/", "GET")

        if (promise.status == "404") {
          setError({
            type: "warning",
            message: "No se han encontrado viviendas para este usuario",
          });
        } else {
          promise.then((data) => {
            setHouses(data);
          });
        }

  };

  const formatParentCallback = () => {
    var conceptsFilter = undefined;
    var accountsFilter = undefined;
    var typesFilter = undefined;
    var groupFilter = undefined;
    var subgroupFilter = undefined;
    var noteFilter_temp = undefined;
    var housesFilter = undefined;
    var vehiclesFilter = undefined;
    if (toShow.includes("note")) {
      noteFilter_temp = noteFilter;
    }
    if (!conceptsArray.visible == true && toShow.some((r) =>
    [
      "concepts",
      "house_concepts",
      "vehicle_concepts",
      "invoice_concepts",
      "stock_concepts",
      "paysheet_concepts",
    ].includes(r))) {
      conceptsFilter = conceptsArray.filters;
    }
    if (!accountsArray.visible == true && toShow.includes("accounts")) {
      accountsFilter = accountsArray.filters;
    }
    if (!typesArray.visible == true && toShow.includes("types")) {
      typesFilter = typesArray.filters;
    }
    if (!groupArray.visible == true && toShow.includes("groups")) {
      groupFilter = groupArray.filters;
    }
    if (
      !houseSubgroupArray.visible == true &&
      toShow.includes("house_subgroups")
    ) {
      subgroupFilter = houseSubgroupArray.filters;
    }
    if (
      !vehicleSubgroupArray.visible == true &&
      toShow.includes("vehicle_subgroups")
    ) {
      subgroupFilter = vehicleSubgroupArray.filters;
    }
    if (
      !invoiceSubgroupArray.visible == true &&
      toShow.includes("invoice_subgroups")
    ) {
      subgroupFilter = invoiceSubgroupArray.filters;
    }
    if (!subgroupArray.visible == true && toShow.includes("subgroups")) {
      subgroupFilter = subgroupArray.filters;
    }
    if (!vehiclesArray.visible == true && toShow.includes("vehicles")) {
      vehiclesFilter = vehiclesArray.filters;
    }
    if (!housesArray.visible == true && toShow.includes("houses")) {
      housesFilter = housesArray.filters;
    }

    parentCallBack(
      conceptsFilter,
      accountsFilter,
      typesFilter,
      noteFilter_temp,
      groupFilter,
      subgroupFilter,
      vehiclesFilter,
      housesFilter
    );
  };

  React.useEffect(() => {
    if (!prevToShow) return;

    fetchAccounts();
    fetchConcepts();
    fetchVehicles();
    fetchHouses();
  }, [prevToShow]);

  React.useEffect(() => {
    if (toShow.includes("types")) {
      processTreeLvl1(typesArray, "setTypesArray");
    }
    if (toShow.includes("groups")) {
      processTreeLvl1(groupArray, "setGroupArray");
    }
    if (toShow.includes("subgroups")) {
      processTreeLvl1(subgroupArray, "setSubgroupArray");
    }
    if (toShow.includes("house_subgroups")) {
      processTreeLvl1(houseSubgroupArray, "setHouseSubgroupArray");
    }
    if (toShow.includes("vehicle_subgroups")) {
      processTreeLvl1(vehicleSubgroupArray, "setVehicleSubgroupArray");
    }
    if (toShow.includes("invoice_subgroups")) {
      processTreeLvl1(invoiceSubgroupArray, "setInvoiceSubgroupArray");
    }
    if (accounts && toShow.includes("accounts")) {
      var temp = { info: [], filters: [], tree: [], visible: true };
      accounts.forEach((element) => {
        temp.info.push({ name: element.name, id: element._id, visible: true });
      });
      processTreeLvl1(temp, "setAccountsArray");
    }
    if (vehicles && toShow.includes("vehicles")) {
      var temp = { info: [], filters: [], tree: [], visible: true };
      vehicles.forEach((element) => {
        temp.info.push({ name: element.name, id: element._id, visible: true });
      });
      processTreeLvl1(temp, "setVehiclesArray");
    }
    if (houses && toShow.includes("houses")) {
      var temp = { info: [], filters: [], tree: [], visible: true };
      houses.forEach((element) => {
        temp.info.push({ name: element.name, id: element._id, visible: true });
      });
      processTreeLvl1(temp, "setHousesArray");
    }

    if (
      concepts &&
      toShow.some((r) =>
        [
          "concepts",
          "house_concepts",
          "vehicle_concepts",
          "invoice_concepts",
          "stock_concepts",
          "paysheet_concepts",
        ].includes(r)
      )
    ) {
      var temp = { info: [], filters: [], tree: [], visible: true };

      concepts.forEach((element) => {
        temp[element._id] = { info: [], tree: [] };
        element.concepts.forEach((element2) => {
          temp[element._id].info.push({
            name: element2.name,
            id: element2.id,
            visible: true,
          });
        });
        temp.info.push({ name: element.name, id: element._id, visible: true });
      });

      processTreeLvl2(temp, "setConceptsArray");
    }
  }, [prevToShow, accounts, concepts, vehicles, houses]);

  const processTreeLvl1 = (array, setArray) => {
    var temp = { ...array };
    temp.tree = [];
    temp.filters = [];
    array.info.forEach((element) => {
      if (element.visible) {
        temp.filters.push(element.id);
      }
      temp.tree.push(
        <Tree
          key={element.id}
          content={element.name}
          visible={element.visible}
          canHide
          onClick={(e) => handleClick(temp, 1, 1, setArray, e, element.id)}
        />
      );
    });
    eval(setArray)(temp);
  };
  const processTreeLvl2 = (array, setArray) => {
    var temp = { ...array };
    temp.tree = [];
    temp.filters = [];

    temp.info.forEach((element) => {
      temp[element.id].tree = [];
      temp[element.id].info.forEach((element2) => {
        if (element2.visible) {
          temp.filters.push(element2.id);
        }
        temp[element.id].tree.push(
          <Tree
            key={element2.id}
            content={element2.name}
            visible={element2.visible}
            canHide
            onClick={(e) =>
              handleClick(temp, 2, 2, setArray, e, element2.id, element.id)
            }
          />
        );
      });

      temp.tree.push(
        <Tree
          key={element.id}
          content={element.name}
          visible={element.visible}
          canHide
          onClick={(e) => handleClick(temp, 1, 2, setArray, e, element.id)}
        >
          {temp[element.id].tree}
        </Tree>
      );
    });
    setConceptsArray(temp);
  };
  React.useEffect(() => {
    formatParentCallback();
  }, [
    conceptsArray,
    accountsArray,
    typesArray,
    noteFilter,
    groupArray,
    subgroupArray,
    houseSubgroupArray,
    vehicleSubgroupArray,
    invoiceSubgroupArray,
    vehiclesArray,
    housesArray,
  ]);

  const handleClick = (array, position, lvl, setArray, state, id, parentId) => {
    if (position == 0 && lvl >= 1) {
      array.info.forEach((element) => {
        if (lvl == 2) {
          array[element.id].info.forEach((element) => {
            element.visible = state;
          });
        }
        element.visible = state;
      });
      array.visible = state;
    }
    if (position == 1 && lvl >= 1) {
      var index = array.info.findIndex((x) => x.id == id);
      array.info[index].visible = state;
    }
    if (position == 1 && lvl == 2) {
      array[id].info.forEach((element) => {
        element.visible = state;
      });
    }

    if (position == 2 && lvl == 2) {
      var index2 = array[parentId].info.findIndex((x) => x.id == id);
      array[parentId].info[index2].visible = state;
      var parent1Visible = true;
      array[parentId].info.forEach((element) => {
        parent1Visible = parent1Visible && element.visible;
      });
      var index3 = array.info.findIndex((x) => x.id == parentId);
      array.info[index3].visible = parent1Visible;
    }
    if (position >= 1 && lvl >= 1) {
      var parent0Visible = true;
      array.info.forEach((element) => {
        parent0Visible = parent0Visible && element.visible;
      });
      array.visible = parent0Visible;
    }

    eval("processTreeLvl" + lvl)(array, setArray);
  };

  return (
    <Card title={texts.records[language]} isCollapsible>
      <Card.Body >
        <Row style={{minHeight:'106.72px'}}>
          {toShow.includes("note") && (
            <Col lg={12} md={12}>
              <Form.Group label={texts.noteFilterLabel[language]}>
                <Form.Input
                  name="noteFilter"
                  autoComplete="off"
                  value={noteFilter}
                  placeholder={texts.noteFilterPlaceholder[language]}
                  onChange={(e) => setNoteFilter(e.target.value)}
                />
              </Form.Group>
              <br />
            </Col>
          )}
          {toShow.includes("groups") && (
            <Col lg={12} md={4}>
              <Tree
                content="Grupo"
                type={
                  groupArray.visible
                    ? "ALL"
                    : groupArray.filters.length
                    ? "SOME"
                    : "NONE"
                }
                canHide
                visible={groupArray.visible}
                onClick={(e) =>
                  handleClick(groupArray, 0, 1, "setGroupArray", e)
                }
              >
                {groupArray.tree}
              </Tree>
              <br />
            </Col>
          )}
          {toShow.includes("subgroups") && (
            <Col lg={12} md={4}>
              <Tree
                content="Subgrupo"
                type={
                  subgroupArray.visible
                    ? "ALL"
                    : subgroupArray.filters.length
                    ? "SOME"
                    : "NONE"
                }
                canHide
                visible={subgroupArray.visible}
                onClick={(e) =>
                  handleClick(subgroupArray, 0, 1, "setSubgroupArray", e)
                }
              >
                {subgroupArray.tree}
              </Tree>
              <br />
            </Col>
          )}
          {toShow.includes("house_subgroups") && (
            <Col lg={12} md={4}>
              <Tree
                content="Subgrupo"
                type={
                  houseSubgroupArray.visible
                    ? "ALL"
                    : houseSubgroupArray.filters.length
                    ? "SOME"
                    : "NONE"
                }
                canHide
                visible={houseSubgroupArray.visible}
                onClick={(e) =>
                  handleClick(
                    houseSubgroupArray,
                    0,
                    1,
                    "setHouseSubgroupArray",
                    e
                  )
                }
              >
                {houseSubgroupArray.tree}
              </Tree>
              <br />
            </Col>
          )}
          {toShow.includes("vehicle_subgroups") && (
            <Col lg={12} md={4}>
              <Tree
                content="Subgrupo"
                type={
                  vehicleSubgroupArray.visible
                    ? "ALL"
                    : vehicleSubgroupArray.filters.length
                    ? "SOME"
                    : "NONE"
                }
                canHide
                visible={vehicleSubgroupArray.visible}
                onClick={(e) =>
                  handleClick(
                    vehicleSubgroupArray,
                    0,
                    1,
                    "setVehicleSubgroupArray",
                    e
                  )
                }
              >
                {vehicleSubgroupArray.tree}
              </Tree>
              <br />
            </Col>
          )}
          {toShow.includes("invoice_subgroups") && (
            <Col lg={12} md={4}>
              <Tree
                content="Subgrupo"
                type={
                  invoiceSubgroupArray.visible
                    ? "ALL"
                    : invoiceSubgroupArray.filters.length
                    ? "SOME"
                    : "NONE"
                }
                canHide
                visible={invoiceSubgroupArray.visible}
                onClick={(e) =>
                  handleClick(
                    invoiceSubgroupArray,
                    0,
                    1,
                    "setInvoiceSubgroupArray",
                    e
                  )
                }
              >
                {invoiceSubgroupArray.tree}
              </Tree>
              <br />
            </Col>
          )}
          {concepts &&
            toShow.some((r) =>
              [
                "concepts",
                "house_concepts",
                "vehicle_concepts",
                "invoice_concepts",
                "stock_concepts",
                "paysheet_concepts",
              ].includes(r)
            ) && (
              <Col lg={12} md={4}>
                <Tree
                  content={texts.conceptsLabel[language]}
                  type={
                    conceptsArray.visible
                      ? "ALL"
                      : conceptsArray.filters.length
                      ? "SOME"
                      : "NONE"
                  }
                  canHide
                  visible={conceptsArray.visible}
                  onClick={(e) =>
                    handleClick(conceptsArray, 0, 2, "setConceptsArray", e)
                  }
                >
                  {conceptsArray.tree}
                </Tree>
                <br />
              </Col>
            )}
          {toShow.includes("types") && (
            <Col lg={12} md={4}>
              <Tree
                content={texts.typeLabel[language]}
                type={
                  typesArray.visible
                    ? "ALL"
                    : typesArray.filters.length
                    ? "SOME"
                    : "NONE"
                }
                canHide
                visible={typesArray.visible}
                onClick={(e) =>
                  handleClick(typesArray, 0, 1, "setTypesArray", e)
                }
              >
                {typesArray.tree}
              </Tree>
              <br />
            </Col>
          )}
          {accounts && toShow.includes("accounts") && (
            <Col lg={12} md={4}>
              <Tree
                content={texts.accountsLabel[language]}
                type={
                  accountsArray.visible
                    ? "ALL"
                    : accountsArray.filters.length
                    ? "SOME"
                    : "NONE"
                }
                canHide
                visible={accountsArray.visible}
                onClick={(e) =>
                  handleClick(accountsArray, 0, 1, "setAccountsArray", e)
                }
              >
                {accountsArray.tree}
              </Tree>
              <br />
            </Col>
          )}
          {vehicles && toShow.includes("vehicles") && (
            <Col lg={12} md={4}>
              <Tree
                content="Vehículos"
                type={
                  vehiclesArray.visible
                    ? "ALL"
                    : vehiclesArray.filters.length
                    ? "SOME"
                    : "NONE"
                }
                canHide
                visible={vehiclesArray.visible}
                onClick={(e) =>
                  handleClick(vehiclesArray, 0, 1, "setVehiclesArray", e)
                }
              >
                {vehiclesArray.tree}
              </Tree>
              <br />
            </Col>
          )}
          {houses && toShow.includes("houses") && (
            <Col lg={12} md={4}>
              <Tree
                content="Viviendas"
                type={
                  housesArray.visible
                    ? "ALL"
                    : housesArray.filters.length
                    ? "SOME"
                    : "NONE"
                }
                canHide
                visible={housesArray.visible}
                onClick={(e) =>
                  handleClick(housesArray, 0, 1, "setHousesArray", e)
                }
              >
                {housesArray.tree}
              </Tree>
              <br />
            </Col>
          )}
        </Row>
      </Card.Body>
    </Card>
  );
};

export default AccountRecord;
