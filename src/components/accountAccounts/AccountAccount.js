import React from "react";

import { Card, Col } from "react-bootstrap";

import { useHistory } from "react-router-dom";
import currencyFormater from "../../helpers/currencyFormater";

function importAll(r) {
  let images = {};
  r.keys().map((item, index) => {
    images[item.replace("./", "")] = r(item);
  });
  return images;
}
const images = importAll(
  require.context("../../img/accounts_images", false, /\.(png|jpe?g|svg)$/)
);

const AccountAccount = ({
  props,
  blurred,
  recordsCheckBoxHandler,
  showModalHandler,
}) => {
  const main_currency_symbol = sessionStorage.getItem("main_currency_symbol");
  const main_currency_format = sessionStorage.getItem("main_currency_format");
  const history = useHistory();

  const cardStyle = { 
    marginBottom: "0.1rem", 
    borderLeft: "5px solid " + props.color,
    opacity: blurred ? "0.75" : "1"
  };
  const nameStyle = {
    fontSize: "18px",
    display: "flex",
    alignItems: "center",
    //justifyContent: "center",
    fontWeight: "600",
    //textAlign: "left",
    padding: "0.75rem",
    wordBreak: "break-word",
    hyphens: "auto",
  };

  const symbolStyle = {
    fontWeight: "bold",
    display: "flex",
    alignItems: "center",
    // padding: "0.75rem",
  };
  const amountStyle = {
    fontSize: "20px",
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0.75rem",
  };

  const initialAmountStyle = {
    fontSize: "15px",
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0.75rem",
  };

  const handleClick = (e) => {
    history.push("/account/accounts/details/" + props._id);
  };

  return (
    <Card style={cardStyle} onClick={handleClick}>
      <Card.Body style={{ padding: "0px 10px", display: "flex" }}>
        <Col
          md={12}
          xs={12}
          style={{ display: "flex", padding: "0px", cursor: "pointer" }}
        >
          <Col md={2} xs={2} style={symbolStyle}>
            <img src={images[props.image]} style={{ height: "40px" }} />
          </Col>
          <Col md={3} xs={6} style={nameStyle}>
            {props.name}
          </Col>
          <Col md={3} className="d-none d-md-flex" style={initialAmountStyle}>
            <div>
              <p style={{ marginBottom: "0px" }}>
                <strong
                  style={{
                    color:
                      (props.initial_amount > 0
                        ? "#5eba00"
                        : props.initial_amount == 0
                        ? "#54c8ff"
                        : "#FF0000"),
                  }}
                >
                  {currencyFormater(
                    props.initial_amount,
                    props.format,
                    props.symbol
                  )}{" "}
                  → Inicial
                </strong>
              </p>
              {!props.has_main_currency && (
                <p style={{ marginBottom: "0px", fontSize: "12px" }}>
                  ~{" "}
                  {currencyFormater(
                    props.initial_amount * props.exchange,
                    main_currency_format,
                    main_currency_symbol
                  )}
                </p>
              )}
            </div>
          </Col>

          {/* <Col md={1} className="d-none d-md-flex"></Col> */}
          <Col md={4} xs={4} style={amountStyle}>
            <div>
              <p style={{ marginBottom: "0px" }}>
                <strong
                  style={{
                    color:
                      blurred ? "#495057" :
                        (props.amount > 0
                          ? "#5eba00"
                          : props.amount == 0
                          ? "#54c8ff"
                          : "#FF0000"),
                  }}
                >
                  {currencyFormater(props.amount, props.format, props.symbol)}
                </strong>
              </p>
              {!props.has_main_currency && (
                <p style={{ marginBottom: "0px", fontSize: "12px" }}>
                  ~{" "}
                  {currencyFormater(
                    props.amount * props.exchange,
                    main_currency_format,
                    main_currency_symbol
                  )}
                </p>
              )}
            </div>
          </Col>
        </Col>
      </Card.Body>
    </Card>
  );
};

export default AccountAccount;
