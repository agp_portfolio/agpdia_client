import React from "react";

import { Col, Row, Card } from "react-bootstrap";

import { Doughnut, Line, Bar } from "react-chartjs-2";

import { Form, Button } from "tabler-react";
import { useHistory } from "react-router-dom";
import currencyFormater from "../../helpers/currencyFormater";
import dynamicColors from "../../helpers/dynamicColors";
import arrayColumn from "../../helpers/arrayColumn";
import dateFormater from "../../helpers/dateFormater";
import fetchFunction from "../../helpers/fetchFuction";
import Select from "react-select";

import backButtonWithModal from "../../helpers/backButtonWithModal";
import AccountAccountForm from "./AccountAccountForm";
import ModalForm from "../ModalForm";

import Swal from "sweetalert2";
import dateLanguage from "../../helpers/dateLanguage";

const AccountAccountGraph = ({ props, updateSignal, changeSection }) => {
  const language = sessionStorage.getItem("language");
  // const main_currency_format = sessionStorage.getItem("main_currency_format");
  const [error, setError] = React.useState();
  const [graphData, setGraphData] = React.useState({});
  const [type, setType] = React.useState("day");
  const [categories, setCategories] = React.useState();
  const [concepts, setConcepts] = React.useState();
  const [graphType, setGraphType] = React.useState({
    label: "Línea",
    value: "line_graph_account",
  });
  const [graphTypeOptions, setGraphTypeOptions] = React.useState([
    { label: "Línea", value: "line_graph_account" },
    { label: "Barras", value: "bar_graph_account" },
  ]);
  const [barDataset, setBarDataset] = React.useState([]);
  const [groupByConcepts, setGroupByConcepts] = React.useState(false);
  const [prevProps, setPrevProps] = React.useState({});
  const [firstRender, setFirstRender] = React.useState(false);

  const cardStyle = {
    marginBottom: "0.1rem",
    borderLeft: "5px solid " + graphData.color,
  };

  React.useEffect(() => {
    // if (!firstRender) return;
    if (JSON.stringify(props) == JSON.stringify(prevProps)) return;
    setPrevProps(props);
  }, [props]);

  React.useEffect(() => {
    if (!firstRender) return;
    if (JSON.stringify(prevProps) == JSON.stringify({})) return;
    refetchGraph();
  }, [prevProps]);

  const fetchGraph = () => {
    var filter = {
      group: type,
      account_id: props.id,
      byConcept: groupByConcepts,
    };

    var [promise, abort] = fetchFunction("accounts/aggs/accounts/" + graphType.value, "POST", filter)
    promise.then((data) => {
      if (!data) {
        // No action
      } else {
        setGraphData(data);
      }
    });
  };

  const fetchCategories = () => {
    var [promise, abort] = fetchFunction("accounts/categories", "GET")
    promise.then((data) => {
      if (!data) {
        // No action
      } else {
        setCategories(data);
      }
    });
  };

  const fetchConcepts = () => {
    var [promise, abort] = fetchFunction("accounts/concepts", "GET")
    promise.then((data) => {
      if (!data) {
        // No action
      } else {
        setConcepts(data);
      }
    });
  };

  const refetchGraph = () => {
    fetchGraph();
  };

  const createDataSets = () => {

    var colors = dynamicColors(Object.keys(graphData.datasets).length);
    console.log(colors);
    var temp = [];
    var i = 0;

    Object.keys(graphData.datasets).forEach((key) => {
      if (groupByConcepts) {
        var label = concepts.filter((concept) => concept._id == key)[0].name;
      } else {
        var label = categories.filter((category) => category._id == key)[0]
          .name;
      }
      temp.push({
        type: "bar",
        label: label,
        fill: false,
        backgroundColor: colors[i][0],
        borderColor: colors[i][1], //This doesn't work with bar chart
        data: graphData.datasets[key],
      });
      i++;
    });
    setBarDataset(temp);

  };

  React.useEffect(() => {

    refetchGraph();
  }, [type, graphType, groupByConcepts]);

  React.useEffect(() => {
    if (!categories) fetchCategories();
    if (!concepts) fetchConcepts();
    if (graphType.value == "bar_graph_account") createDataSets();
  }, [graphData]);

  React.useEffect(() => {
    setFirstRender(true);
  }, []);

  return (
    <Col md={12} className="graphsAccounts p-0">
      <Card style={cardStyle}>
        <Card.Body style={{ padding: "10px", display: "flex" }}>
          <Col>
            {" "}
            <Row>
              <Col md={3}>
                <Select
                  onChange={(e) => {
                    setGraphType(e);
                  }}
                  isSearchable={false}
                  value={{
                    label: graphType ? graphType.label : "Selecciona tipo",
                  }}
                  options={graphTypeOptions}
                />
              </Col>
              <Col md={1}></Col>
              <Col md={3}>
                {graphType.value == "bar_graph_account" && (
                  <Form.Group>
                    <Form.SelectGroup
                      value={groupByConcepts}
                      onChange={(e) =>
                        setGroupByConcepts(e.target.value == "true")
                      }
                    >
                      <Form.SelectGroupItem
                        label="categorias"
                        name="groupByConcepts"
                        value={false}
                        checked={!groupByConcepts}
                        onChange={(e) => {}}
                      />
                      <Form.SelectGroupItem
                        label="conceptos"
                        name="groupByConcepts"
                        value={true}
                        checked={groupByConcepts}
                        onChange={(e) => {}}
                      />
                    </Form.SelectGroup>
                  </Form.Group>
                )}
              </Col>
              <Col md={1}></Col>

              <Col md={4} style={{ float: "right" }}>
                <Form.Group>
                  <Form.SelectGroup
                    value={type}
                    onChange={(e) => setType(e.target.value)}
                  >
                    <Form.SelectGroupItem
                      label="día"
                      name="type"
                      value="day"
                      checked={type == "day"}
                      onChange={(e) => {}}
                    />
                    <Form.SelectGroupItem
                      label="mes"
                      name="type"
                      value="month"
                      checked={type == "month"}
                      onChange={(e) => {}}
                    />
                    <Form.SelectGroupItem
                      label="año"
                      name="type"
                      value="year"
                      checked={type == "year"}
                      onChange={(e) => {}}
                    />
                  </Form.SelectGroup>
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col>
                {graphType.value == "line_graph_account" && (
                  <Line
                    data={{
                      labels: graphData.label,
                      datasets: [
                        {
                          label: "Balance",
                          data: graphData.balance,
                          fill: false,
                          backgroundColor: graphData.color,
                          borderColor: graphData.color,
                          //borderColor: graphData.color + "33",
                        },
                      ],
                    }}
                    options={{
                      legend: {
                        display: false,
                      },
                      elements: {
                        point: {
                          radius: 2,
                          hoverRadius: 3,
                        },
                        line: {
                          tension: 0,
                        },
                      },
                      //  aspectRatio: 3,
                      tooltips: {
                        callbacks: {
                          title: function (tooltipItem, data) {
                            var temp = new Date(
                              tooltipItem[0].label
                            ).toLocaleString("en-GB", {
                              year: "numeric",
                              month: "2-digit",
                              day: "2-digit",
                            });
                            switch (type) {
                              case "day":
                                return temp;
                                break;
                              case "month":
                                return temp.substr(3, 10);
                                break;
                              case "year":
                                return temp.substr(6, 10);
                                break;
                            }
                            return;
                          },

                          label: function (tooltipItem, data) {
                            return (
                              data.datasets[tooltipItem.datasetIndex].label +
                              ": " +
                              currencyFormater(
                                Number(tooltipItem.value),
                                graphData.format,
                                graphData.symbol
                              )
                            );
                          },
                          // footer: function (tooltipItem, data) {
                          //   //console.log(tooltipItem,data)
                          //   return ".............................................";
                          // },
                        },
                      },
                      scales: {
                        //     // We use this empty structure as a placeholder for dynamic theming.
                        xAxes: [
                          {
                            display: window.innerWidth < 500 ? false : true,
                            type: "time",
                            // time da problemas con el callback
                            time: {
                              displayFormats: {
                                millisecond: "DD",
                                second: "DD",
                                minute: "DD",
                                hour: "DD/MMM",
                                day: "MMM/YYYY",
                                week: "YYYY",
                                month: "YYYY",
                                quarter: "YYYY",
                                year: "YYYY",
                              },
                            },
                            gridLines: {
                              display: false,
                            },
                            ticks: {
                              maxTicksLimit: 7,
                              maxRotation: 0,
                              minRotation: 0,
                              callback: function (label, index, labels) {
                                return dateLanguage(label, language);
                              },
                            },
                            // offset: true,
                          },
                        ],
                        yAxes: [
                          {
                            //type: 'logarithmic',

                            type: "linear",

                            //stacked: true,
                            ticks: {
                              maxTicksLimit: 5,
                              // beginAtZero: true,
                              // fontSize: window.innerWidth < 500 ? 10 : 12,
                              userCallback: function (tick) {
                                // console.log(graphData);
                                return currencyFormater(
                                  tick,
                                  graphData.format,
                                  graphData.symbol
                                );
                              },
                            },
                            scaleLabel: {
                              //labelString: 'Voltage',
                              //display: false
                            },
                          },
                        ],
                      },
                    }}
                  />
                )}
                {graphType.value == "bar_graph_account" && (
                  <Bar
                    type="bar"
                    data={{
                      labels: graphData.label,
                      datasets: barDataset,
                    }}
                    options={{
                      legend: {
                        position: "bottom",
                        display: (graphData.datasets && window.innerWidth > 500)
                          ? Object.keys(graphData.datasets).length < 15
                          : false,
                      },
                      scales: {
                        xAxes: [
                          {
                            // barThickness: 12,  // number (pixels) or 'flex'
                            // maxBarThickness: 15, // number (pixels)
                            display: window.innerWidth < 500 ? false : true,
                            type: "time",
                            // // time da problemas con el callback
                            time: {
                              unit: type,
                              round: type,
                              displayFormats: {
                                millisecond: "DD",
                                second: "DD",
                                minute: "DD",
                                hour: "DD/MMM",
                                day: "DD/MMM",
                                week: "MMM/YYYY",
                                month: "MMM/YYYY",
                                quarter: "YYYY",
                                year: "YYYY",
                              },
                            },
                            gridLines: {
                              display: false,
                            },
                            ticks: {
                              maxTicksLimit: 7,
                              maxRotation: 0,
                              minRotation: 0,
                              callback: function (label, index, labels) {
                                return dateLanguage(label, language);
                              },
                            },
                            stacked: true,
                          },
                        ],
                        yAxes: [
                          {
                            type: "linear",

                            //stacked: true,
                            ticks: {
                              maxTicksLimit: 5,
                              // beginAtZero: true,
                              // fontSize: window.innerWidth < 500 ? 10 : 12,
                              userCallback: function (tick) {
                                // console.log(graphData);
                                return currencyFormater(
                                  tick,
                                  graphData.format,
                                  graphData.symbol
                                );
                              },
                            },
                            scaleLabel: {
                              //labelString: 'Voltage',
                              //display: false
                            },
                            stacked: true,
                          },
                        ],
                      },
                      tooltips: {
                        callbacks: {
                          title: function (tooltipItem, data) {
                            var temp = new Date(
                              tooltipItem[0].label
                            ).toLocaleString("en-GB", {
                              year: "numeric",
                              month: "2-digit",
                              day: "2-digit",
                            });
                            switch (type) {
                              case "day":
                                return temp;
                                break;
                              case "month":
                                return temp.substr(3, 10);
                                break;
                              case "year":
                                return temp.substr(6, 10);
                                break;
                            }
                            return;
                          },

                          label: function (tooltipItem, data) {
                            return (
                              data.datasets[tooltipItem.datasetIndex].label +
                              ": " +
                              currencyFormater(
                                Number(tooltipItem.value),
                                graphData.format,
                                graphData.symbol
                              )
                            );
                          },
                          // footer: function (tooltipItem, data) {
                          //   //console.log(tooltipItem,data)
                          //   return ".............................................";
                          // },
                        },
                      },
                    }}
                  />
                )}
              </Col>
            </Row>
          </Col>
        </Card.Body>
      </Card>
    </Col>
  );
};

export default AccountAccountGraph;
