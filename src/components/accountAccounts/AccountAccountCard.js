import React from "react";

import { Col, Row, Card } from "react-bootstrap";
import { Form, Button } from "tabler-react";
import { useHistory } from "react-router-dom";
import currencyFormater from "../../helpers/currencyFormater";
import fetchFunction from "../../helpers/fetchFuction";

import backButtonWithModal from "../../helpers/backButtonWithModal";
import AccountAccountForm from "../accountAccounts/AccountAccountForm";
import ModalForm from "../ModalForm";

import Swal from "sweetalert2";

function importAll(r) {
  let images = {};
  r.keys().map((item, index) => {
    images[item.replace("./", "")] = r(item);
  });
  return images;
}
const images = importAll(
  require.context("../../img/accounts_images", false, /\.(png|jpe?g|svg)$/)
);

const AccountAccountCard = ({ props, updateSignal, changeSection }) => {
  const main_currency_symbol = sessionStorage.getItem("main_currency_symbol");
  const main_currency_format = sessionStorage.getItem("main_currency_format");
  const [error, setError] = React.useState();
  const [account, setAccount] = React.useState({});
  const [balance, setBalance] = React.useState();
  const [prevProps, setPrevProps] = React.useState({});
  const [firstRender, setFirstRender] = React.useState(false);

  const history = useHistory();

  var modalRef = React.useRef();

  React.useEffect(() => {
    if (!firstRender) return;
    if (JSON.stringify(props) == JSON.stringify(prevProps)) return;
    setPrevProps(props);
  }, [props]);

  const backButtonStyle = {
    height: "2.5rem",
    width: "2.5rem",
    background:
      "#fff url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI0MiIgaGVpZ2h0PSI0MiIgdmlld0JveD0iNCAxIDM2IDM2Ij48cGF0aCBmaWxsLXJ1bGU9Im5vbnplcm8iIHN0cm9rZT0iIzM1NDA1MiIgc3Ryb2tlLXdpZHRoPSIyIiBkPSJNMjQuMzIgMjdhLjY2My42NjMgMCAwMS0uNDgtLjIwNGwtNi42NDItNi44MDRhLjcxLjcxIDAgMDEwLS45ODVsNi42NDEtNi44MDNhLjY2OS42NjkgMCAwMS45NjIgMCAuNzA4LjcwOCAwIDAxMCAuOTg0TDE4LjY0IDE5LjVsNi4xNiA2LjMxM2EuNzEuNzEgMCAwMTAgLjk4NS42NzIuNjcyIDAgMDEtLjQ4MS4yMDJ6IiBmaWxsPSJub25lIi8+PC9zdmc+) 50%/contain",
    cursor: "pointer",
    borderRadius: "6px",
    boxShadow: "0 2px 3px 0 rgba(0,0,0,.2)",
    marginRight: "10px",
  };

  const rowStyle = {
    width: "100%",
    margin: "0.75rem",
  };

  const cardStyle = {
    marginBottom: "0.1rem",
    borderLeft: "5px solid " + account.color,
  };

  const nameStyle = {
    fontSize: "18px",
    display: "flex",
    alignItems: "center",
    //justifyContent: "center",
    fontWeight: "600",
    //textAlign: "left",
    // padding: "0.75rem",
    wordBreak: "break-word",
    hyphens: "auto",
  };

  const symbolStyle = {
    fontWeight: "bold",
    display: "flex",
    alignItems: "center",
    // padding: "0.75rem",
  };
  const activeLinkStyle = {
    fontWeight: "700",
    color: account.color,
    borderColor: account.color,
  };

  const infoTypeStyle = {
    fontSize: "14px",
    fontWeight: 400,
    lineHeight: 1.14,
    color: "#7f8fa4",
  };
  const infoStyle = {
    fontSize: "16px",
    fontWeight: 600,
    lineHeight: 1.14,
    color: "#354052",
  };

  const functionButtonStyle = {
    fontSize: "16px",
    fontWeight: 600,
    lineHeight: 1.14,
    color: "#354052",
    textAlign: "end",
  };

  const fetchAccount = () => {
    var [promise, abort] = fetchFunction("accounts/accounts/" + props.id, "GET")
    promise.then((data) => {
      if (!data) {
        // No action
      } else {
        if (data.message) redirect();
        setAccount(data);
      }
    });  
  };

  const fetchBalance = () => {
    var [promise, abort] = fetchFunction("accounts/aggs/accounts/balance/" + props.id, "GET")
    promise.then((data) => {
      if (!data) {
        // No action
      } else {
        data ? setBalance(data) : setBalance(0);
      }
    });  
  };

  const redirect = (e) => {
    history.push("/account/accounts/");
  };
  const refetchAccount = () => {
    fetchAccount();
  };
  const refetchBalance = () => {
    fetchBalance();
  };
  const deleteItems = () => {
    Swal.fire({
      title: "¿Estas seguro de que proceder con la eliminación?",
      // text: `${props.selected.length} ${
      //   props.selected.length == 1 ? " registro" : " registros"
      // } a eliminar`,
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, !Borrar!",
    }).then((result) => {
      if (!result.value) return;
      var [promise, abort] = fetchFunction("accounts/accounts/" + props.id, "DELETE")
      promise.then((data) => {
        if (!data) {
          Swal.fire({
            text: `No se han encontrado los registros a eliminar o esta en uso`,
            icon: "error",
            timer: "2000",
          });
        } else {
          Swal.fire({
            text: `El registro se ha eliminado`,
            icon: "success",
            timer: "2000",
          });
          history.push("/account/accounts/");
        }
      });

    });
  };

  const modalSaveHandler = () => {
    modalRef.current.handleHide();
    updateSignal();
  };

  const handleEdit = (toAdd) => {
    backButtonWithModal.neutralizeBack(modalRef.current);
    modalRef.current.handleShow();
    modalRef.current.task("objectId", toAdd);
  };

  React.useEffect(() => {
    refetchAccount();
    refetchBalance();
    setFirstRender(true);
  }, []);

  React.useEffect(() => {
    if (JSON.stringify(prevProps) == JSON.stringify({})) return;
    refetchAccount();
    refetchBalance();
  }, [prevProps]);
  return (
    <Card style={cardStyle}>
      <Card.Header>
        <Row style={{ width: "100%" }}>
          <Col
            lg={10}
            md={8}
            xs={12}
            style={{ display: "flex", alignItems: "center" }}
          >
            <div style={backButtonStyle} onClick={redirect}></div>
            <h3 style={{ margin: "inherit" }}>Detalles de la cuenta</h3>
          </Col>

          <Col lg={1} md={2} xs={9} style={functionButtonStyle}>
            <Button pill outline color="warning" onClick={() => handleEdit()}>
              Editar
            </Button>
          </Col>
          <Col lg={1} md={2} xs={3} style={functionButtonStyle}>
            <Button pill outline color="danger" onClick={(e) => deleteItems(e)}>
              Borrar
            </Button>
          </Col>
        </Row>
      </Card.Header>
      <Card.Body style={{ padding: "10px", display: "flex" }}>
        {account && (
          <Row style={rowStyle}>
            {/* <Col md={3} xs={5} style={symbolStyle}> */}
            <img
              src={images[account.image]}
              style={{ margin: "0px 30px", maxHeight: "100px" }}
            />
            {/* </Col>
            <Col md={9} xs={7} style={nameStyle}> */}
            <div>
              <div style={{ marginBottom: "15px" }}>
                <div style={infoTypeStyle}>Nombre</div>
                <div style={infoStyle}>
                  <span>{account.name}</span>
                </div>
              </div>
              <div>
                <div style={infoTypeStyle}>Cantidad</div>
                <div style={infoStyle}>
                  Inicial →{" "}
                  <strong
                    style={{
                      color:
                        account.initial_amount > 0
                          ? "#5eba00"
                          : account.initial_amount == 0
                          ? "#54c8ff"
                          : "#FF0000",
                    }}
                  >
                    {currencyFormater(
                      account.initial_amount,
                      account.format,
                      account.symbol
                    )}
                  </strong>
                </div>
                <div style={infoStyle}>
                  Actual →{" "}
                  <strong
                    style={{
                      color:
                        account.initial_amount + balance > 0
                          ? "#5eba00"
                          : account.initial_amount + balance == 0
                          ? "#54c8ff"
                          : "#FF0000",
                    }}
                  >
                    {currencyFormater(
                      account.initial_amount + balance,
                      account.format,
                      account.symbol
                    )}
                  </strong>
                </div>
              </div>
            </div>
            {/* </Col> */}
          </Row>
        )}
      </Card.Body>
      <Card.Footer style={{ padding: "0px 24px" }}>
        <div className="row row align-items-center">
          {/* <div className="col-lg-3 ml-auto"></div> */}
          <div className="col col-md order-md-first">
            <ul className="nav nav-tabs border-0 flex-column flex-md-row forced-row">
              <li className="nav-item">
                <a
                  className="nav-link" onClick={()=>changeSection('graphs')}
                  style={props.section == "graphs" ? activeLinkStyle : {}}
                >
                  <i className="fa fa-bar-chart"></i> Gráficos
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link" onClick={()=>changeSection('records')}
                  style={props.section == "records" ? activeLinkStyle : {}}
                >
                  <i className="fa fa-server"></i> Registros
                </a>
              </li>
            </ul>
          </div>
        </div>
      </Card.Footer>
      <ModalForm
        ref={modalRef}
        props={{
          title: "Editar",
        }}
      >
        <AccountAccountForm
          props={{
            objectId: account._id,
            currencies: props.currencies,
            modalType: "Update",
          }}
          modalSaveHandler={modalSaveHandler}
        />
      </ModalForm>
    </Card>
  );
};

export default AccountAccountCard;
