import React, { useEffect, useState, useRef } from "react";

import { Modal, Col, Row } from "react-bootstrap";
import DateRangePicker from "react-bootstrap-daterangepicker";
import { Form, Button } from "tabler-react";
import Select from "react-select";
import fetchFunction from "../../helpers/fetchFuction";

import InputColor from "react-input-color";
import ImagePicker from "react-image-picker";
import "react-image-picker/dist/index.css";

function importAll(r) {
  let images = {};
  r.keys().map((item, index) => {
    images[item.replace("./", "")] = r(item);
  });
  return images;
}
const images = importAll(
  require.context("../../img/accounts_images", false, /\.(png|jpe?g|svg)$/)
);

const AccountAccountForm = ({ props, modalSaveHandler }) => {
  const language = sessionStorage.getItem("language");
  const [currencySymbol, setCurrencySymbol] = useState("€");
  const [initialAccount, setInitialAccount] = useState(false);
  const [fetchedAccount, setFetchedAccount] = useState(false);
  const [newAccount, setNewAccount] = useState(false);
  const [validForm, setValidForm] = useState();

  const [accountsOptions, setAccountsOptions] = useState();
  const [currencyOptions, setCurrencyOptions] = useState();

  const [imagesOptions, setImagesOptions] = useState([]);
  // const [conceptsOptions, setConceptsOptions] = useState();
  const [allConceptsOptions, setAllConceptsOptions] = useState();

  const [category, setCategory] = useState();
  const [group, setGroup] = useState();
  const [subgroup, setSubgroup] = useState();
  const [consumptionU, setConsumptionU] = useState();

  const [name, setName] = useState();
  const [initialAmount, setInitialAmount] = useState();
  const [color, setColor] = useState("");
  const [currency, setCurrency] = useState();
  const [image, setImage] = useState();
  const [statistics, setStatistics] = useState();

  const keyRef = useRef(Date.now()); // Needed to update the date
  keyRef.current = Date.now(); // Needed to update the date

  const fetchAccount = (objectId) => {
    var [promise, abort] = fetchFunction("accounts/accounts/" + objectId, "GET")
    promise.then((data) => {
      if (!data) {
        // No action
      } else {
        setFetchedAccount(data);
        setFields(data);
      }
    });
  };

  const handleSave = () => {
    var processedNewAccount = newAccount;

    switch (props.modalType) {
      case "Add":
        var [promise, abort] = fetchFunction("accounts/accounts/", "POST", newAccount)
        promise.then((data) => {
          if (!data) {
            // No action
          } else {
            modalSaveHandler();
          }
        });
        break;
      case "Update":
        var objectId = props.objectId;
        var [promise, abort] = fetchFunction(
          "accounts/accounts/" + objectId,
          "PUT",
          processedNewAccount
        )
        promise.then((data) => {
          if (!data) {
            // No action
          } else {
            modalSaveHandler();
          }
        });
        break;

      default:
        break;
    }
  };

  const setFields = (data) => {
    console.log(initialAccount, newAccount)
    if (data) {
      setName(data.name);
      setInitialAmount(data.initial_amount);
      setColor(data.color);
      setCurrency({
        label: data.symbol + "→" + data.code,
        value: data.currency_id,
      });
      setImage(data.image);
      setStatistics(data.statistics)
      var backData = {
        name: data.name,
        initial_amount: data.initial_amount && data.initial_amount.toString().replace(",", "."),
        currency_id: data.currency_id,
        color: data.color,
        image: data.image,
        statistics: data.statistics,
      };
    } else {
      setName();
      setInitialAmount();
      setColor("#70bbe6");
      // setCurrency();
      setImage();
      setStatistics("general")
      var backData = {
        name: undefined,
        initial_amount: undefined,
        currency_id: props.currencies.find(element => element.is_main)._id,
        color: "#70bbe6",
        image: undefined,
        statistics: "all",
      };

    }
    setInitialAccount(backData);
    setNewAccount(backData);
  };

  React.useEffect(() => {
    if (props.currencies) {
      var tempCurrencies = [];
      props.currencies.forEach((currency) => {
        if (currency.is_main){
          setCurrency({
            label: currency.symbol + "→" + currency.code,
            value: currency._id,
          });
        }
        tempCurrencies.push({
          label: currency.symbol + "→" + currency.code,
          value: currency._id,
        });
      });
      
      setCurrencyOptions(tempCurrencies);
    }
    if (props.modalType == "Update") {
      fetchAccount(props.objectId);
    } else if (props.modalType == "Add") {
      setFields();
    }

    if (!imagesOptions.length) {

      var tempImages = [];
      Object.keys(images).forEach((image) => {
        tempImages.push(images[image]);
      });
      setImagesOptions(tempImages);
    }
  }, []);

  React.useEffect(
    () => {
      var imageName = image ? Object.keys(images).find(key => images[key] === image.src) : undefined
      var backData = {
        name: name,
        initial_amount: initialAmount && initialAmount.toString().replace(",", "."),
        currency_id: currency ? currency.value : undefined,
        color: color,
        image: imageName,
        statistics: statistics,
      };
       if (
        name &&
        /^(\-)?[0-9]{0,9}([\.\,]([0-9]{1,2})?)?$/i.test(initialAmount) &&
        currency &&
        image
      ) {
        setValidForm(true);
      } else {
        setValidForm(false);
      }
      setNewAccount(backData);
    },
    [
      name,
      initialAmount,
      color,
      currency,
      image,
      statistics,
    ]
  );

  return (
    <>
      <Row style={{ margin: "0px" }} className="accountForm coloredForm">
        <Col md={12} style={{ padding: "0px" }}>
          <div
            style={{
              background: "rgb(144, 164, 174)",
              color: "white",
              padding: "1rem",
            }}
          >
            <Row>
              <Col md={4}>
                <Form.Group label="Nombre *" style={{ paddingRight: "10px" }}>
                  <Form.Input
                    name="name"
                    className="input"
                    placeholder="Nombre..."
                    autoComplete="off"
                    type="text"
                    value={name}
                    onChange={(e) => {
                      setName(e.target.value);
                    }}
                  />
                </Form.Group>
              </Col>
              <Col md={3}>
                <Form.Group
                  label="Importe inicial *"
                  className="field-amount"
                  style={{ paddingRight: "10px" }}
                >
                  <Form.Input
                    name="initialAmount"
                    className="input"
                    placeholder="0,00"
                    autoComplete="off"
                    type="text"
                    value={
                      initialAmount != undefined
                        ? initialAmount.toString().replace(".", ",")
                        : ""
                    }
                    onChange={(e) => {
                      var temp = e.target.value;

                      if (/^(\-)?[0-9]{0,9}([\.\,]([0-9]{1,2})?)?$/i.test(temp))
                        temp ? setInitialAmount(temp) : setInitialAmount();
                    }}
                  />
                </Form.Group>
              </Col>


              <Col md={3}>
                <Form.Group label="Moneda *">
                  <div style={{ color: "rgb(73, 80, 87)", width: "100%" }}>
                    <Select
                      onChange={(e) => {
                        setCurrency(e);
                      }}
                      isSearchable={false}
                      value={{
                        label: currency ? currency.label : "Selecciona moneda",
                      }}
                      isDisabled
                      options={currencyOptions}
                    />
                  </div>
                </Form.Group>
              </Col>
              <Col md={2}>
                <Form.Group label="Color*">
                  <InputColor
                    className="form-control"
                    initialValue={color}
                    onChange={(e) => {
                      setColor(e.hex);
                    }}
                    placement="right"
                  />
                </Form.Group>
              </Col>

              <Col md={12}>
                <Form.Group label={props.modalType == 'Add' ? 'Imagen *':'Imagen'}>
                  <ImagePicker
                    images={imagesOptions.map((image, i) => ({
                      src: image,
                      value: i,
                    }))}

                    onPick={(e) => {setImage(e)}}
                  />
                </Form.Group>
              </Col>
              <Col md={12}>
                    <Form.Group className="type" label ="Graficos y totales">
                      <Form.SelectGroup
                        value={statistics}
                        onChange={(e) => {
                          setStatistics(e.target.value);
                        }}
                      >
                        <Form.SelectGroupItem
                          label="Acciones"
                          name="statistics"
                          value="stock"
                          checked={statistics == "stock"}
                          onChange={(e) => {}}
                        />
                        <Form.SelectGroupItem
                          label="General"
                          name="statistics"
                          value="general"
                          checked={statistics == "general"}
                          onChange={(e) => {}}
                        />
                      </Form.SelectGroup>
                    </Form.Group>
                </Col>
            </Row>
          </div>
        </Col>
      </Row>
      <Row style={{ margin: "0px" }}>
        <Col md={12} style={{ height: "10px" }}></Col>
      </Row>
      {newAccount &&
      JSON.stringify(initialAccount) != JSON.stringify(newAccount) ? (
        <Row className="form-buttons" style={{ margin: "0px" }}>
          <Col md={12} style={{ padding: "0px", height: "60px" }}>
            <Row className="form-buttons" style={{ padding: "inherit" }}>
              <Col xs={6}>
                <div style={{ float: "right" }}>
                  <Button
                    pill
                    outline
                    color="info"
                    onClick={() => setFields(fetchedAccount)}
                  >
                    Deshacer
                  </Button>
                </div>
              </Col>
              <Col xs={6}>
                {validForm && (
                  <div style={{ float: "left" }}>
                    <Button pill color="success" onClick={() => handleSave()}>
                      Guardar
                    </Button>
                  </div>
                )}
              </Col>
            </Row>
          </Col>
        </Row>
      ) : (
        <Row style={{ height: "60px", margin: "0" }}>
          <Col md={12} style={{ padding: "0px", height: "60px" }}></Col>
        </Row>
      )}
    </>
  );
};

export default AccountAccountForm;
