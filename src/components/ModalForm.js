import React, { forwardRef, useImperativeHandle, useState } from "react";

import { Button, Modal } from "react-bootstrap";

import AccountRecordForm from "./accountRecords/AccountRecordForm";

import backButtonWithModal from "../helpers/backButtonWithModal";

const ModalForm = forwardRef(({ props, children }, ref) => {

  const [show, setShow] = useState(false);
  const [form, setForm] = useState();

  useImperativeHandle(ref, () => ({
    handleShow() {
      setShow(true);
    },

    handleHide() {
      handleClose();
    },

    task(objectId, toEdit, whichOne) {
      setForm(whichOne);
    },
  }));

  const handleClose = () => {

    backButtonWithModal.revivalBack();
    setShow(false);
  };

  return (
    <>
      <Modal show={show} onHide={handleClose} dialogClassName={props.className}>
        <Modal.Header closeButton>
          <Modal.Title>{props.title}</Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ padding: "0px" }}>{children}</Modal.Body>
      </Modal>
    </>
  );
});

export default ModalForm;
