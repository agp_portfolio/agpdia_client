import React, { useEffect, useState, useRef } from "react";

import { Modal, Col, Row } from "react-bootstrap";
import DateRangePicker from "react-bootstrap-daterangepicker";
import { Form, Button } from "tabler-react";
import Select from "react-select";
import fetchFunction from "../../helpers/fetchFuction";
import dateFormater from "../../helpers/dateFormater";
import textsCalendar from "../../texts/Calendar";

import InputColor from "react-input-color";

const TravelTravelForm = ({ props, modalSaveHandler }) => {
  const language = sessionStorage.getItem("language");
  const [currencySymbol, setCurrencySymbol] = useState("€");
  const [initialTravel, setInitialTravel] = useState(false);
  const [fetchedTravel, setFetchedTravel] = useState(false);
  const [newTravel, setNewTravel] = useState(false);
  const [validForm, setValidForm] = useState();

  const [accountsOptions, setTravelsOptions] = useState();
  const [currencyOptions, setCurrencyOptions] = useState();
  const [countryOptions, setCountryOptions] = useState();
  const [regionOptions, setRegionOptions] = useState();

  // const [conceptsOptions, setConceptsOptions] = useState();
  const [allConceptsOptions, setAllConceptsOptions] = useState();

  const [continent, setContinent] = useState();
  const [initialContinent, setInitialContinent] = useState();
  const [country, setCountry] = useState();
  const [initialCountry, setInitialCountry] = useState();
  const [region, setRegion] = useState();
  const [place, setPlace] = useState();
  const [longitude, setLongitude] = useState();
  const [latitude, setLatitude] = useState();
  const [annotation, setAnnotation] = useState();
  const [date, setDate] = useState();
  const [isScheduled, setIsScheduled] = useState();

  const keyRef = useRef(Date.now()); // Needed to update the date
  keyRef.current = Date.now(); // Needed to update the date

  const fetchTravel = (objectId) => {
    var [promise, abort] = fetchFunction("travels/travels/" + objectId, "GET");
    promise.then((data) => {
      if (!data) {
        // No action
      } else {
        setFetchedTravel(data);
        setFields(data);
      }
    });
  };

  const handleSave = () => {
    var processedNewTravel = newTravel;
    processedNewTravel.date = dateFormater(processedNewTravel.date, "database");
    switch (props.modalType) {
      case "Add":
        case "Copy":
        var [promise, abort] = fetchFunction(
          "travels/travels/",
          "POST",
          newTravel
        );
        promise.then((data) => {
          if (!data) {
            // No action
          } else {
            modalSaveHandler();
          }
        });
        break;
      case "Update":
        var objectId = props.objectId;
        var [promise, abort] = fetchFunction(
          "travels/travels/" + objectId,
          "PUT",
          processedNewTravel
        );
        promise.then((data) => {
          if (!data) {
            // No action
          } else {
            modalSaveHandler();
          }
        });
        break;

      default:
        break;
    }
  };

  const setFields = (data) => {
    if (data) {
      setDate(dateFormater(data.date, "show"));
      setContinent({
        label: data.continent,
        value: data.continentCode,
      });
      setCountry({
        label: data.country,
        value: data.countryCode,
      });
      setRegion({
        label: data.region,
        value: data.regionCode,
      });
      setPlace(data.place);
      setLatitude(data.latitude);
      setLongitude(data.longitude);
      setAnnotation(data.annotation);
      setIsScheduled(data.is_scheduled);
      var backData = {
        date: dateFormater(data.date, "show"),
        continent: data.continent,
        continentCode: data.continentCode,
        country: data.country,
        countryCode: data.countryCode,
        region: data.region,
        regionCode: data.regionCode,
        place: data.place,
        latitude: data.latitude,
        longitude: data.longitude,
        annotation: data.annotation,
        is_scheduled: data.is_scheduled,
      };
    } else {
      setDate(dateFormater(new Date(), "show"));
      setContinent({
        label: initialContinent.label,
        value: initialContinent.value,
      });
      setCountry({
        label: initialCountry.label,
        value: initialCountry.value,
      });
      setRegion();
      setPlace();
      setLatitude();
      setLongitude();
      setAnnotation();
      setIsScheduled(false);
      var backData = {
        date: dateFormater(new Date(), "show"),
        continent: initialContinent.label,
        continentCode: initialContinent.value,
        country: initialCountry.label,
        countryCode: initialCountry.value,
        region: undefined,
        regionCode: undefined,
        place: undefined,
        latitude: undefined,
        longitude: undefined,
        annotation: undefined,
        is_scheduled: false,
      };
    }
    setInitialTravel(backData);
    setNewTravel(backData);
  };

  React.useEffect(() => {
    if (!initialContinent || !initialCountry) return;
    if (props.modalType == "Update" || props.modalType == "Copy") {
      fetchTravel(props.objectId);
    } else if (props.modalType == "Add") {
      setFields();
    }
  }, [initialContinent, initialCountry]);
  React.useEffect(() => {
    setInitialContinent(
      props.continents.find((element) => element.value == "EU")
    );
    setInitialCountry(props.countries.find((element) => element.value == "ES"));
  }, []);
  React.useEffect(() => {
    var backData = {
      date: date,
      continent: continent ? continent.label : undefined,
      continentCode: continent ? continent.value : undefined,
      country: country ? country.label : undefined,
      countryCode: country ? country.value : undefined,
      region: region ? region.label : undefined,
      regionCode: region ? region.value : undefined,
      place: place,
      latitude: latitude,
      longitude: longitude,
      annotation: annotation,
      is_scheduled: isScheduled,
    };
    if (date && continent && country && place) {
      setValidForm(true);
    } else {
      setValidForm(false);
    }

    setNewTravel(backData);
  }, [
    date,
    continent,
    country,
    region,
    place,
    longitude,
    latitude,
    annotation,
    isScheduled,
  ]);
  React.useEffect(() => {
    if (!props.countries) return;

    var countryOptions = [];
    var continentCode = continent ? continent.value : "EU";

    countryOptions = props.countries.filter(
      (country) => country.parent == continentCode
    );
    setCountryOptions(countryOptions);
  }, [continent]);
  React.useEffect(() => {
    if (!props.regions) return;

    var regionOptions = [];
    var countryCode = country ? country.value : "ES";
    regionOptions = props.regions.filter(
      (region) => region.parent == countryCode
    );
    setRegionOptions(regionOptions);
  }, [country]);

  return (
    <>
      <Row style={{ margin: "0px" }} className="accountForm coloredForm">
        <Col md={12} style={{ padding: "0px" }}>
          <div
            style={{
              background: "rgb(144, 164, 174)",
              color: "white",
              padding: "1rem",
            }}
          >
            <Row>
              <Col md={6}>
                <Form.Group label={"Fecha *"}>
                  <DateRangePicker
                    key={keyRef.current} // Needed to update the date
                    initialSettings={{
                      singleDatePicker: true,
                      opens: "center",
                      startDate: date,
                      locale: {
                        format: "DD/MM/YYYY",
                        firstDay: 1,
                        cancelLabel: textsCalendar.cancelButton[language],
                        applyLabel: textsCalendar.applyButton[language],
                        todayLabel: "Hoy",
                        customRangeLabel:
                          textsCalendar.customRangeLabel[language],
                        daysOfWeek: textsCalendar.daysOfWeek[language],
                        monthNames: textsCalendar.monthNames[language],
                      },
                    }}
                    onCallback={(e) => setDate(dateFormater(e._d, "show"))}
                    onShow={(e) => {
                      e.target.blur();
                      // if (!$(".daterangepicker .drp-buttons .todayBtn").length)
                      //   $(".daterangepicker .drp-buttons").prepend(
                      //     `<button class="todayBtn btn btn-sm btn-default" type="button" onClick="setDate()" >Hoy</button>`
                      //   );
                      // console.log($(".daterangepicker .drp-buttons .todayBtn"));
                    }}
                  >
                    <input
                      type="text"
                      className="form-control"
                      style={{ textAlign: "center" }}
                    />
                  </DateRangePicker>
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group label="Continente *">
                  <div style={{ color: "rgb(73, 80, 87)", width: "100%" }}>
                    <Select
                      onChange={(e) => {
                        setContinent(e);
                        setCountry({});
                        setRegion({});
                      }}
                      isSearchable={false}
                      value={continent ? continent : initialContinent}
                      options={props.continents}
                    />
                  </div>
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group label="País *">
                  <div style={{ color: "rgb(73, 80, 87)", width: "100%" }}>
                    <Select
                      onChange={(e) => {
                        setCountry(e);
                        setRegion({});
                      }}
                      // isSearchable={true}
                      value={continent ? country : initialCountry}
                      options={countryOptions}
                    />
                  </div>
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group label="Provincia/Region">
                  <div style={{ color: "rgb(73, 80, 87)", width: "100%" }}>
                    <Select
                      onChange={(e) => {
                        setRegion(e);
                      }}
                      // isSearchable={true}
                      value={region}
                      options={regionOptions}
                    />
                  </div>
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group label="Lugar *" style={{ paddingRight: "10px" }}>
                  <Form.Input
                    name="place"
                    className="input"
                    placeholder="Lugar..."
                    autoComplete="off"
                    type="text"
                    value={place}
                    onChange={(e) => {
                      setPlace(e.target.value);
                    }}
                  />
                </Form.Group>
              </Col>
              <Col md={3}>
                <Form.Group
                  label="Latitud"
                  className="field-amount"
                  style={{ paddingRight: "10px" }}
                >
                  <Form.Input
                    name="latitud"
                    className="input"
                    placeholder="00.0000000"
                    autoComplete="off"
                    type="number"
                    value={latitude}
                    onChange={(e) => {
                      setLatitude(e.target.value);
                    }}
                  />
                </Form.Group>
              </Col>
              <Col md={3}>
                <Form.Group
                  label="Longitud"
                  className="field-amount"
                  style={{ paddingRight: "10px" }}
                >
                  <Form.Input
                    name="longitud"
                    className="input"
                    placeholder="00.0000000"
                    autoComplete="off"
                    type="number"
                    value={longitude}
                    onChange={(e) => {
                      setLongitude(e.target.value);
                    }}
                  />
                </Form.Group>
              </Col>

              <Col md={9}>
                <Form.Group label="Nota" style={{ paddingRight: "10px" }}>
                  <Form.Input
                    name="annotation"
                    className="input"
                    placeholder="Nota..."
                    autoComplete="off"
                    type="text"
                    value={annotation ? annotation : ""}
                    onChange={(e) => {
                      setAnnotation(e.target.value);
                    }}
                  />
                </Form.Group>
              </Col>
              <Col md={3}>
                <Form.Group className="type" label="Pendiente">
                  <Form.SelectGroup
                    value={isScheduled}
                    onChange={(e) => {
                      setIsScheduled(eval(e.target.value));
                    }}
                  >
                    <Form.SelectGroupItem
                      label="Si"
                      name="isScheduled"
                      value={true}
                      checked={isScheduled}
                      onChange={(e) => {}}
                    />
                    <Form.SelectGroupItem
                      label="No"
                      name="isScheduled"
                      value={false}
                      checked={!isScheduled}
                      onChange={(e) => {}}
                    />
                  </Form.SelectGroup>
                </Form.Group>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
      <Row style={{ margin: "0px" }}>
        <Col md={12} style={{ height: "10px" }}></Col>
      </Row>
      {newTravel &&
      JSON.stringify(initialTravel) != JSON.stringify(newTravel) ? (
        <Row className="form-buttons" style={{ margin: "0px" }}>
          <Col md={12} style={{ padding: "0px", height: "60px" }}>
            <Row className="form-buttons" style={{ padding: "inherit" }}>
              <Col xs={6}>
                <div style={{ float: "right" }}>
                  <Button
                    pill
                    outline
                    color="info"
                    onClick={() => setFields(fetchedTravel)}
                  >
                    Deshacer
                  </Button>
                </div>
              </Col>
              <Col xs={6}>
                {validForm && (
                  <div style={{ float: "left" }}>
                    <Button pill color="success" onClick={() => handleSave()}>
                      Guardar
                    </Button>
                  </div>
                )}
              </Col>
            </Row>
          </Col>
        </Row>
      ) : (
        <Row style={{ height: "60px", margin: "0" }}>
          <Col md={12} style={{ padding: "0px", height: "60px" }}></Col>
        </Row>
      )}
    </>
  );
};

export default TravelTravelForm;
