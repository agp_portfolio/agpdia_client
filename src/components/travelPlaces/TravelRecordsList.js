// @flow

import React, { useEffect } from "react";
import fetchFunction from "../../helpers/fetchFuction";
import { Col, Row, Card } from "react-bootstrap";
import { Doughnut, Line, Bar } from "react-chartjs-2";
import dateLanguage from "../../helpers/dateLanguage";
import { Form, Button } from "tabler-react";
import Select from "react-select";
import moment from "moment";
import ModalForm from "../ModalForm";
import TravelPlace from './TravelPlace'
import TravelPlaceForm from './TravelPlaceForm'
import TravelPlaceTotal from './TravelPlaceTotal'
import backButtonWithModal from "../../helpers/backButtonWithModal";
import { each } from "jquery";

function TravelRecordsList({ props, updateSignal, refetchTravels }) {
  const [itemsToForm, setItemsToForm] = React.useState([]);
  const [travels, setTravels] = React.useState();
  const [continents, setContinents] = React.useState();
  const [countries, setCountries] = React.useState();
  const [regions, setRegions] = React.useState();
  const [filter, setFilter] = React.useState();
  const [travelsTotal, setTravelsTotal] = React.useState(false);
  const [travelsData, setTravelsData] = React.useState();
  const [modalTitle, setModalTitle] = React.useState("");
  const [modalType, setModalType] = React.useState();

  var modalRef = React.useRef();

  const fetchContinents = () => {
    var [promise, abort] = fetchFunction("continents/", "GET")
    promise.then((data) => {
      if (!data) {
        setContinents();
      } else {
        var continentSelect = [] 
        data.forEach((continent) =>{
          continentSelect.push({
            label: continent.name,
            value: continent.code
          })
        })
        setContinents(continentSelect);
      }
    });
  };

  const fetchCountries = () => {
    var [promise, abort] = fetchFunction("countries/", "GET")
    promise.then((data) => {
      if (!data) {
        setCountries();
      } else {
        var countrySelect = [] 
        data.forEach((country) =>{
          countrySelect.push({
            parent: country.continent,
            label: country.name,
            value: country.code
          })
        })
        setCountries(countrySelect);
      }
    });
  };
  const fetchRegions = () => {
    var [promise, abort] = fetchFunction("regions/", "GET")
    promise.then((data) => {
      if (!data) {
        setRegions();
      } else {
        var regionSelect = [] 
        data.forEach((region) =>{
          regionSelect.push({
            parent: region.country,
            label: region.name,
            value: region.code
          })
        })
        setRegions(regionSelect);
      }
    });
  };
  const makeTravelsArray = () => {
    var travelsTable = [];

    setTravelsTotal(
      <TravelPlaceTotal
        key={new Date().getTime()}
        props={{
          continents: undefined,
          countries:undefined,
          regions:undefined,
          places:undefined,
        }}
        showModalHandler={showModalHandler}
      />);
    props.records.forEach((travel) => {

      travelsTable.push(
          <TravelPlace
            key={travel.id}
            props={travel}
            showModalHandler={showModalHandler}
            refetchTravels={refetchTravels}
          />
        );
    });
    setTravels(travelsTable);
  };

  const modalSaveHandler = () => {
    modalRef.current.handleHide();

    if (updateSignal) {
      updateSignal();
    }
    refetchTravels();
  };

  const showModalHandler = (objectId, toAdd) => {
    objectId ? setItemsToForm([objectId]) : setItemsToForm();
    console.log(objectId, toAdd)
    if (toAdd) {
      if(objectId){
        setModalTitle("Copiar elemento");
        setModalType("Copy");
        console.log('asdf')
      }else{
        setModalTitle("Añadir elemento");
        setModalType("Add");
      }
    } else{
      setModalTitle("Editar elemento");
      setModalType("Update");
    }
    backButtonWithModal.neutralizeBack(modalRef.current);
    modalRef.current.handleShow();
    modalRef.current.task(objectId, toAdd);
  };

  // React.useEffect(() => {
  //   fetchContinents();
  //   fetchCountries();
  //   fetchRegions();
  // }, []);

  React.useEffect(() => {
    makeTravelsArray();
  }, [props]);
  return (
    <>
      <Col lg={12}>
        {!travelsTotal && !travels ? (
          <h1>Loading...</h1>
        ) : (
          <>
            <Row>
              {travelsTotal}
            </Row>
            <Row>
              {travels}
            </Row>
          </>
        )}
      </Col>
      <ModalForm
        ref={modalRef}
        props={{
          title: modalTitle,
          className: "itemsForm",
        }}
      >
        <TravelPlaceForm
          props={{
            objectId: itemsToForm,
            continents: props.continents,
            countries: props.countries,
            regions: props.regions,
            modalType: modalType,
          }}
          modalSaveHandler={modalSaveHandler}
        />
      </ModalForm>
    </>
  );
}

export default TravelRecordsList;
