import React from "react";

import { Form, Card } from "tabler-react";
import Tree from "react-animated-tree";
import { Row, Col } from "react-bootstrap";
import fetchFunction from "../../helpers/fetchFuction";

const TravelFilter = ({ toShow, parentCallBack, props }) => {
  const language = sessionStorage.getItem("language");
  const [error, setError] = React.useState({});
  // const [continentsArray, setContinentsArray] = React.useState({
  //   info: [],
  //   filters: [],
  //   tree: [],
  //   visible: true,
  // });
  // const [countriesArray, setCountriesArray] = React.useState({
  //   info: [],
  //   filters: [],
  //   tree: [],
  //   visible: true,
  // });
  // const [regionsArray, setRegionsArray] = React.useState({
  //   info: [],
  //   filters: [],
  //   tree: [],
  //   visible: true,
  // });

  // const [continents, setContinents] = React.useState();
  // const [countries, setCountries] = React.useState();
  // const [regions, setRegions] = React.useState();
  const [noteFilter, setNoteFilter] = React.useState();
  const [rendered, setRendered] = React.useState(false);
  const [prevProps, setPrevProps] = React.useState({});


  React.useEffect(() => {
    // if (!firstRender) return;
    if (JSON.stringify(props) == JSON.stringify(prevProps)) return;
    setPrevProps(props);
  }, [props]);

  const formatParentCallback = () => {
    // var continentsFilter = continentsArray.filters;
    // var countriesFilter = countriesArray.filters;
    // var regionsFilter = regionsArray.filters;
    var noteFilter_temp = noteFilter;

    parentCallBack(
      // continentsFilter,
      // countriesFilter,
      // regionsFilter,
      noteFilter_temp,
    );
  };

  React.useEffect(() => {
    // var tempContinents ={ info: [], filters: [], tree: [], visible: true };
    // var tempCountries = { info: [], filters: [], tree: [], visible: true };
    // var tempRegions = { info: [], filters: [], tree: [], visible: true };
    // if (props.continents){
    //   props.continents.forEach((element) => {
    //     tempContinents.info.push({ name: element.label, id: element.value, visible: true });
    //   });
    //   processTreeLvl1(tempContinents, "setContinentsArray");
    // }
    // if (props.countries){
    //   props.countries.forEach((element) => {
    //     tempCountries.info.push({ name: element.label, id: element.value, visible: true });
    //   });
    //   processTreeLvl1(tempCountries, "setCountriesArray");
    // }
    // if (props.regions){
    //   props.regions.forEach((element) => {
    //     tempRegions.info.push({ name: element.label, id: element.value, visible: true });
    //   });
    //   processTreeLvl1(regionsArray, "setRegionsArray");
    // }
  }, [prevProps]);

  const processTreeLvl1 = (array, setArray) => {
    // console.log(array, setArray)
    var temp = { ...array };
    temp.tree = [];
    temp.filters = [];
    array.info.forEach((element) => {
      if (element.visible) {
        temp.filters.push(element.id);
      }
      temp.tree.push(
        <Tree
          key={element.id}
          content={element.name}
          visible={element.visible}
          canHide
          onClick={(e) => handleClick(temp, 1, 1, setArray, e, element.id)}
        />
      );
    });
    eval(setArray)(temp);
  };
  
  React.useEffect(() => {

    formatParentCallback();
  }, [
    // continentsArray,
    // countriesArray,
    // regionsArray,
    noteFilter,
  ]);

  const handleClick = (array, position, lvl, setArray, state, id, parentId) => {
    if (position == 0 && lvl >= 1) {
      array.info.forEach((element) => {
        if (lvl == 2) {
          array[element.id].info.forEach((element) => {
            element.visible = state;
          });
        }
        element.visible = state;
      });
      array.visible = state;
    }
    if (position == 1 && lvl >= 1) {
      var index = array.info.findIndex((x) => x.id == id);
      array.info[index].visible = state;
    }
    if (position == 1 && lvl == 2) {
      array[id].info.forEach((element) => {
        element.visible = state;
      });
    }

    if (position == 2 && lvl == 2) {
      var index2 = array[parentId].info.findIndex((x) => x.id == id);
      array[parentId].info[index2].visible = state;
      var parent1Visible = true;
      array[parentId].info.forEach((element) => {
        parent1Visible = parent1Visible && element.visible;
      });
      var index3 = array.info.findIndex((x) => x.id == parentId);
      array.info[index3].visible = parent1Visible;
    }
    if (position >= 1 && lvl >= 1) {
      var parent0Visible = true;
      array.info.forEach((element) => {
        parent0Visible = parent0Visible && element.visible;
      });
      array.visible = parent0Visible;
    }
    eval("processTreeLvl" + lvl)(array, setArray);
  };

  return (
    <Card title={"Lugares"} isCollapsible>
      <Card.Body >
        <Row >
          <Col lg={12} md={12}>
            <Form.Group label={"Filtro por texto"}>
              <Form.Input
                name="noteFilter"
                autoComplete="off"
                value={noteFilter}
                placeholder={"Texto a buscar"}
                onChange={(e) => setNoteFilter(e.target.value)}
              />
            </Form.Group>
            {/* <br /> */}
          </Col>
          {/* <Col lg={12} md={4}>
            <Tree
              content={"Continentes"}
              type={
                continentsArray.visible
                  ? "ALL"
                  : continentsArray.filters.length
                  ? "SOME"
                  : "NONE"
              }
              canHide
              visible={continentsArray.visible}
              onClick={(e) =>
                handleClick(continentsArray, 0, 1, "setContinentsArray", e)
              }
            >
              {continentsArray.tree}
            </Tree>
            <br />
          </Col>
          <Col lg={12} md={4}>
            <Tree
              content={"Paises"}
              type={
                countriesArray.visible
                  ? "ALL"
                  : countriesArray.filters.length
                  ? "SOME"
                  : "NONE"
              }
              canHide
              visible={countriesArray.visible}
              onClick={(e) =>
                handleClick(countriesArray, 0, 1, "setCountriesArray", e)
              }
            >
              {countriesArray.tree}
            </Tree>
            <br />
          </Col> */}
          {/* <Col lg={12} md={4}>
            <Tree
              content={"Regiones/Provincias"}
              type={
                regionsArray.visible
                  ? "ALL"
                  : regionsArray.filters.length
                  ? "SOME"
                  : "NONE"
              }
              canHide
              visible={regionsArray.visible}
              onClick={(e) =>
                handleClick(regionsArray, 0, 1, "setRegionsArray", e)
              }
            >
              {regionsArray.tree}
            </Tree>
            <br />
          </Col> */}
        </Row>
      </Card.Body>
    </Card>
  );
};

export default TravelFilter;
