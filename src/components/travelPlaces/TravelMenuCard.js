import React from "react";

import { Col, Row, Card } from "react-bootstrap";
import { useHistory } from "react-router-dom";



const TravelMenuCard = ({ props, updateSignal, changeSection }) => {
  const main_currency_symbol = sessionStorage.getItem("main_currency_symbol");
  const main_currency_format = sessionStorage.getItem("main_currency_format");
  const [error, setError] = React.useState();
  const [account, setAccount] = React.useState({});
  const [balance, setBalance] = React.useState();
  const [prevProps, setPrevProps] = React.useState({});
  const [firstRender, setFirstRender] = React.useState(false);

  const history = useHistory();



  const backButtonStyle = {
    height: "2.5rem",
    width: "2.5rem",
    background:
      "#fff url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI0MiIgaGVpZ2h0PSI0MiIgdmlld0JveD0iNCAxIDM2IDM2Ij48cGF0aCBmaWxsLXJ1bGU9Im5vbnplcm8iIHN0cm9rZT0iIzM1NDA1MiIgc3Ryb2tlLXdpZHRoPSIyIiBkPSJNMjQuMzIgMjdhLjY2My42NjMgMCAwMS0uNDgtLjIwNGwtNi42NDItNi44MDRhLjcxLjcxIDAgMDEwLS45ODVsNi42NDEtNi44MDNhLjY2OS42NjkgMCAwMS45NjIgMCAuNzA4LjcwOCAwIDAxMCAuOTg0TDE4LjY0IDE5LjVsNi4xNiA2LjMxM2EuNzEuNzEgMCAwMTAgLjk4NS42NzIuNjcyIDAgMDEtLjQ4MS4yMDJ6IiBmaWxsPSJub25lIi8+PC9zdmc+) 50%/contain",
    cursor: "pointer",
    borderRadius: "6px",
    boxShadow: "0 2px 3px 0 rgba(0,0,0,.2)",
    marginRight: "10px",
  };

  const rowStyle = {
    width: "100%",
    margin: "0.75rem",
  };

  const cardStyle = {
    marginBottom: "0.1rem",
    borderLeft: "5px solid #C8E5EB",
  };

  const nameStyle = {
    fontSize: "18px",
    display: "flex",
    alignItems: "center",
    //justifyContent: "center",
    fontWeight: "600",
    //textAlign: "left",
    // padding: "0.75rem",
    wordBreak: "break-word",
    hyphens: "auto",
  };

  const symbolStyle = {
    fontWeight: "bold",
    display: "flex",
    alignItems: "center",
    // padding: "0.75rem",
  };
  const activeLinkStyle = {
    fontWeight: "700",
    color: "black",
    borderColor: "black",
  };

  const infoTypeStyle = {
    fontSize: "14px",
    fontWeight: 400,
    lineHeight: 1.14,
    color: "#7f8fa4",
  };
  const infoStyle = {
    fontSize: "16px",
    fontWeight: 600,
    lineHeight: 1.14,
    color: "#354052",
  };

  const functionButtonStyle = {
    fontSize: "16px",
    fontWeight: 600,
    lineHeight: 1.14,
    color: "#354052",
    textAlign: "end",
  };
  return (
    <Card style={cardStyle}>
      <Card.Footer style={{ padding: "0px 24px" }}>
        <div className="row row align-items-center">
          {/* <div className="col-lg-3 ml-auto"></div> */}
          <div className="col col-md order-md-first">
            <ul className="nav nav-tabs border-0 flex-column flex-md-row forced-row">
              <li className="nav-item">
                <a
                  className="nav-link" onClick={()=>changeSection('worldMap')}
                  style={props.section == "worldMap" ? activeLinkStyle : {}}
                >
                  <i className="fa fa-calendar-o"></i> Mapa Mundial
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link" onClick={()=>changeSection('europeMap')}
                  style={props.section == "europeMap" ? activeLinkStyle : {}}
                >
                  <i className="fa fa-calendar-o"></i> Mapa Europa
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link" onClick={()=>changeSection('spainMap')}
                  style={props.section == "spainMap" ? activeLinkStyle : {}}
                >
                  <i className="fa fa-calendar-o"></i> Mapa España
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link" onClick={()=>changeSection('records')}
                  style={props.section == "records" ? activeLinkStyle : {}}
                >
                  <i className="fa fa-clock-o"></i> Registros
                </a>
              </li>
            </ul>
          </div>
        </div>
      </Card.Footer>
    </Card>
  );
};

export default TravelMenuCard;
