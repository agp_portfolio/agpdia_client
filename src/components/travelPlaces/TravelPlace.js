import React from "react";

import { Card, Col, Row } from "react-bootstrap";
import { Icon } from "tabler-react";
import { useHistory } from "react-router-dom";
import dateFormater from "../../helpers/dateFormater";
import currencyFormater from "../../helpers/currencyFormater";
import defaultImage from "../../img/inventories_images/default.png";
import Swal from "sweetalert2";
import fetchFunction from "../../helpers/fetchFuction";
import dateDiff from "../../helpers/dateDiff";

const TravelPlace = ({ props, refetchTravels, showModalHandler }) => {
  var color;
  switch (props.state) {
    case "good":
      color = "green";
      break;
    case "regular":
      color = "orange";
      break;
    case "bad":
      color = "red";
      break;
    case "disposed":
      color = "gray";
      break;

    default:
      break;
  }
  const cardStyle = {
    marginBottom: "0.1rem",
    borderLeft: "5px solid " + (props.is_scheduled ? "#9aa0ac" : "#c8e5eb"),
  };
  const normalStyle = {
    // fontSize: "14px",
    alignItems: "center",
    wordBreak: "break-word",
    hyphens: "auto",
    overflow: "hidden",
    whiteSpace: "nowrap" /* Don't forget this one */,
    textOverflow: "ellipsis",
  };
  const coordinatesStyle = {
    // fontSize: "14px",
    alignItems: "center",
    paddingTop: "1rem",
  };
  const dateStyle = {
    // fontSize: "14px",
    alignItems: "center",
    fontWeight: "600",
    paddingTop: "1rem",
    display: "flex",
    justifyContent: "center",
  };
  const dateStyleMovil = {
    // fontSize: "14px",
    alignItems: "center",
    fontWeight: "600",
    paddingTop: "1rem",
    display: "flex",
    // justifyContent: "center",
  };
  const noteStyle = {
    // fontSize: "14px",
    alignItems: "center",
    paddingBottom: "1rem",
    wordBreak: "break-word",
    hyphens: "auto",
    overflow: "hidden",
    whiteSpace: "nowrap" /* Don't forget this one */,
    textOverflow: "ellipsis",
  };
  const topIconStyle = {
    textAlign: "end",
    marginTop: "1rem",
  };

  const bottomIconStyle = {
    textAlign: "end",
    marginBottom: "5px",
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-end",
  };


  const handleClick = (e) => {
    showModalHandler(props._id, false);
  };

  const deleteTravel = () => {
    Swal.fire({
      title: "¿Estas seguro de que proceder con la eliminación?",
      // text: `${props.selected.length} ${
      //   props.selected.length == 1 ? " registro" : " registros"
      // } a eliminar`,
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, !Borrar!",
    }).then((result) => {
      if (!result.value) return;
      var [promise, abort] = fetchFunction(
        "travels/travels/" + props._id,
        "DELETE"
      );
      promise.then((data) => {
        if (!data) {
          Swal.fire({
            text: `No se han encontrado los elementos a eliminar o estan en uso`,
            icon: "error",
            timer: "2000",
          });
        } else {
          Swal.fire({
            text: `El elementos se ha eliminado`,
            icon: "success",
            timer: "1000",
          });
          refetchTravels();
        }
      });
    });
  };

  return (
    <Col xs={12}>
      <Row>
        <Card style={cardStyle}>
          <Card.Body style={{ padding: "0px 10px", display: "flex" }}>
            <Col
              xs={12}
              xs={12}
              onClick={handleClick}
              style={{ marginBottom: "10px" }}
            >
              {/* Card para pantalla grande */}
              <span className="d-none d-md-inline">
                <Row>
                  <Col md={2} style={dateStyle}>
                    <span>{dateFormater(props.date, "show")}</span>
                  </Col>
                  <Col
                    xs={8}
                    className="d-none d-md-flex m-auto"
                    style={coordinatesStyle}
                    title={`${props.latitude},${props.longitude}`}
                    onClick={(e) => {
                      e.stopPropagation();
                    }}
                  >
                    {props.latitude && props.longitude
                      ? `Coordenadas: ${props.latitude},${props.longitude}`
                      : ""}
                  </Col>
                  <Col xs={2} style={topIconStyle}>
                    <Icon
                      name="times"
                      onClick={(e) => {
                        e.stopPropagation();
                        deleteTravel();
                      }}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col
                    xs={2}
                    style={{
                      display: "flex",
                      alignItems: "center",
                    }}
                  >
                    <img
                      src={`https://flagcdn.com/w160/${props.countryCode.toLowerCase()}.png`}
                      srcset={`https://flagcdn.com/w320/${props.countryCode.toLowerCase()}.png 2x`}
                      width="100"
                      className="d-none d-md-flex"
                      style={{
                        maxWidth: "none",
                        width: "100px",
                        border: "1px solid grey",
                      }}
                    />
                  </Col>
                  <Col xs={9}>
                    <Row>
                      <Col
                        xs={10}
                        style={normalStyle}
                        title={`${props.continent}, ${props.country}, ${props.region}, ${props.place}`}
                      >
                        Continente: {props.continent}
                        <br />
                        País: {props.country}
                        <br />
                        Región: {props.region ? props.region : ""}
                        <br />
                        Lugar: {props.place ? props.place : ""}
                        <br />
                        Nota: {props.annotation ? props.annotation : ""}
                      </Col>
                    </Row>
                  </Col>
                  <Col xs={1} style={bottomIconStyle}>
                    <Icon
                      name="copy"
                      onClick={(e) => {
                        e.stopPropagation();
                        showModalHandler(props._id, true);
                      }}
                    />
                  </Col>
                </Row>
              </span>
              {/* Card para pantalla pequeña ( Movil o tablet) */}
              <span className="d-md-none">
                <Row>
                  <Col xs={4} style={dateStyleMovil}>
                    <span>{dateFormater(props.date, "show")}</span>
                  </Col>
                  <Col
                    xs={6}
                    className="m-auto"
                    style={coordinatesStyle}
                    title={`${props.latitude},${props.longitude}`}
                    onClick={(e) => {
                      e.stopPropagation();
                    }}
                  >
                    {props.latitude && props.longitude
                      ? `${props.latitude},${props.longitude}`
                      : ""}
                    
                  </Col>
                  <Col xs={2} style={topIconStyle}>
                    <Icon
                      name="times"
                      onClick={(e) => {
                        e.stopPropagation();
                        deleteTravel();
                      }}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col
                    xs={2}
                    style={{
                      display: "flex",
                      alignItems: "center",
                    }}
                  >
                    <img
                      src={`https://flagcdn.com/w160/${props.countryCode.toLowerCase()}.png`}
                      srcset={`https://flagcdn.com/w320/${props.countryCode.toLowerCase()}.png 2x`}
                      width="100"
                      className="d-md-none"
                      style={{
                        marginTop: "0px",
                        maxWidth: "100%",
                        // width: "70px",
                        border: "1px solid grey",
                      }}
                    />
                  </Col>
                  <Col xs={9}>
                    <Row>
                      <Col
                        xs={10}
                        style={normalStyle}
                        title={`${props.continent}, ${props.country}, ${props.region}, ${props.place}`}
                      >
                        <span className="d-none d-md-flex">
                          {props.continent}
                          {"  "}
                          <i
                            class="fa fa-long-arrow-right"
                            aria-hidden="true"
                          ></i>
                          {"  "}
                        </span>

                        {props.country}
                        {"  "}
                        <span className="d-none d-md-flex">
                          {props.region && (
                            <>
                              <i
                                class="fa fa-long-arrow-right"
                                aria-hidden="true"
                              ></i>
                              {"  "}
                              {props.region}
                              {"  "}
                            </>
                          )}
                        </span>
                        {props.place && (
                          <>
                            <i
                              class="fa fa-long-arrow-right"
                              aria-hidden="true"
                            ></i>
                            {"  "}
                            <strong>{props.place}</strong>
                            {"  "}
                            <br />
                            {props.annotation ? (
                              <span title={props.annotation}>
                                {" "}
                                Nt. {props.annotation}
                              </span>
                            ) : (
                              <span>&nbsp;&nbsp;</span>
                            )}
                          </>
                        )}
                      </Col>
                    </Row>
                  </Col>
                  <Col xs={1} style={bottomIconStyle}>
                    <Icon
                      name="copy"
                      onClick={(e) => {
                        e.stopPropagation();
                        showModalHandler(props._id, true);
                      }}
                    />
                  </Col>
                </Row>
              </span>
            </Col>
          </Card.Body>
        </Card>
      </Row>
    </Col>
  );
};

export default TravelPlace;
