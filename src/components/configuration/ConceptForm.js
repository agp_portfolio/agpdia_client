import React, { useEffect, useState, useRef } from "react";

import { Modal, Col, Row } from "react-bootstrap";
import DateRangePicker from "react-bootstrap-daterangepicker";
import { Form, Button } from "tabler-react";
import Select from "react-select";
import fetchFunction from "../../helpers/fetchFuction";

import InputColor from "react-input-color";
import ImagePicker from "react-image-picker";
import "react-image-picker/dist/index.css";

const ConceptForm = ({ props, modalSaveHandler }) => {
  const language = sessionStorage.getItem("language");
  const [currencySymbol, setCurrencySymbol] = useState("€");
  const [initialConcept, setInitialConcept] = useState(false);
  const [fetchedConcept, setFetchedConcept] = useState(false);
  const [newConcept, setNewConcept] = useState(false);
  const [validForm, setValidForm] = useState();

  const [accountsOptions, setConceptsOptions] = useState();
  const [currencyOptions, setCurrencyOptions] = useState();

  const [imagesOptions, setImagesOptions] = useState([]);
  // const [conceptsOptions, setConceptsOptions] = useState();
  const [allConceptsOptions, setAllConceptsOptions] = useState();

  const [concept, setConcept] = useState();
  const [group, setGroup] = useState();
  const [subgroup, setSubgroup] = useState();
  const [consumptionU, setConsumptionU] = useState();

  const [name, setName] = useState();
  const [category, setCategory] = useState();
  const [initialAmount, setInitialAmount] = useState();
  const [color, setColor] = useState("");
  const [currency, setCurrency] = useState();
  const [image, setImage] = useState();

  const keyRef = useRef(Date.now()); // Needed to update the date
  keyRef.current = Date.now(); // Needed to update the date

  const fetchConcept = (objectId) => {
    var [promise, abort] = fetchFunction("accounts/concepts/" + objectId, "GET")
    promise.then((data) => {
      if (!data) {
        // No action
      } else {
        setFetchedConcept(data);
        setFields(data);
      }
    });
  };

  const handleSave = () => {
    var processedNewConcept = newConcept;

    switch (props.modalType) {
      case "Add":
        var [promise, abort] = fetchFunction("accounts/concepts/", "POST", newConcept)
        promise.then((data) => {
          if (!data) {
            // No action
          } else {
            modalSaveHandler();
          }
        });
        break;
      case "Update":
        var objectId = props.objectId;
        var [promise, abort] = fetchFunction(
          "accounts/concepts/" + objectId,
          "PUT",
          processedNewConcept
        )
        promise.then((data) => {
          if (!data) {
            // No action
          } else {
            modalSaveHandler();
          }
        });
        break;

      default:
        break;
    }
  };

  const setFields = (data) => {
    if (data) {
      setName(data.name);
      setCategory({ value: data.category_id, label: data.category_name });
      setGroup(data.group);
      setSubgroup(data.subgroup);

      var backData = {
        name: data.name,
        category_id: data.category_id,
        group: data.group,
        subgroup: data.subgroup,
      };
    } else {
      setName();
      setCategory();
      setGroup();
      setSubgroup();
      var backData = {
        name: undefined,
        category_id: undefined,
        group: undefined,
        subgroup: undefined,
      };
    }
    setInitialConcept(backData);
    setNewConcept(backData);
  };

  React.useEffect(() => {
    if (props.modalType == "Update") {
      fetchConcept(props.objectId);
    } else if (props.modalType == "Add") {
      setFields();
    }
  }, []);

  React.useEffect(() => {
    var backData = {
      name: name,
      category_id: category ? category.value : undefined,
      group: group,
      subgroup: subgroup,
    };
    if (name && category) {
      setValidForm(true);
    } else {
      setValidForm(false);
    }
    setNewConcept(backData);
  }, [name, category, group, subgroup]);
  return (
    <>
      <Row style={{ margin: "0px" }} className="conceptForm coloredForm">
        <Col md={12} style={{ padding: "0px" }}>
          <div
            style={{
              background: "rgb(144, 164, 174)",
              color: "white",
              padding: "1rem",
            }}
          >
            <Row>
              <Col md={6}>
                <Form.Group label="Nombre *" style={{ paddingRight: "10px" }}>
                  <Form.Input
                    name="name"
                    className="input"
                    placeholder="Nombre..."
                    autoComplete="off"
                    type="text"
                    value={name}
                    onChange={(e) => {
                      setName(e.target.value);
                    }}
                  />
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group label="Categoría *">
                  <div style={{ color: "rgb(73, 80, 87)", width: "100%" }}>
                    <Select
                      onChange={(e) => {
                        setCategory(e);
                      }}
                      isSearchable={false}
                      value={{
                        label: category
                          ? category.label
                          : "Selecciona categoría",
                      }}
                      options={props.categoryOptions}
                    />
                  </div>
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group label="Grupo" className="group">
                  <Form.SelectGroup
                    pills
                    value={group}
                    onChange={(e) => {
                      if (group != e.target.value) {
                        setGroup(e.target.value);
                        setSubgroup();
                      }
                    }}
                  >
                    <Form.SelectGroupItem
                      icon="home"
                      label="Vivienda"
                      name="group"
                      value="house"
                      checked={group == "house"}
                      onChange={(e) => {}}
                    />
                    <Form.SelectGroupItem
                      icon="car"
                      label="Vehículo"
                      name="group"
                      value="vehicle"
                      checked={group == "vehicle"}
                      onChange={(e) => {}}
                    />
                    <Form.SelectGroupItem
                      icon="line-chart"
                      label="Acciones"
                      name="group"
                      value="stock"
                      checked={group == "stock"}
                      onChange={(e) => {}}
                    />
                    <Form.SelectGroupItem
                      icon="times"
                      label="General"
                      name="group"
                      value="general"
                      checked={group == "general"}
                      onChange={(e) => {}}
                    />
                  </Form.SelectGroup>
                </Form.Group>
              </Col>
              <Col md={6}>
                {group != "stock" && (
                  <Form.Group label="Subgrupo" className="group">
                    <Form.SelectGroup
                      pills
                      value={subgroup}
                      onChange={(e) => setSubgroup(e.target.value)}
                    >
                      {!group && (
                        <Form.SelectGroupItem
                          icon="file-text-o"
                          label="Nómina"
                          name="subgroup"
                          value="paysheet"
                          checked={subgroup == "paysheet"}
                          onChange={(e) => {}}
                        />
                      )}
                      {group == "house" && (
                        <>
                          <Form.SelectGroupItem
                            icon="tint"
                            label="Agua"
                            name="subgroup"
                            value="water_house"
                            checked={subgroup == "water_house"}
                            onChange={(e) => {}}
                          />
                          <Form.SelectGroupItem
                            icon="lightbulb-o"
                            label="Luz"
                            name="subgroup"
                            value="light_house"
                            checked={subgroup == "light_house"}
                            onChange={(e) => {}}
                          />
                          <Form.SelectGroupItem
                            icon="thermometer-full"
                            label="Gas"
                            name="subgroup"
                            value="gas_house"
                            checked={subgroup == "gas_house"}
                            onChange={(e) => {}}
                          />
                        </>
                      )}
                      {group == "vehicle" && (
                        <>
                          <Form.SelectGroupItem
                            icon="road"
                            label="Combustible"
                            name="subgroup"
                            value="power_source_vehicle"
                            checked={subgroup == "power_source_vehicle"}
                            onChange={(e) => {}}
                          />
                          <Form.SelectGroupItem
                            icon="wrench"
                            label="Mantenimiento"
                            name="subgroup"
                            value="maintenance_vehicle"
                            checked={subgroup == "maintenance_vehicle"}
                            onChange={(e) => {}}
                          />
                        </>
                      )}

                      <Form.SelectGroupItem
                        icon="times"
                        label="General"
                        name="subgroup"
                        value="general"
                        checked={subgroup == "general"}
                        onChange={(e) => {}}
                      />
                    </Form.SelectGroup>
                  </Form.Group>
                )}
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
      <Row style={{ margin: "0px" }}>
        <Col md={12} style={{ height: "10px" }}></Col>
      </Row>
      {newConcept &&
      JSON.stringify(initialConcept) != JSON.stringify(newConcept) ? (
        <Row className="form-buttons" style={{ margin: "0px" }}>
          <Col md={12} style={{ padding: "0px", height: "60px" }}>
            <Row className="form-buttons" style={{ padding: "inherit" }}>
              <Col xs={6}>
                <div style={{ float: "right" }}>
                  <Button
                    pill
                    outline
                    color="info"
                    onClick={() => setFields(fetchedConcept)}
                  >
                    Deshacer
                  </Button>
                </div>
              </Col>
              <Col xs={6}>
                {validForm && (
                  <div style={{ float: "left" }}>
                    <Button pill color="success" onClick={() => handleSave()}>
                      Guardar
                    </Button>
                  </div>
                )}
              </Col>
            </Row>
          </Col>
        </Row>
      ) : (
        <Row style={{ height: "60px", margin: "0" }}>
          <Col md={12} style={{ padding: "0px", height: "60px" }}></Col>
        </Row>
      )}
    </>
  );
};

export default ConceptForm;
