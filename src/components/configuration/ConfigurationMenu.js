import React, { useState, useRef } from "react";
import "bootstrap-daterangepicker/daterangepicker.css";
import { Card, Row, Button } from "react-bootstrap";
//import { Button } from "tabler-react";

const ConfigurationMenu = () => {
  return (
    <Card>
      <Card.Body>
        <div class="settings-menu">
          <h1>Configuración</h1>
          <div>
            <div>
              <div>
                <h2>Seleccionables</h2>
                <ul>
                  <li>
                    <a href="/configuration/dropdowns/accounting_categories">Cont. → Categorias</a>
                  </li>
                  <li>
                    <a href="/configuration/dropdowns/accounting_concepts">Cont. → Conceptos</a>
                  </li>
                  <li>
                    <a href="/configuration/dropdowns/houses">Gen. → Viviendas</a>
                  </li>
                  <li>
                    <a href="/configuration/dropdowns/vehicles">Gen. → Vehículos</a>
                  </li>
                  <li>
                    <a href="/configuration/dropdowns/inventory_inventories">Inv. → Inventorios</a>
                  </li>
                  <li>
                    <a href="/configuration/dropdowns/inventory_locations">Inv. → Ubicaciones</a>
                  </li>
                  <li>
                    <a href="/configuration/dropdowns/inventory_groups">Inv. → Grupos</a>
                  </li>
                </ul>
              </div>
              <div>
                <h2>Avisos</h2>
                <ul>
                  <li>
                    <a href="/configuration/alerts/accounts">Contabilidad</a>
                  </li>
                  {/* <li>
                    <a href="/configuration/alerts/trips">Viajes</a>
                  </li>
                  <li>
                    <a href="/configuration/alerts/inventory">Inventario</a>
                  </li> */}
                </ul>
              </div>
            </div>
          </div>
        </div>
      </Card.Body>
    </Card>
  );
};

export default ConfigurationMenu;
