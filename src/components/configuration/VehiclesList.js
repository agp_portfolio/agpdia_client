// @flow

import React from "react";
import { Redirect, useParams } from "react-router-dom";

import Infinite from "react-infinite";

import ModalForm from "../ModalForm";
import { Container, Row, Col, Card, ModalTitle } from "react-bootstrap";
import { Form, Button } from "tabler-react";
//import { Card } from "tabler-react";
import fetchFunction from "../../helpers/fetchFuction";

import AccountRecord from "../accountRecords/AccountRecord";
import Vehicle from "./Vehicle";
import VehicleForm from "./VehicleForm";
import AccountRecordDate from "../accountRecords/AccountRecordDate";
import AccountRecordTotal from "../accountRecords/AccountRecordTotal";
import AccountRecordForm from "../accountRecords/AccountRecordForm";

import backButtonWithModal from "../../helpers/backButtonWithModal";

function AccountRecordsList({ props, updateSignal }) {
  const [error, setError] = React.useState();
  const [modalTitle, setModalTitle] = React.useState("");
  const [vehicles, setVehicles] = React.useState();
  const [itemsToForm, setItemsToForm] = React.useState([]);
  const [modalType, setModalType] = React.useState();

  var modalRef = React.useRef();

  const infoStyle = {
    fontSize: "14px",
    // display: "flex",
    alignItems: "center",
    fontWeight: "600",
    padding: "0.25rem",
    textAlign: "center",
  };

  const fetchVehicles = () => {
    var [promise, abort] = fetchFunction("vehicles/", "GET")
    promise.then((data) => {
      if (!data) {
        // No action
      } else {
        var table = [];
        data.forEach((vehicle) => {
          table.push(
            <Vehicle
              key={vehicle.id}
              props={vehicle}
              showModalHandler={showModalHandler}
              refetchVehicles={refetchVehicles}
            />
          );
        });
        setVehicles(table);
      }
    });
  };

  const showModalHandler = (objectId, toAdd) => {
    objectId ? setItemsToForm([objectId]) : setItemsToForm();
    if (toAdd) {
      setModalTitle("Añadir vehículo");
      setModalType("Add");
    } else {
      setModalTitle("Editar vehículo");
      setModalType("Update");
    }
    backButtonWithModal.neutralizeBack(modalRef.current);
    modalRef.current.handleShow();
    modalRef.current.task(objectId, toAdd);
  };

  const modalSaveHandler = () => {
    modalRef.current.handleHide();

    if (updateSignal) {
      updateSignal();
    }
    refetchVehicles();
  };

  const refetchVehicles = () => {
    fetchVehicles();
  };

  React.useEffect(() => {
    refetchVehicles(true);
  }, []);

  React.useEffect(() => {
    
  }, [vehicles]);

  return (
    <>
      {error && (
        <div className={"alert alert-" + error.type} role="alert">
          {error.message}
        </div>
      )}

      <Col lg={12}>
        <Card
          className="account-record-total"
          style={{ marginBottom: "0.1rem", background: "ghostwhite" }}
        >
          <Card.Body style={{ padding: "0px 10px", display: "flex" }}>
            <Col md={12} xs={12} style={infoStyle}>
              <Button
                pill
                outline
                color="primary"
                onClick={() => showModalHandler(undefined, true)}
              >
                Añadir
              </Button>
            </Col>
          </Card.Body>
        </Card>
        {!vehicles ? (
          <h1>Loading...</h1>
        ) : (
          <Infinite
            elementHeight={60} // The height of the tallest element in the list
            useWindowAsScrollContainer
          >
            {vehicles}
          </Infinite>
        )}
      </Col>
      <ModalForm
        ref={modalRef}
        props={{
          title: modalTitle,
        }}
      >
        <VehicleForm
          props={{
            objectId: itemsToForm,
            modalType: modalType,
          }}
          modalSaveHandler={modalSaveHandler}
        />
      </ModalForm>
    </>
  );
}

export default AccountRecordsList;
