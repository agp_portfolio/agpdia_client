import React, { useEffect, useState, useRef } from "react";

import { Modal, Col, Row } from "react-bootstrap";
import DateRangePicker from "react-bootstrap-daterangepicker";
import { Form, Button } from "tabler-react";
import Select from "react-select";
import fetchFunction from "../../helpers/fetchFuction";

import InputColor from "react-input-color";
import ImagePicker from "react-image-picker";
import "react-image-picker/dist/index.css";

const LocationForm = ({ props, modalSaveHandler }) => {
  const language = sessionStorage.getItem("language");
  const [currencySymbol, setCurrencySymbol] = useState("€");
  const [initialLocation, setInitialLocation] = useState(false);
  const [fetchedLocation, setFetchedLocation] = useState(false);
  const [newLocation, setNewLocation] = useState(false);
  const [validForm, setValidForm] = useState();

  const [accountsOptions, setLocationsOptions] = useState();
  const [currencyOptions, setCurrencyOptions] = useState();

  const [imagesOptions, setImagesOptions] = useState([]);
  // const [locationsOptions, setLocationsOptions] = useState();
  const [allLocationsOptions, setAllLocationsOptions] = useState();

  const [location, setLocation] = useState();
  const [consumptionU, setConsumptionU] = useState();

  const [name, setName] = useState();
  const [inventory, setInventory] = useState();
  const [initialAmount, setInitialAmount] = useState();
  const [color, setColor] = useState("");
  const [currency, setCurrency] = useState();
  const [image, setImage] = useState();

  const keyRef = useRef(Date.now()); // Needed to update the date
  keyRef.current = Date.now(); // Needed to update the date

  const fetchLocation = (objectId) => {
    var [promise, abort] = fetchFunction(
      "inventories/locations/" + objectId,
      "GET"
    );
    promise.then((data) => {
      if (!data) {
        // No action
      } else {
        setFetchedLocation(data);
        setFields(data);
      }
    });
  };

  const handleSave = () => {
    var processedNewLocation = newLocation;

    switch (props.modalType) {
      case "Add":
        var [promise, abort] = fetchFunction(
          "inventories/locations/",
          "POST",
          newLocation
        );
        promise.then((data) => {
          if (!data) {
            // No action
          } else {
            modalSaveHandler();
          }
        });
        break;
      case "Update":
        var objectId = props.objectId;
        var [promise, abort] = fetchFunction(
          "inventories/locations/" + objectId,
          "PUT",
          processedNewLocation
        );
        promise.then((data) => {
          if (!data) {
            // No action
          } else {
            modalSaveHandler();
          }
        });
        break;

      default:
        break;
    }
  };

  const setFields = (data) => {
    if (data) {
      setName(data.name);
      setInventory({ value: data.inventory_id, label: data.inventory_name });

      var backData = {
        name: data.name,
        inventory_id: data.inventory_id,
      };
    } else {
      setName();
      setInventory();

      var backData = {
        name: undefined,
        inventory_id: undefined,
      };
    }
    setInitialLocation(backData);
    setNewLocation(backData);
  };

  React.useEffect(() => {
    if (props.modalType == "Update") {
      fetchLocation(props.objectId);
    } else if (props.modalType == "Add") {
      setFields();
    }
  }, []);

  React.useEffect(() => {
    var backData = {
      name: name,
      inventory_id: inventory ? inventory.value : undefined,
    };
    if (name && inventory) {
      setValidForm(true);
    } else {
      setValidForm(false);
    }
    setNewLocation(backData);
  }, [name, inventory]);
  return (
    <>
      <Row style={{ margin: "0px" }} className="locationForm coloredForm">
        <Col md={12} style={{ padding: "0px" }}>
          <div
            style={{
              background: "rgb(144, 164, 174)",
              color: "white",
              padding: "1rem",
            }}
          >
            <Row>
              <Col md={6}>
                <Form.Group label="Nombre *" style={{ paddingRight: "10px" }}>
                  <Form.Input
                    name="name"
                    className="input"
                    placeholder="Nombre..."
                    autoComplete="off"
                    type="text"
                    value={name}
                    onChange={(e) => {
                      setName(e.target.value);
                    }}
                  />
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group label="Inventorio *">
                  <div style={{ color: "rgb(73, 80, 87)", width: "100%" }}>
                    <Select
                      onChange={(e) => {
                        setInventory(e);
                      }}
                      isSearchable={false}
                      value={{
                        label: inventory
                          ? inventory.label
                          : "Selecciona inventorio",
                      }}
                      options={props.inventoryOptions}
                    />
                  </div>
                </Form.Group>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
      <Row style={{ margin: "0px" }}>
        <Col md={12} style={{ height: "10px" }}></Col>
      </Row>
      {newLocation &&
      JSON.stringify(initialLocation) != JSON.stringify(newLocation) ? (
        <Row className="form-buttons" style={{ margin: "0px" }}>
          <Col md={12} style={{ padding: "0px", height: "60px" }}>
            <Row className="form-buttons" style={{ padding: "inherit" }}>
              <Col xs={6}>
                <div style={{ float: "right" }}>
                  <Button
                    pill
                    outline
                    color="info"
                    onClick={() => setFields(fetchedLocation)}
                  >
                    Deshacer
                  </Button>
                </div>
              </Col>
              <Col xs={6}>
                {validForm && (
                  <div style={{ float: "left" }}>
                    <Button pill color="success" onClick={() => handleSave()}>
                      Guardar
                    </Button>
                  </div>
                )}
              </Col>
            </Row>
          </Col>
        </Row>
      ) : (
        <Row style={{ height: "60px", margin: "0" }}>
          <Col md={12} style={{ padding: "0px", height: "60px" }}></Col>
        </Row>
      )}
    </>
  );
};

export default LocationForm;
