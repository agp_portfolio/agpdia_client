import React, { useEffect, useState, useRef } from "react";

import { Modal, Col, Row } from "react-bootstrap";
import DateRangePicker from "react-bootstrap-daterangepicker";
import { Form, Button } from "tabler-react";
import Select from "react-select";
import fetchFunction from "../../helpers/fetchFuction";

import InputColor from "react-input-color";
import ImagePicker from "react-image-picker";
import "react-image-picker/dist/index.css";

const CategoryForm = ({ props, modalSaveHandler }) => {
  const language = sessionStorage.getItem("language");
  const [currencySymbol, setCurrencySymbol] = useState("€");
  const [initialCategory, setInitialCategory] = useState(false);
  const [fetchedCategory, setFetchedCategory] = useState(false);
  const [newCategory, setNewCategory] = useState(false);
  const [validForm, setValidForm] = useState();

  const [accountsOptions, setCategoriesOptions] = useState();
  const [currencyOptions, setCurrencyOptions] = useState();

  const [imagesOptions, setImagesOptions] = useState([]);
  // const [conceptsOptions, setConceptsOptions] = useState();
  const [allConceptsOptions, setAllConceptsOptions] = useState();

  const [category, setCategory] = useState();
  const [group, setGroup] = useState();
  const [subgroup, setSubgroup] = useState();
  const [consumptionU, setConsumptionU] = useState();

  const [name, setName] = useState();
  const [initialAmount, setInitialAmount] = useState();
  const [color, setColor] = useState("");
  const [currency, setCurrency] = useState();
  const [image, setImage] = useState();

  const keyRef = useRef(Date.now()); // Needed to update the date
  keyRef.current = Date.now(); // Needed to update the date

  const fetchCategory = (objectId) => {
    var [promise, abort] = fetchFunction("accounts/categories/" + objectId, "GET")
    promise.then((data) => {
      if (!data) {
        // No action
      } else {
        setFetchedCategory(data);
        setFields(data);
      }
    });
    
  };

  const handleSave = () => {
    var processedNewCategory = newCategory;

    switch (props.modalType) {
      case "Add":
        var [promise, abort] = fetchFunction("accounts/categories/", "POST", newCategory)
        promise.then((data) => {
          if (!data) {
            // No action
          } else {
            modalSaveHandler();
          }
        });
        break;
      case "Update":
        var objectId = props.objectId;
        var [promise, abort] = fetchFunction(
          "accounts/categories/" + objectId,
          "PUT",
          processedNewCategory
        )
        promise.then((data) => {
          if (!data) {
            // No action
          } else {
            modalSaveHandler();
          }
        });
        break;

      default:
        break;
    }
  };

  const setFields = (data) => {
    if (data) {
      setName(data.name);
      var backData = {
        name: data.name,
      };
      
    } else {
      setName();
      var backData = {
        name: undefined,
      };

    }
    setInitialCategory(backData);
    setNewCategory(backData);
  };

  React.useEffect(() => {
    console.log(props)
    if (props.modalType == "Update") {
      fetchCategory(props.objectId);
    } else if (props.modalType == "Add") {
      setFields();
    }

  
  }, []);

  React.useEffect(
    () => {
      var backData = {
        name: name,
      };
       if (
        name
      ) {
        setValidForm(true);
      } else {
        setValidForm(false);
      }
      setNewCategory(backData);
    },
    [
      name,
    ]
  );

  return (
    <>
      <Row style={{ margin: "0px" }} className="categoryForm">
        <Col md={12} style={{ padding: "0px" }}>
          <div
            style={{
              background: "rgb(144, 164, 174)",
              color: "white",
              padding: "1rem",
            }}
          >
            <Row>
              <Col md={12}>
                <Form.Group label="Nombre *" style={{ paddingRight: "10px" }}>
                  <Form.Input
                    name="name"
                    className="input"
                    placeholder="Nombre..."
                    autoComplete="off"
                    type="text"
                    value={name}
                    onChange={(e) => {
                      setName(e.target.value);
                    }}
                  />
                </Form.Group>
              </Col>
              
            </Row>
          </div>
        </Col>
      </Row>
      <Row style={{ margin: "0px" }}>
        <Col md={12} style={{ height: "10px" }}></Col>
      </Row>
      {newCategory &&
      JSON.stringify(initialCategory) != JSON.stringify(newCategory) ? (
        <Row className="form-buttons" style={{ margin: "0px" }}>
          <Col md={12} style={{ padding: "0px", height: "60px" }}>
            <Row className="form-buttons" style={{ padding: "inherit" }}>
              <Col xs={6}>
                <div style={{ float: "right" }}>
                  <Button
                    pill
                    outline
                    color="info"
                    onClick={() => setFields(fetchedCategory)}
                  >
                    Deshacer
                  </Button>
                </div>
              </Col>
              <Col xs={6}>
                {validForm && (
                  <div style={{ float: "left" }}>
                    <Button pill color="success" onClick={() => handleSave()}>
                      Guardar
                    </Button>
                  </div>
                )}
              </Col>
            </Row>
          </Col>
        </Row>
      ) : (
        <Row style={{ height: "60px", margin: "0" }}>
          <Col md={12} style={{ padding: "0px", height: "60px" }}></Col>
        </Row>
      )}
    </>
  );
};

export default CategoryForm;
