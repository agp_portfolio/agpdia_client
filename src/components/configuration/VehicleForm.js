import React, { useEffect, useState, useRef } from "react";

import { Modal, Col, Row } from "react-bootstrap";
import DateRangePicker from "react-bootstrap-daterangepicker";
import { Form, Button } from "tabler-react";
import Select from "react-select";
import fetchFunction from "../../helpers/fetchFuction";

import InputColor from "react-input-color";
import ImagePicker from "react-image-picker";
import "react-image-picker/dist/index.css";

const VehicleForm = ({ props, modalSaveHandler }) => {
  const language = sessionStorage.getItem("language");
  const [currencySymbol, setCurrencySymbol] = useState("€");
  const [initialVehicle, setInitialVehicle] = useState(false);
  const [fetchedVehicle, setFetchedVehicle] = useState(false);
  const [newVehicle, setNewVehicle] = useState(false);
  const [validForm, setValidForm] = useState();

  const [powerSourceOptions, setPowerSourceOptions] = useState([
    {
      label: "Gasolina",
      value: "gasoline",
    },
    {
      label: "Eléctrico",
      value: "electricity",
    },
  ]);
  const [currencyOptions, setCurrencyOptions] = useState();

  const [imagesOptions, setImagesOptions] = useState([]);
  // const [conceptsOptions, setConceptsOptions] = useState();
  const [allConceptsOptions, setAllConceptsOptions] = useState();

  const [category, setCategory] = useState();
  const [group, setGroup] = useState();
  const [subgroup, setSubgroup] = useState();
  const [consumptionU, setConsumptionU] = useState();

  const [name, setName] = useState();
  const [powerSource, setPowerSource] = useState();
  const [initialAmount, setInitialAmount] = useState();
  const [color, setColor] = useState("");
  const [currency, setCurrency] = useState();
  const [image, setImage] = useState();

  const keyRef = useRef(Date.now()); // Needed to update the date
  keyRef.current = Date.now(); // Needed to update the date

  const fetchVehicle = (objectId) => {
    var [promise, abort] =fetchFunction("vehicles/" + objectId, "GET")
    promise.then((data) => {
      if (!data) {
        // No action
      } else {
        setFetchedVehicle(data);
        setFields(data);
      }
    });
  };

  const handleSave = () => {
    var processedNewVehicle = newVehicle;

    switch (props.modalType) {
      case "Add":
        var [promise, abort] = fetchFunction("vehicles/", "POST", newVehicle)
        promise.then((data) => {
          if (!data) {
            // No action
          } else {
            modalSaveHandler();
          }
        });
        break;
      case "Update":
        var objectId = props.objectId;
        var [promise, abort] = fetchFunction("vehicles/" + objectId, "PUT", processedNewVehicle)
        promise.then((data) => {
          if (!data) {
            // No action
          } else {
            modalSaveHandler();
          }
        });
        break;

      default:
        break;
    }
  };

  const setFields = (data) => {
    if (data) {
      setName(data.name);
      var vehicleLabel = ''
      if(data.power_source == 'gasoline'){
        vehicleLabel = "Gasolina"
      }else if(data.power_source == 'electricity'){
        vehicleLabel="Eléctrico"
      }

      setPowerSource({
        label: vehicleLabel,
        value: data.power_source,
      });
      var backData = {
        name: data.name,
        power_source: data.power_source,
      };
    } else {
      setName();
      setPowerSource()
      var backData = {
        name: undefined,
        power_source: undefined,
      };
    }
    setInitialVehicle(backData);
    setNewVehicle(backData);
  };

  React.useEffect(() => {
    console.log(props);
    if (props.modalType == "Update") {
      fetchVehicle(props.objectId);
    } else if (props.modalType == "Add") {
      setFields();
    }
  }, []);

  React.useEffect(() => {
    var backData = {
      name: name,
      power_source: powerSource ? powerSource.value:undefined,
    };
    if (name && powerSource) {
      setValidForm(true);
    } else {
      setValidForm(false);
    }
    setNewVehicle(backData);
  }, [name, powerSource]);

  return (
    <>
      <Row style={{ margin: "0px" }} className="vehicleForm">
        <Col md={12} style={{ padding: "0px" }}>
          <div
            style={{
              background: "rgb(144, 164, 174)",
              color: "white",
              padding: "1rem",
            }}
          >
            <Row>
              <Col md={6}>
                <Form.Group label="Nombre *" style={{ paddingRight: "10px" }}>
                  <Form.Input
                    name="name"
                    className="input"
                    placeholder="Nombre..."
                    autoComplete="off"
                    type="text"
                    value={name}
                    onChange={(e) => {
                      setName(e.target.value);
                    }}
                  />
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group label="Fuente de alimentación *">
                  <div style={{ color: "rgb(73, 80, 87)", width: "100%" }}>
                    <Select
                      onChange={(e) => {
                        setPowerSource(e);
                      }}
                      isSearchable={false}
                      value={{
                        label: powerSource ? powerSource.label : "Selecciona fuente de alimentación",
                      }}
                      options={powerSourceOptions}
                    />
                  </div>
                </Form.Group>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
      <Row style={{ margin: "0px" }}>
        <Col md={12} style={{ height: "10px" }}></Col>
      </Row>
      {newVehicle &&
      JSON.stringify(initialVehicle) != JSON.stringify(newVehicle) ? (
        <Row className="form-buttons" style={{ margin: "0px" }}>
          <Col md={12} style={{ padding: "0px", height: "60px" }}>
            <Row className="form-buttons" style={{ padding: "inherit" }}>
              <Col xs={6}>
                <div style={{ float: "right" }}>
                  <Button
                    pill
                    outline
                    color="info"
                    onClick={() => setFields(fetchedVehicle)}
                  >
                    Deshacer
                  </Button>
                </div>
              </Col>
              <Col xs={6}>
                {validForm && (
                  <div style={{ float: "left" }}>
                    <Button pill color="success" onClick={() => handleSave()}>
                      Guardar
                    </Button>
                  </div>
                )}
              </Col>
            </Row>
          </Col>
        </Row>
      ) : (
        <Row style={{ height: "60px", margin: "0" }}>
          <Col md={12} style={{ padding: "0px", height: "60px" }}></Col>
        </Row>
      )}
    </>
  );
};

export default VehicleForm;
