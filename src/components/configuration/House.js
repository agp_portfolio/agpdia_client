import React from "react";

import { Card, Col } from "react-bootstrap";
import { Form, Button } from "tabler-react";
import currencyFormater from "../../helpers/currencyFormater";
import Swal from "sweetalert2";
import fetchFunction from "../../helpers/fetchFuction";

const House = ({ props, refetchHouses, showModalHandler }) => {
  const main_currency_symbol = sessionStorage.getItem("main_currency_symbol");
  const main_currency_format = sessionStorage.getItem("main_currency_format");
  const cardStyle = {
    height: "60px",
    marginBottom: "0.1rem",
    borderLeft: "5px solid " + props.account_color,
  };
  const conceptStyle = {
    fontSize: "14px",
    display: "flex",
    alignItems: "center",
    //justifyContent: "center",
    fontWeight: "600",
    //textAlign: "left",
    padding: "0.75rem",
    wordBreak: "break-word",
    hyphens: "auto",
  };
  const accountStyle = {
    fontSize: "12px",
    display: "flex",
    alignItems: "center",
    //justifyContent: "center",
    //textAlign: "center",
    padding: "0.75rem",
  };
  const categoryStyle = {
    fontSize: "12px",
    display: "flex",
    alignItems: "center",
    //justifyContent: "center",
    textAlign: "center",
    padding: "0.75rem",
  };
  const noteStyle = {
    fontSize: "12px",
    display: "flex",
    alignItems: "center",
    padding: "0.75rem",
  };
  const amountStyle = {
    fontSize: "18px",
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0.75rem",
  };

  const functionButtonStyle = {
    fontSize: "16px",
    fontWeight: 600,
    lineHeight: 1.14,
    color: "#354052",
    textAlign: "end",
    display: "flex",
    alignItems: "center",
  };

  const handleClick = (e) => {
    showModalHandler(props._id, false);
  };

  const deleteItems = () => {
    Swal.fire({
      title: "¿Estas seguro de que proceder con la eliminación?",
      // text: `${props.selected.length} ${
      //   props.selected.length == 1 ? " registro" : " registros"
      // } a eliminar`,
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, !Borrar!",
    }).then((result) => {
      if (!result.value) return;
      var [promise, abort] = fetchFunction("houses/" + props._id, "DELETE")
      promise.then((data) => {
        if (!data) {
          Swal.fire({
            text: `No se han encontrado las viviendas a eliminar o estan en uso`,
            icon: "error",
            timer: "2000",
          });
        } else {
          Swal.fire({
            text: `La vivienda se ha eliminado`,
            icon: "success",
            timer: "2000",
          });
          refetchHouses();
        }
      });
    });
  };

  return (
    <Card style={cardStyle}>
      <Card.Body style={{ padding: "0px 10px", display: "flex" }}>
        <Col
          md={12}
          style={{ display: "flex", padding: "0px", cursor: "pointer" }}
        >
          <Col md={9} lg={10} style={conceptStyle} onClick={handleClick}>
            {props.name}
          </Col>
          <Col md={3} lg={2} style={functionButtonStyle}>
            <Button pill outline color="danger" onClick={(e) => deleteItems(e)}>
              Borrar
            </Button>
          </Col>
        </Col>
      </Card.Body>
    </Card>
  );
};

export default House;
