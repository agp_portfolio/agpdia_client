import React, { useEffect, useState, useRef } from "react";

import { Modal, Col, Row } from "react-bootstrap";
import DateRangePicker from "react-bootstrap-daterangepicker";
import { Form, Button } from "tabler-react";
import Select from "react-select";
import fetchFunction from "../../helpers/fetchFuction";

import InputColor from "react-input-color";
import ImagePicker from "react-image-picker";
import "react-image-picker/dist/index.css";

const GroupForm = ({ props, modalSaveHandler }) => {
  const language = sessionStorage.getItem("language");
  const [currencySymbol, setCurrencySymbol] = useState("€");
  const [initialGroup, setInitialGroup] = useState(false);
  const [fetchedGroup, setFetchedGroup] = useState(false);
  const [newGroup, setNewGroup] = useState(false);
  const [validForm, setValidForm] = useState();

  const [accountsOptions, setGroupsOptions] = useState();
  const [currencyOptions, setCurrencyOptions] = useState();

  const [imagesOptions, setImagesOptions] = useState([]);
  // const [conceptsOptions, setConceptsOptions] = useState();
  const [allConceptsOptions, setAllConceptsOptions] = useState();

  const [category, setCategory] = useState();
  const [group, setGroup] = useState();
  const [subgroup, setSubgroup] = useState();
  const [consumptionU, setConsumptionU] = useState();

  const [name, setName] = useState();
  const [initialAmount, setInitialAmount] = useState();
  const [color, setColor] = useState("");
  const [currency, setCurrency] = useState();
  const [image, setImage] = useState();

  const keyRef = useRef(Date.now()); // Needed to update the date
  keyRef.current = Date.now(); // Needed to update the date

  const fetchGroup = (objectId) => {
    var [promise, abort] = fetchFunction("inventories/groups/" + objectId, "GET")
    promise.then((data) => {
      if (!data) {
        // No action
      } else {
        setFetchedGroup(data);
        setFields(data);
      }
    });
  };

  const handleSave = () => {
    var processedNewGroup = newGroup;

    switch (props.modalGroup) {
      case "Add":
        var [promise, abort] = fetchFunction("inventories/groups/", "POST", newGroup)
        promise.then((data) => {
          if (!data) {
            // No action
          } else {
            modalSaveHandler();
          }
        });
        break;
      case "Update":
        var objectId = props.objectId;
        var [promise, abort] = fetchFunction(
          "inventories/groups/" + objectId,
          "PUT",
          processedNewGroup
        )
        promise.then((data) => {
          if (!data) {
            // No action
          } else {
            modalSaveHandler();
          }
        });
        break;

      default:
        break;
    }
  };

  const setFields = (data) => {
    if (data) {
      setName(data.name);
      var backData = {
        name: data.name,
      };
      
    } else {
      setName();
      var backData = {
        name: undefined,
      };

    }
    setInitialGroup(backData);
    setNewGroup(backData);
  };

  React.useEffect(() => {
    console.log(props)
    if (props.modalGroup == "Update") {
      fetchGroup(props.objectId);
    } else if (props.modalGroup == "Add") {
      setFields();
    }

  
  }, []);

  React.useEffect(
    () => {
      var backData = {
        name: name,
      };
       if (
        name
      ) {
        setValidForm(true);
      } else {
        setValidForm(false);
      }
      setNewGroup(backData);
    },
    [
      name,
    ]
  );

  return (
    <>
      <Row style={{ margin: "0px" }} className="groupForm">
        <Col md={12} style={{ padding: "0px" }}>
          <div
            style={{
              background: "rgb(144, 164, 174)",
              color: "white",
              padding: "1rem",
            }}
          >
            <Row>
              <Col md={12}>
                <Form.Group label="Nombre *" style={{ paddingRight: "10px" }}>
                  <Form.Input
                    name="name"
                    className="input"
                    placeholder="Nombre..."
                    autoComplete="off"
                    group="text"
                    value={name}
                    onChange={(e) => {
                      setName(e.target.value);
                    }}
                  />
                </Form.Group>
              </Col>
              
            </Row>
          </div>
        </Col>
      </Row>
      <Row style={{ margin: "0px" }}>
        <Col md={12} style={{ height: "10px" }}></Col>
      </Row>
      {newGroup &&
      JSON.stringify(initialGroup) != JSON.stringify(newGroup) ? (
        <Row className="form-buttons" style={{ margin: "0px" }}>
          <Col md={12} style={{ padding: "0px", height: "60px" }}>
            <Row className="form-buttons" style={{ padding: "inherit" }}>
              <Col xs={6}>
                <div style={{ float: "right" }}>
                  <Button
                    pill
                    outline
                    color="info"
                    onClick={() => setFields(fetchedGroup)}
                  >
                    Deshacer
                  </Button>
                </div>
              </Col>
              <Col xs={6}>
                {validForm && (
                  <div style={{ float: "left" }}>
                    <Button pill color="success" onClick={() => handleSave()}>
                      Guardar
                    </Button>
                  </div>
                )}
              </Col>
            </Row>
          </Col>
        </Row>
      ) : (
        <Row style={{ height: "60px", margin: "0" }}>
          <Col md={12} style={{ padding: "0px", height: "60px" }}></Col>
        </Row>
      )}
    </>
  );
};

export default GroupForm;
