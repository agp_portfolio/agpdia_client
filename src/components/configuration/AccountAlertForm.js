import React, { useEffect, useState, useRef } from "react";

import { Modal, Col, Row } from "react-bootstrap";
import DateRangePicker from "react-bootstrap-daterangepicker";
import { Form, Button } from "tabler-react";
import Select from "react-select";
import fetchFunction from "../../helpers/fetchFuction";
import textsCalendar from "../../texts/Calendar";
import dateFormater from "../../helpers/dateFormater";
import moment from "moment";

import InputColor from "react-input-color";
import ImagePicker from "react-image-picker";
import "react-image-picker/dist/index.css";

const AccountAlertForm = ({ props, modalSaveHandler }) => {
  const language = sessionStorage.getItem("language");
  const [currencySymbol, setCurrencySymbol] = useState("€");
  const [initialAccountAlert, setInitialAccountAlert] = useState(false);
  const [fetchedAccountAlert, setFetchedAccountAlert] = useState(false);
  const [newAccountAlert, setNewAccountAlert] = useState(false);
  const [validForm, setValidForm] = useState();
  const [hideCategorySelect, setHideCategorySelect] = useState(false);
  const [categoriesOptions, setCategoriesOptions] = useState();

  const [accountsOptions, setAccountAlertsOptions] = useState();
  const [currencyOptions, setCurrencyOptions] = useState();

  const [imagesOptions, setImagesOptions] = useState([]);
  // const [conceptsOptions, setConceptsOptions] = useState();
  const [allConceptsOptions, setAllConceptsOptions] = useState();

  const [accountAlert, setAccountAlert] = useState();
  const [group, setGroup] = useState();
  const [subgroup, setSubgroup] = useState();
  const [consumptionU, setConsumptionU] = useState();

  const [name, setName] = useState();
  const [note, setNote] = useState();
  const [category, setCategory] = useState();
  const [concept, setConcept] = useState();
  const [isActive, setIsActive] = useState();
  const [startDate, setStartDate] = useState();
  const [endDate, setEndDate] = useState();
  const [periodicityMonths, setPeriodicityMonths] = useState();
  const [periodsToAlert, setPeriodsToAlert] = useState();
  const [initialAmount, setInitialAmount] = useState();
  const [color, setColor] = useState("");
  const [currency, setCurrency] = useState();
  const [image, setImage] = useState();

  const keyRef = useRef(Date.now()); // Needed to update the date
  keyRef.current = Date.now(); // Needed to update the date

  const fetchAccountAlert = (objectId) => {
    var [promise, abort] = fetchFunction("alerts/accounts/" + objectId, "GET")
    promise.then((data) => {
      if (!data) {
        // No action
      } else {
        setFetchedAccountAlert(data);
        setFields(data);
      }
    });  
    
  };

  const fetchConcepts = () => {
    var [promise, abort] = fetchFunction("accounts/aggs/concepts/filter_list", "POST")
    promise.then((data) => {
      if (!data) {
        // No action
      } else {
        var tempCategories = [];
            var tempConcepts = {};
            data.forEach((category) => {
              tempCategories.push({
                label: category.name,
                value: category._id,
              });
              tempConcepts[category._id] = [];
              // tempConcepts[undefined] = [<option></option>];
              category.concepts.forEach((concept) => {
                tempConcepts[category._id].push({
                  label: concept.name,
                  value: concept.id,
                });
              });
            });
            setCategoriesOptions(tempCategories);
            setAllConceptsOptions(tempConcepts);
      }
    });
  };

  const handleSave = () => {
    var processedNewAccountAlert = newAccountAlert;
  
    switch (props.modalType) {
      case "Add":
        processedNewAccountAlert.start_date = dateFormater(
          processedNewAccountAlert.start_date,
          "database"
        );
        processedNewAccountAlert.end_date = dateFormater(
          processedNewAccountAlert.end_date,
          "database"
        );
        var [promise, abort] = fetchFunction("alerts/accounts/", "POST", processedNewAccountAlert)
        promise.then((data) => {
          if (!data) {
            // No action
          } else {
            modalSaveHandler();
          }
        });  
        break;
      case "Update":
        processedNewAccountAlert.start_date = dateFormater(
          processedNewAccountAlert.start_date,
          "database"
        );
        processedNewAccountAlert.end_date = dateFormater(
          processedNewAccountAlert.end_date,
          "database"
        );
        var objectId = props.objectId;
        var [promise, abort] = fetchFunction(
          "alerts/accounts/" + objectId,
          "PUT",
          processedNewAccountAlert
        )
        promise.then((data) => {
          if (!data) {
            // No action
          } else {
            modalSaveHandler();
          }
        });
        break;

      default:
        break;
    }
  };

  const setFields = (data) => {
    if (data) {
      setName(data.name);
      setConcept({ value: data.concept_id, label: data.concept_name });
      setCategory({ value: data.category_id, label: data.category_name });
      setHideCategorySelect(true);
      setPeriodicityMonths(data.periodicity_months)
      setPeriodsToAlert(data.periods_to_alert)
      setStartDate(dateFormater(data.start_date, "show"))
      setEndDate(dateFormater(data.end_date, "show"))
      setNote(data.note)
      setIsActive(data.is_active ? "true" : "false")
      var backData = {
        name: data.name,
        concept_id: data.concept_id,
        category_id: data.category,
        periodicity_months: data.periodicity_months,
        periods_to_alert: data.periods_to_alert,
        start_date: dateFormater(data.start_date, "show"),
        end_date: dateFormater(data.end_date, "show"),
        note: data.note,
        is_active: data.is_active,
      };
    } else {
      setName();
      setConcept();
      setCategory();
      setHideCategorySelect(false);
      setPeriodicityMonths()
      setPeriodsToAlert()
      setStartDate(dateFormater(new Date(), "show"));
      setEndDate(dateFormater(moment(dateFormater('31/12/9999', "database")).toDate(), "show"))
      setNote()
      setIsActive("true")
      var backData = {
        name: undefined,
        concept_id: undefined,
        category_id: undefined,
        periodicity_months: undefined,
        periods_to_alert: undefined,
        start_date: dateFormater(new Date(), "show"),
        end_date: dateFormater(moment(dateFormater('31/12/9999', "database")).toDate(), "show"),
        note: undefined,
        is_active: true,
      };
    }
    setInitialAccountAlert(backData);
    setNewAccountAlert(backData);
  };

  React.useEffect(() => {
    // console.log(props);
    fetchConcepts();
    if (props.modalType == "Update") {
      fetchAccountAlert(props.objectId);
    } else if (props.modalType == "Add") {
      setFields();
    }
  }, []);

  React.useEffect(() => {
    var backData = {
      name: name,
      concept_id: concept ? concept.value : undefined,
      periodicity_months: periodicityMonths,
      periods_to_alert: periodsToAlert,
      start_date: startDate,
      end_date: endDate,
      note: note,
      is_active: isActive == "true" ? true : false,
    };
    if (name && concept && periodicityMonths && periodsToAlert) {
      setValidForm(true);
    } else {
      setValidForm(false);
    }
    setNewAccountAlert(backData);
  }, [name, concept, periodicityMonths, periodsToAlert, startDate, endDate, isActive, note]);

  return (
    <>
      <Row style={{ margin: "0px" }} className="accountAlertForm coloredForm">
        <Col md={12} style={{ padding: "0px" }}>
          <div
            style={{
              background: "rgb(144, 164, 174)",
              color: "white",
              padding: "1rem",
            }}
          >
            <Row>
              <Col xs={6} md={6}>
                <Form.Group label="Nombre *" style={{ paddingRight: "10px" }}>
                  <Form.Input
                    name="name"
                    className="input"
                    placeholder="Nombre..."
                    autoComplete="off"
                    type="text"
                    value={name ? name : ""}
                    onChange={(e) => {
                      setName(e.target.value);
                    }}
                  />
                </Form.Group>
              </Col>
              <Col xs={6} md={6}>
                <Form.Group
                  label={
                    hideCategorySelect ? category.label + " *" : "Categoría *"
                  }
                >
                  <div style={{ color: "rgb(73, 80, 87)" }}>
                    <Select
                      openMenuOnFocus={hideCategorySelect}
                      onChange={(e) => {
                        if (hideCategorySelect && e.value) {
                          setConcept(e);
                        } else {
                          setCategory(e);
                          setHideCategorySelect(true);
                        }
                      }}
                      onMenuOpen={(e) => {
                        if (concept) {
                          setCategory();
                          setConcept();
                          setHideCategorySelect(false);
                        }
                      }}
                      menuIsOpen={category && !concept}
                      value={{
                        label: concept
                          ? concept.label
                          : "Selecciona " +
                            (hideCategorySelect ? "Concepto" : "Categoría"),
                      }}
                      options={
                        hideCategorySelect && !concept
                          ? category
                            ? allConceptsOptions[category.value]
                            : { label: "Seleccionar Concepto" }
                          : categoriesOptions
                      }
                    />
                  </div>
                </Form.Group>
              </Col>
              <Col xs={6} md={3}>
                <Form.Group
                  label="Cada ? meses *"
                  style={{ paddingRight: "10px" }}
                >
                  <Form.Input
                    name="periodicityMonths"
                    className="input"
                    placeholder="1"
                    autoComplete="off"
                    type="text"
                    value={
                      periodicityMonths != undefined
                        ? periodicityMonths.toString()
                        : ""
                    }
                    onChange={(e) => {
                      var temp = e.target.value;

                      if (/^[0-9]{0,9}?$/i.test(temp))
                        temp
                          ? setPeriodicityMonths(temp)
                          : setPeriodicityMonths();
                    }}
                  />
                </Form.Group>
              </Col>
              <Col xs={6} md={3}>
                <Form.Group
                  label="Nº de periodos *"
                  style={{ paddingRight: "10px" }}
                >
                  <Form.Input
                    name="periodsToAlert"
                    className="input"
                    placeholder="1"
                    autoComplete="off"
                    type="text"
                    value={
                      periodsToAlert != undefined
                        ? periodsToAlert.toString()
                        : ""
                    }
                    onChange={(e) => {
                      var temp = e.target.value;

                      if (/^[0-9]{0,9}?$/i.test(temp))
                        temp ? setPeriodsToAlert(temp) : setPeriodsToAlert();
                    }}
                  />
                </Form.Group>
              </Col>
              <Col xs={6} md={3}>
                <Form.Group label="Fecha inicio *">
                  <DateRangePicker
                    key={keyRef.current} // Needed to update the date
                    initialSettings={{
                      singleDatePicker: true,
                      opens: "center",
                      startDate: startDate,
                      locale: {
                        format: "DD/MM/YYYY",
                        firstDay: 1,
                        cancelLabel: textsCalendar.cancelButton[language],
                        applyLabel: textsCalendar.applyButton[language],
                        todayLabel: "Hoy",
                        customRangeLabel:
                          textsCalendar.customRangeLabel[language],
                        daysOfWeek: textsCalendar.daysOfWeek[language],
                        monthNames: textsCalendar.monthNames[language],
                      },
                    }}
                    onCallback={(e) => setStartDate(dateFormater(e._d, "show"))}
                    onShow={(e) => {
                      e.target.blur();
                    }}
                  >
                    <input
                      type="text"
                      className="form-control"
                      style={{ textAlign: "center" }}
                    />
                  </DateRangePicker>
                </Form.Group>
              </Col>
              <Col xs={6} md={3}>
                <Form.Group label="Fecha fin *">
                  <DateRangePicker
                    key={keyRef.current} // Needed to update the date
                    initialSettings={{
                      singleDatePicker: true,
                      opens: "center",
                      startDate: endDate,
                      locale: {
                        format: "DD/MM/YYYY",
                        firstDay: 1,
                        cancelLabel: textsCalendar.cancelButton[language],
                        applyLabel: textsCalendar.applyButton[language],
                        todayLabel: "Hoy",
                        customRangeLabel:
                          textsCalendar.customRangeLabel[language],
                        daysOfWeek: textsCalendar.daysOfWeek[language],
                        monthNames: textsCalendar.monthNames[language],
                      },
                    }}
                    onCallback={(e) => setEndDate(dateFormater(e._d, "show"))}
                    onShow={(e) => {
                      e.target.blur();
                    }}
                  >
                    <input
                      type="text"
                      className="form-control"
                      style={{ textAlign: "center" }}
                    />
                  </DateRangePicker>
                </Form.Group>
              </Col>
              <Col xs={8} md={9}>
                <Form.Group label="Nota">
                  <Form.Textarea
                    clasName="field-note"
                    name="note"
                    placeholder="Nota informativa..."
                    value={note ? note : " "}
                    onChange={(e) => {
                      if (e.target.value.length) {
                        setNote(e.target.value);
                      } else {
                        setNote();
                      }
                    }}
                    autoComplete="off"
                    rows={1}
                  />
                </Form.Group>
              </Col>
              <Col xs={4} md={3}>
                <Form.Group label="¿Activo? *" className="group">
                  <Form.SelectGroup
                    pills
                    value={isActive}
                    onChange={(e) => {
                      console.log(e.target.value);
                      setIsActive(e.target.value);
                    }}
                  >
                    <Form.SelectGroupItem
                      icon="check"
                      label="Si"
                      name="group"
                      value={"true"}
                      checked={isActive == "true"}
                      onChange={(e) => {}}
                    />
                    <Form.SelectGroupItem
                      icon="times"
                      label="No"
                      name="No"
                      value={"false"}
                      checked={isActive == "false"}
                      onChange={(e) => {}}
                    />
                  </Form.SelectGroup>
                </Form.Group>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
      <Row style={{ margin: "0px" }}>
        <Col md={12} style={{ height: "10px" }}></Col>
      </Row>
      {newAccountAlert &&
      JSON.stringify(initialAccountAlert) != JSON.stringify(newAccountAlert) ? (
        <Row className="form-buttons" style={{ margin: "0px" }}>
          <Col md={12} style={{ padding: "0px", height: "60px" }}>
            <Row className="form-buttons" style={{ padding: "inherit" }}>
              <Col xs={6}>
                <div style={{ float: "right" }}>
                  <Button
                    pill
                    outline
                    color="info"
                    onClick={() => setFields(fetchedAccountAlert)}
                  >
                    Deshacer
                  </Button>
                </div>
              </Col>
              <Col xs={6}>
                {validForm && (
                  <div style={{ float: "left" }}>
                    <Button pill color="success" onClick={() => handleSave()}>
                      Guardar
                    </Button>
                  </div>
                )}
              </Col>
            </Row>
          </Col>
        </Row>
      ) : (
        <Row style={{ height: "60px", margin: "0" }}>
          <Col md={12} style={{ padding: "0px", height: "60px" }}></Col>
        </Row>
      )}
    </>
  );
};

export default AccountAlertForm;
