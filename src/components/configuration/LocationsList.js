// @flow

import React from "react";
import { Redirect, useParams } from "react-router-dom";

import Infinite from "react-infinite";
import Select from "react-select";

import ModalForm from "../ModalForm";
import { Container, Row, Col, Card, ModalTitle } from "react-bootstrap";
import { Form, Button } from "tabler-react";
//import { Card } from "tabler-react";
import fetchFunction from "../../helpers/fetchFuction";

import AccountRecord from "../accountRecords/AccountRecord";
import Location from "./Location";
import LocationForm from "./LocationForm";
import AccountRecordDate from "../accountRecords/AccountRecordDate";
import AccountRecordTotal from "../accountRecords/AccountRecordTotal";
import AccountRecordForm from "../accountRecords/AccountRecordForm";

import backButtonWithModal from "../../helpers/backButtonWithModal";

function InventoryLocationsList({ props, updateSignal }) {
  const [error, setError] = React.useState();
  const [modalTitle, setModalTitle] = React.useState("");
  const [locations, setLocations] = React.useState();
  const [itemsToForm, setItemsToForm] = React.useState([]);
  const [modalType, setModalType] = React.useState();
  const [inventory, setInventory] = React.useState();
  const [inventoryOptions, setInventoryOptions] = React.useState();

  var modalRef = React.useRef();

  const infoStyle = {
    fontSize: "14px",
    // display: "flex",
    alignItems: "center",
    fontWeight: "600",
    padding: "0.25rem",
    textAlign: "center",
  };

  const fetchLocations = () => {
    if(!inventory){
      setLocations([])
      return
    } 
    var filter = { inventory_id: inventory.value}

    var [promise, abort] = fetchFunction("inventories/aggs/locations/ofInventory", "POST", filter)
    promise.then((data) => {
      if (!data) {
        // No action
      } else {
        var table = [];
            data.forEach((location) => {
              table.push(
                <Location
                  key={location.id}
                  props={location}
                  showModalHandler={showModalHandler}
                  refetchLocations={refetchLocations}
                />
              );
            });
            setLocations(table);
      }
    });
  };

  const fetchInventories = () => {
    var [promise, abort] = fetchFunction("inventories/inventories/", "GET")
    promise.then((data) => {
      if (!data) {
        // No action
      } else {
        var table = [];
            data.forEach((inventory) => {
              table.push(
                {label:inventory.name, value:inventory._id}
              );
            });
            setInventoryOptions(table);
      }
    });
  };

  const showModalHandler = (objectId, toAdd) => {
    objectId ? setItemsToForm([objectId]) : setItemsToForm();
    if (toAdd) {
      setModalTitle("Añadir ubicación");
      setModalType("Add");
    } else {
      setModalTitle("Editar ubicación");
      setModalType("Update");
    }
    backButtonWithModal.neutralizeBack(modalRef.current);
    modalRef.current.handleShow();
    modalRef.current.task(objectId, toAdd);
  };

  const modalSaveHandler = () => {
    modalRef.current.handleHide();

    if (updateSignal) {
      updateSignal();
    }
    refetchLocations();
  };

  const refetchLocations = () => {
    fetchLocations();
  };

  React.useEffect(() => {
    if(!inventoryOptions) fetchInventories()
    refetchLocations(true);
  }, [inventory]);


  return (
    <>
      {error && (
        <div className={"alert alert-" + error.type} role="alert">
          {error.message}
        </div>
      )}

      <Col lg={12}>
        <Card
          className="account-record-total"
          style={{ marginBottom: "0.1rem", background: "ghostwhite" }}
        >
          <Card.Body style={{ padding: "0px 10px", display: "flex" }}>
            <Col md={6} xs={6} style={infoStyle}>
              
                <div style={{ color: "rgb(73, 80, 87)", width: "100%" }}>
                  <Select
                    onChange={(e) => {
                      setInventory(e);
                    }}
                    isSearchable={false}
                    value={{
                      label: inventory
                        ? inventory.label
                        : "Selecciona inventorio",
                    }}
                    options={inventoryOptions}
                  />
                </div>

            </Col>
            <Col md={6} xs={6} style={infoStyle}>
              <Button
                pill
                outline
                color="primary"
                onClick={() => showModalHandler(undefined, true)}
              >
                Añadir
              </Button>
            </Col>
          </Card.Body>
        </Card>
        {!locations ? (
          <h1>Loading...</h1>
        ) : (
          <Infinite
            elementHeight={60} // The height of the tallest element in the list
            useWindowAsScrollContainer
          >
            {locations}
          </Infinite>
        )}
      </Col>
      <ModalForm
        ref={modalRef}
        props={{
          title: modalTitle,
          className:"locationForm coloredform"
        }}
      >
        <LocationForm
          props={{
            objectId: itemsToForm,
            modalType: modalType,
            inventoryOptions:inventoryOptions,
          }}
          modalSaveHandler={modalSaveHandler}
        />
      </ModalForm>
    </>
  );
}

export default InventoryLocationsList;
