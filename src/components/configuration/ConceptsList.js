// @flow

import React from "react";
import { Redirect, useParams } from "react-router-dom";

import Infinite from "react-infinite";
import Select from "react-select";

import ModalForm from "../ModalForm";
import { Container, Row, Col, Card, ModalTitle } from "react-bootstrap";
import { Form, Button } from "tabler-react";
//import { Card } from "tabler-react";
import fetchFunction from "../../helpers/fetchFuction";

import AccountRecord from "../accountRecords/AccountRecord";
import Concept from "./Concept";
import ConceptForm from "./ConceptForm";
import AccountRecordDate from "../accountRecords/AccountRecordDate";
import AccountRecordTotal from "../accountRecords/AccountRecordTotal";
import AccountRecordForm from "../accountRecords/AccountRecordForm";

import backButtonWithModal from "../../helpers/backButtonWithModal";

function AccountRecordsList({ props, updateSignal }) {
  const [error, setError] = React.useState();
  const [modalTitle, setModalTitle] = React.useState("");
  const [concepts, setConcepts] = React.useState();
  const [itemsToForm, setItemsToForm] = React.useState([]);
  const [modalType, setModalType] = React.useState();
  const [category, setCategory] = React.useState();
  const [categoryOptions, setCategoryOptions] = React.useState();

  var modalRef = React.useRef();

  const infoStyle = {
    fontSize: "14px",
    // display: "flex",
    alignItems: "center",
    fontWeight: "600",
    padding: "0.25rem",
    textAlign: "center",
  };

  const fetchConcepts = () => {
    if(!category){
      setConcepts([])
      return
    } 
    var filter = { category_id: category.value}

    var [promise, abort] = fetchFunction("accounts/aggs/concepts/ofCategory", "POST", filter)
    promise.then((data) => {
      if (!data) {
        // No action
      } else {
        var table = [];
        data.forEach((concept) => {
          table.push(
            <Concept
              key={concept.id}
              props={concept}
              showModalHandler={showModalHandler}
              refetchConcepts={refetchConcepts}
            />
          );
        });
        setConcepts(table);
      }
    });
  };

  const fetchCategories = () => {
    var [promise, abort] = fetchFunction("accounts/categories/", "GET")
    promise.then((data) => {
      if (!data) {
        // No action
      } else {
        var table = [];
        data.forEach((category) => {
          table.push(
            {label:category.name, value:category._id}
          );
        });
        setCategoryOptions(table);
      }
    });
  };

  const showModalHandler = (objectId, toAdd) => {
    objectId ? setItemsToForm([objectId]) : setItemsToForm();
    if (toAdd) {
      setModalTitle("Añadir concepto");
      setModalType("Add");
    } else {
      setModalTitle("Editar concepto");
      setModalType("Update");
    }
    backButtonWithModal.neutralizeBack(modalRef.current);
    modalRef.current.handleShow();
    modalRef.current.task(objectId, toAdd);
  };

  const modalSaveHandler = () => {
    modalRef.current.handleHide();

    if (updateSignal) {
      updateSignal();
    }
    refetchConcepts();
  };

  const refetchConcepts = () => {
    fetchConcepts();
  };

  React.useEffect(() => {
    if(!categoryOptions) fetchCategories()
    refetchConcepts(true);
  }, [category]);


  return (
    <>
      {error && (
        <div className={"alert alert-" + error.type} role="alert">
          {error.message}
        </div>
      )}

      <Col lg={12}>
        <Card
          className="account-record-total"
          style={{ marginBottom: "0.1rem", background: "ghostwhite" }}
        >
          <Card.Body style={{ padding: "0px 10px", display: "flex" }}>
            <Col md={6} xs={6} style={infoStyle}>
              
                <div style={{ color: "rgb(73, 80, 87)", width: "100%" }}>
                  <Select
                    onChange={(e) => {
                      setCategory(e);
                    }}
                    isSearchable={false}
                    value={{
                      label: category
                        ? category.label
                        : "Selecciona categoría",
                    }}
                    options={categoryOptions}
                  />
                </div>

            </Col>
            <Col md={6} xs={6} style={infoStyle}>
              <Button
                pill
                outline
                color="primary"
                onClick={() => showModalHandler(undefined, true)}
              >
                Añadir
              </Button>
            </Col>
          </Card.Body>
        </Card>
        {!concepts ? (
          <h1>Loading...</h1>
        ) : (
          <Infinite
            elementHeight={60} // The height of the tallest element in the list
            useWindowAsScrollContainer
          >
            {concepts}
          </Infinite>
        )}
      </Col>
      <ModalForm
        ref={modalRef}
        props={{
          title: modalTitle,
          className:"conceptForm coloredform"
        }}
      >
        <ConceptForm
          props={{
            objectId: itemsToForm,
            modalType: modalType,
            categoryOptions:categoryOptions,
          }}
          modalSaveHandler={modalSaveHandler}
        />
      </ModalForm>
    </>
  );
}

export default AccountRecordsList;
