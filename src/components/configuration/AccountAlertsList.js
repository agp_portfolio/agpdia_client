// @flow

import React from "react";
import { Redirect, useParams } from "react-router-dom";

import Infinite from "react-infinite";

import ModalForm from "../ModalForm";
import { Container, Row, Col, Card, ModalTitle } from "react-bootstrap";
import { Form, Button } from "tabler-react";
//import { Card } from "tabler-react";
import fetchFunction from "../../helpers/fetchFuction";

import AccountRecord from "../accountRecords/AccountRecord";
import AccountAlert from "./AccountAlert";
import AccountAlertForm from "./AccountAlertForm";
import AccountRecordDate from "../accountRecords/AccountRecordDate";
import AccountRecordTotal from "../accountRecords/AccountRecordTotal";
import AccountRecordForm from "../accountRecords/AccountRecordForm";

import backButtonWithModal from "../../helpers/backButtonWithModal";

function AccountRecordsList({ props, updateSignal }) {
  const [error, setError] = React.useState();
  const [modalTitle, setModalTitle] = React.useState("");
  const [accountAlerts, setAccountAlerts] = React.useState();
  const [itemsToForm, setItemsToForm] = React.useState([]);
  const [modalType, setModalType] = React.useState();

  var modalRef = React.useRef();

  const infoStyle = {
    fontSize: "14px",
    // display: "flex",
    alignItems: "center",
    fontWeight: "600",
    padding: "0.25rem",
    textAlign: "center",
  };

  const fetchAccountAlerts = () => {
    var [promise, abort] = fetchFunction("alerts/accounts/", "GET")
    promise.then((data) => {
      if (!data) {
        // No action
      } else {
        var table = [];
        data.forEach((accountAlert) => {
          table.push(
            <AccountAlert
              key={accountAlert.id}
              props={accountAlert}
              showModalHandler={showModalHandler}
              refetchAccountAlerts={refetchAccountAlerts}
            />
          );
        });
        setAccountAlerts(table);
      }
    });  
  };

  const showModalHandler = (objectId, toAdd) => {
    objectId ? setItemsToForm([objectId]) : setItemsToForm();
    if (toAdd) {
      setModalTitle("Añadir categoría");
      setModalType("Add");
    } else {
      setModalTitle("Editar categoría");
      setModalType("Update");
    }
    backButtonWithModal.neutralizeBack(modalRef.current);
    modalRef.current.handleShow();
    modalRef.current.task(objectId, toAdd);
  };

  const modalSaveHandler = () => {
    modalRef.current.handleHide();

    if (updateSignal) {
      updateSignal();
    }
    refetchAccountAlerts();
    // The window is reloaded to show the updated message alerts
    window.location.reload(); 
  };

  const refetchAccountAlerts = () => {
    fetchAccountAlerts();
  };

  React.useEffect(() => {
    refetchAccountAlerts(true);
  }, []);

  React.useEffect(() => {

  }, [accountAlerts]);

  return (
    <>
      {error && (
        <div className={"alert alert-" + error.type} role="alert">
          {error.message}
        </div>
      )}

      <Col lg={12}>
        <Card
          className="account-record-total"
          style={{ marginBottom: "0.1rem", background: "ghostwhite" }}
        >
          <Card.Body style={{ padding: "0px 10px", display: "flex" }}>
            <Col md={12} xs={12} style={infoStyle}>
              <Button
                pill
                outline
                color="primary"
                onClick={() => showModalHandler(undefined, true)}
              >
                Añadir
              </Button>
            </Col>
          </Card.Body>
        </Card>
        {!accountAlerts ? (
          <h1>Loading...</h1>
        ) : (
          <Infinite
            elementHeight={60} // The height of the tallest element in the list
            useWindowAsScrollContainer
          >
            {accountAlerts}
          </Infinite>
        )}
      </Col>
      <ModalForm
        ref={modalRef}
        props={{
          title: modalTitle,
        }}
      >
        <AccountAlertForm
          props={{
            objectId: itemsToForm,
            modalType: modalType,
          }}
          modalSaveHandler={modalSaveHandler}
        />
      </ModalForm>
    </>
  );
}

export default AccountRecordsList;
