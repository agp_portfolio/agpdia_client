import React from "react";

import { Card, Col } from "react-bootstrap";
import { Form, Button } from "tabler-react";
import currencyFormater from "../../helpers/currencyFormater";
import Swal from "sweetalert2";
import fetchFunction from "../../helpers/fetchFuction";
import dateFormater from "../../helpers/dateFormater";

const AccountAlert = ({ props, refetchAccountAlerts, showModalHandler }) => {
  const main_currency_symbol = sessionStorage.getItem("main_currency_symbol");
  const main_currency_format = sessionStorage.getItem("main_currency_format");
  var color;
  
  if(new Date(props.end_date) < new Date()){
    color = "gray"
  }else if(new Date(props.start_date) > new Date()){
    color = "deepskyblue"
  }else if(props.is_active){
    color = "green"
  }else{
    color = "orange"
  }
  
  
  const cardStyle = {
    height: "60px",
    marginBottom: "0.1rem",
    borderLeft: "5px solid " + color,
    color: props.state == "disposed" && "#80808094",
  };
  const conceptStyle = {
    fontSize: "14px",
    display: "flex",
    alignItems: "center",
    //justifyContent: "center",
    fontWeight: "600",
    //textAlign: "left",
    padding: "0.75rem",
    wordBreak: "break-word",
    hyphens: "auto",
  };
  const accountStyle = {
    fontSize: "12px",
    display: "flex",
    alignItems: "center",
    //justifyContent: "center",
    //textAlign: "center",
    padding: "0.75rem",
  };
  const accountAlertStyle = {
    fontSize: "12px",
    display: "flex",
    alignItems: "center",
    //justifyContent: "center",
    textAlign: "center",
    padding: "0.75rem",
  };
  const noteStyle = {
    fontSize: "12px",
    display: "flex",
    alignItems: "center",
    padding: "0.75rem",
  };
  const amountStyle = {
    fontSize: "18px",
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0.75rem",
  };

  const functionButtonStyle = {
    fontSize: "16px",
    fontWeight: 600,
    lineHeight: 1.14,
    color: "#354052",
    textAlign: "end",
    display: "flex",
    alignItems: "center",
  };

  const handleClick = (e) => {
    showModalHandler(props._id, false);
  };

  const deleteItems = () => {
    Swal.fire({
      title: "¿Estas seguro de que proceder con la eliminación?",
      // text: `${props.selected.length} ${
      //   props.selected.length == 1 ? " registro" : " registros"
      // } a eliminar`,
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, !Borrar!",
    }).then((result) => {
      if (!result.value) return;
      var [promise, abort] = fetchFunction("alerts/accounts/" + props._id, "DELETE")
      promise.then((data) => {
        if (!data) {
          Swal.fire({
            text: `No se han encontrado las categorias a eliminar o estan en uso`,
            icon: "error",
            timer: "2000",
          });
        } else {
          Swal.fire({
            text: `La categoría se ha eliminado`,
            icon: "success",
            timer: "2000",
          });
          refetchAccountAlerts();
        }
      });

    });
  };

  return (
    <Card style={cardStyle}>
      <Card.Body style={{ padding: "0px 10px", display: "flex" }}>
        <Col
          md={12}
          style={{ display: "flex", padding: "0px", cursor: "pointer" }}
        >
          <Col xs={8} md={9} lg={10} style={conceptStyle} onClick={handleClick}>
            <Col lg={3} md={4} xs={5} style={conceptStyle}>
              {props.name.length > 20 ? props.name.substr(0,17) + "...":props.name}
            </Col>
            <Col lg={3} md={4} xs={7} style={conceptStyle}>
              {dateFormater(props.start_date, "show")}
              {" → "}
              {props.end_date.substr(0,4) >= 3000 ? "∞" : dateFormater(props.end_date, "show")}
            </Col>
            <Col lg={3} md={5} className="d-none d-md-flex" style={noteStyle}>
              {"Hasta " + props.periods_to_alert + (props.periods_to_alert==1 ? " aviso":" avisos" )+" con " + props.periodicity_months +(props.periodicity_months==1 ? " mes":" meses" )+ " de frecuencia"}
            </Col>
            <Col lg={3} md={3} className="d-none d-lg-flex" style={noteStyle}>
              {(props.note && props.note.length > 60) ? props.note.substr(0,57) + "...":props.note}
            </Col>
          </Col>
          
          <Col xs={4} md={3} lg={2} style={functionButtonStyle}>
            <Button pill outline color="danger" onClick={(e) => deleteItems(e)}>
              Borrar
            </Button>
          </Col>
        </Col>
      </Card.Body>
    </Card>
  );
};

export default AccountAlert;
