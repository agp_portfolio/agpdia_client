import React from "react";

import { Card, Col, Row } from "react-bootstrap";
import { Icon } from "tabler-react";
import { useHistory } from "react-router-dom";
import dateFormater from "../../helpers/dateFormater";
import currencyFormater from "../../helpers/currencyFormater";
import defaultImage from "../../img/inventories_images/default.png";
import Swal from "sweetalert2";
import fetchFunction from "../../helpers/fetchFuction";
import dateDiff from "../../helpers/dateDiff";

function importAll(r) {
  let images = {};
  r.keys().map((item, index) => {
    images[item.replace("./", "")] = r(item);
  });
  return images;
}
const images = importAll(
  require.context("../../img/accounts_images", false, /\.(png|jpe?g|svg)$/)
);

const InventoryItem = ({ props, refetchItems, showModalHandler }) => {
  const main_currency_symbol = sessionStorage.getItem("main_currency_symbol");
  const main_currency_format = sessionStorage.getItem("main_currency_format");
  const history = useHistory();
  var color;
  switch (props.state) {
    case "good":
      color = "green";
      break;
    case "regular":
      color = "orange";
      break;
    case "bad":
      color = "red";
      break;
    case "disposed":
      color = "gray";
      break;

    default:
      break;
  }
  const cardStyle = {
    marginBottom: "0.1rem",
    borderLeft: "5px solid " + color,
    color: props.state == "disposed" && "#80808094",
  };
  const nameStyle = {
    fontSize: "18px",
    // display: "flex",
    alignItems: "center",
    //justifyContent: "center",
    fontWeight: "600",
    //textAlign: "left",
    paddingTop: "1rem",
    wordBreak: "break-word",
    hyphens: "auto",
  };
  const inventoryStyle = {
    fontSize: "15px",
    // display: "flex",
    alignItems: "center",
    //justifyContent: "center",
    fontWeight: "600",
    //textAlign: "left",
    // paddingTop: "0.75rem",
    wordBreak: "break-word",
    hyphens: "auto",
  };
  const symbolStyle = {
    fontWeight: "bold",
    display: "flex",
    alignItems: "center",
    // padding: "0.75rem",
  };
  const otherStyle = {
    fontSize: "14px",
    // display: "flex",
    alignItems: "center",
    //justifyContent: "center",
    // fontWeight: "600",
    //textAlign: "left",
    // paddingTop: "0.75rem",
    wordBreak: "break-word",
    hyphens: "auto",
  };

  const dateStyle = {
    fontSize: "14px",
    // display: "flex",
    alignItems: "center",
    //justifyContent: "center",
    // fontWeight: "600",
    //textAlign: "left",
    paddingTop: "1rem",
    // wordBreak: "break-word",
    // hyphens: "auto",
  };
  const noteStyle = {
    fontSize: "14px",
    // display: "flex",
    alignItems: "center",
    //justifyContent: "center",
    // fontWeight: "600",
    //textAlign: "left",
    paddingBottom: "1rem",
    wordBreak: "break-word",
    hyphens: "auto",
  };

  const amountStyle = {
    fontSize: "15px",
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    // padding: "0.75rem",
    // marginTop: "35px",
  };
  const iconStyle = {
    textAlign: "end",
    marginTop: "1rem",
  };

  const deleteItem = () => {
    Swal.fire({
      title: "¿Estas seguro de que proceder con la eliminación?",
      // text: `${props.selected.length} ${
      //   props.selected.length == 1 ? " registro" : " registros"
      // } a eliminar`,
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, !Borrar!",
    }).then((result) => {
      if (!result.value) return;
      var [promise, abort] = fetchFunction("inventories/items/" + props.id, "DELETE")
      promise.then((data) => {
        if (!data) {
          Swal.fire({
            text: `No se han encontrado los elementos a eliminar o estan en uso`,
            icon: "error",
            timer: "2000",
          });
        } else {
          Swal.fire({
            text: `El elementos se ha eliminado`,
            icon: "success",
            timer: "1000",
          });
          refetchItems();
        }
      });
    });
  };

  return (
    <Card style={cardStyle}>
      <Card.Body style={{ padding: "0px 10px", display: "flex" }}>
        <Col md={12} xs={12}>
          <Row>
            <Col md={5} xs={5} style={symbolStyle}>
              {props.image ? (
                <img
                  src={props.image}
                  style={{
                    margin: "22px 0px",
                    height: "100px",
                    width: "100px",
                    objectFit: "contain",
                    opacity: props.state == "disposed" && "0.4",
                  }}
                />
              ) : (
                <img
                  src={defaultImage}
                  style={{
                    margin: "22px 0px",
                    height: "100px",
                    width: "100px",
                    objectFit: "contain",
                    opacity: props.state == "disposed" && "0.4",
                  }}
                />
              )}
            </Col>
            <Col md={7} xs={7}>
              <Row>
                <Col md={12} xs={12} style={nameStyle}>
                  {props.name}
                </Col>
                <Col
                  md={12}
                  xs={12}
                  className="d-none d-md-flex"
                  style={dateStyle}
                >
                  {dateFormater(props.acquisition_date, "show")}
                  {props.dispose_date
                    ? "  →  " +
                      dateFormater(props.dispose_date, "show") +
                      ` (~ ${dateDiff(
                        props.acquisition_date,
                        props.dispose_date,
                        true
                      )})`
                    : ` (~ ${dateDiff(props.acquisition_date, new Date(), true)})`}
                </Col>
                <Col md={12} style={inventoryStyle}>
                  {props.inventory}
                </Col>
                <Col md={12} style={otherStyle}>
                  {props.location}
                </Col>
                <Col md={12} className="d-none d-md-flex" style={amountStyle}>
                  <div>
                    <p style={{ marginBottom: "0px" }}>
                      <strong>
                        {currencyFormater(
                          props.acquisition_price,
                          props.currency_format,
                          props.currency_symbol
                        )}{" "}
                      </strong>
                    </p>
                    {!props.has_main_currency && (
                      <p style={{ marginBottom: "0px", fontSize: "12px" }}>
                        ~{" "}
                        {currencyFormater(
                          props.initial_amount * props.exchange,
                          main_currency_format,
                          main_currency_symbol
                        )}
                      </p>
                    )}
                  </div>
                </Col>
                {/* <Col
                  md={0}
                  xs={0}
                  className="d-none d-md-flex"
                  style={dateStyle}
                >
                  {dateFormater(props.acquisition_date, "show")}
                  {props.dispose_date
                    ? "  →  " +
                      dateFormater(props.dispose_date, "show") +
                      ` (~ ${dateDiff(
                        props.acquisition_date,
                        props.dispose_date,
                        true
                      )})`
                    : ` (~ ${dateDiff(props.acquisition_date, new Date(), true)})`}
                </Col> */}
                {/* <Col
                  md={0}
                  xs={0}
                  className="d-none d-md-flex"
                  style={iconStyle}
                >
                  {props.has_invoice && <Icon name="paperclip" />}
                </Col> */}
                {/* <Col md={1} xs={1} style={iconStyle}>
                  <Icon
                    name="times"
                    onClick={(e) => {
                      e.stopPropagation();
                      deleteItem();
                    }}
                  />
                </Col> */}
              </Row>
              {/* <Row>
                <Col md={5} style={inventoryStyle}>
                  {props.inventory}
                </Col>
                <Col md={5} style={otherStyle}>
                  {props.brand}
                </Col>
              </Row> */}
              {/* <Row>
                <Col md={5} style={otherStyle}>
                  {props.location}
                </Col>
                <Col md={5} style={otherStyle}>
                  {props.acquisition_place}
                </Col>
              </Row> */}
              <Row>
                {/* <Col md={5} style={otherStyle}>
                  {props.group}
                </Col>
                <Col md={5} style={otherStyle}>
                  {props.identifier}
                </Col> */}
                
              </Row>
              {/* <Row>
                <Col md={12} style={noteStyle}>
                  {window.innerWidth < 500
                    ? props.note && props.note.length > 20
                      ? props.note.substr(0, 17) + "..."
                      : props.note
                    : props.note && props.note.length > 60
                    ? props.note.substr(0, 57) + "..."
                    : props.note}
                </Col>
              </Row> */}
            </Col>
          </Row>
        </Col>
      </Card.Body>
    </Card>
  );
};

export default InventoryItem;
