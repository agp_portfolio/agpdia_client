import React from "react";

import { Card, Col } from "react-bootstrap";
import { Form } from "tabler-react";
import currencyFormater from "../../helpers/currencyFormater";

const AccountRecord = ({ props, recordsCheckBoxHandler, showModalHandler }) => {
  const main_currency_symbol = sessionStorage.getItem("main_currency_symbol");
  const main_currency_format = sessionStorage.getItem("main_currency_format");
  
  const cardStyle = { 
    height:'60px',
    marginBottom: "0.1rem", 
    borderLeft: "5px solid " + props.account_color
  };
  const conceptStyle = {
    fontSize: "14px",
    display: "flex",
    alignItems: "center",
    //justifyContent: "center",
    fontWeight: "600",
    //textAlign: "left",
    padding: "0.75rem",
    wordBreak: "break-word",
    hyphens: "auto",
  };
  const accountStyle = {
    fontSize: "12px",
    display: "flex",
    alignItems: "center",
    //justifyContent: "center",
    //textAlign: "center",
    padding: "0.75rem",
  };
  const categoryStyle = {
    fontSize: "12px",
    display: "flex",
    alignItems: "center",
    //justifyContent: "center",
    textAlign: "center",
    padding: "0.75rem",
  };
  const noteStyle = {
    fontSize: "12px",
    display: "flex",
    alignItems: "center",
    padding: "0.75rem",
  };
  const amountStyle = {
    fontSize: "18px",
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0.75rem",
  };
  const handleChange = (e) => {
    recordsCheckBoxHandler(props.id, e.target.checked,props.transfer_oid);
  };
  const handleClick = (e) => {
    showModalHandler(props.id, false);
  };


  return (
    <Card style={cardStyle}>
      <Card.Body style={{ padding: "0px 10px", display: "flex" }}>
        <Col
          md={12}
          style={{ display: "flex", padding: "0px", cursor: "pointer" }}
        >
          <Col md={6} xs={6} style={conceptStyle}>
            {props.concept.length > 30 ? props.concept.substr(0,27) + "...":props.concept}
          </Col>
        
          {/* <Col md={1} className="d-none d-md-flex"></Col> */}
          <Col md={6} xs={6} style={amountStyle}>
            <div>
              <p style={{ marginBottom: "0px" }}>
                <strong
                  style={{
                    color:
                      props.amount > 0
                        ? "#5eba00"
                        : props.amount == 0
                        ? "#54c8ff"
                        : "#FF0000",
                  }}
                >
                  {currencyFormater(
                    props.amount,
                    props.currency_format,
                    props.currency_symbol
                  )}
                </strong>
              </p>
              {!props.has_main_currency && (
                <p style={{ marginBottom: "0px", fontSize: "12px" }}>
                  ~{" "}
                  {currencyFormater(
                    props.amount * props.exchange,
                    main_currency_format,
                    main_currency_symbol
                  )}
                </p>
              )}
            </div>
          </Col>
        </Col>
      </Card.Body>
    </Card>
  );
};

export default AccountRecord;
