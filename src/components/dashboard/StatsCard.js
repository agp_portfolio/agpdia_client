// @flow

import * as React from "react";
import cn from "classnames";

import { Card, Text, Header, Icon } from "tabler-react";

/**
 * Used for dispaying an individual statistic/number with 2 potential layouts
 */
function StatsCard({
  className,
  movement,
  total,
  label,
  layout = 1,
  chart,
  blured = false,
  percentageColor,
}) {
  const classes = cn(className);
  // const movementString = `${movement > 0 ? "+" : ""}${movement.toString().replace('.',',')}%`;
  const movementString = `${movement.toString().replace('.',',')}%`;
  
  var movementColor = !movement ? "#ffc107" : movement > 0 ? "#3bb53b" : "#ff3a3a";
  movementColor = percentageColor ? percentageColor : movementColor
  if (layout === 2) {
    return (
      <Card className={classes}>
        <div style={{height: '150px'}}>
          <Card.Body>
            <div className={`card-value float-right`} style={{color:movementColor}}>
              {movementString}
            </div>
            <h3 className="mb-1" style={{color: blured ? 'gray' : 'black'}}>{total}</h3>
            <Text muted>{label}</Text>
          </Card.Body>
          {chart && <div className="card-chart-bg">{chart}</div>}
        </div>
      </Card>
    );
  }

  return (
    <Card className={classes}>
      <div style={{height: '150px'}}>
        <Card.Body className="p-3 mt-4 mr-6 text-center">
          <Text color={movementColor} className="text-right">
            {movementString}
            <Icon
              name={
                !movement ? "minus" : movement > 0 ? "chevron-up" : "chevron-down"
              }
            />
          </Text>
          <h1 className="mb-0" style={{color: blured ? 'gray' : 'black'}}>{total}</h1>
          <Text color="muted" className=" mb-4">
            {label}
          </Text>
        </Card.Body>
      </div>
    </Card>
  );
}

StatsCard.displayName = "StatsCard";

export default StatsCard;
