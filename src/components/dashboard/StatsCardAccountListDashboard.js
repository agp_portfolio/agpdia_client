// @flow

import React from "react";
import {
  Page,
  Avatar,
  Icon,
  Grid,
  Text,
  Table,
  Alert,
  Progress,
  colors,
  Dropdown,
  StampCard,
  StatsCard,
  ProgressCard,
  Badge,
} from "tabler-react";
import currencyFormater from "../../helpers/currencyFormater";
import numberFormater from "../../helpers/numberFormater";
import C3Chart from "react-c3js";
import { Container, Row, Col, Card, Button, ModalTitle } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import StatsCardAccount from "./StatsCardAccountDashboard";
import fetchFunction from "../../helpers/fetchFuction";

function StatsCardCustom({ card }) {
  const history = useHistory();
  const [accountsData, setAccountsData] = React.useState();
  const [accounts, setAccounts] = React.useState();

  const handleClick = (e) => {
    if (window.innerWidth > 500) {
      history.push("/account/accounts/details/" + card.account_id);
    }
  };

  React.useEffect(() => {
    fetchAccounts();
  }, []);

  const fetchAccounts = () => {
    var filter = {};
    var [promise, abort] = fetchFunction(
      "accounts/aggs/dashboard/dashboardAccounts",
      "POST",
      filter
    );
    promise.then((data) => {
      if (!data) {
        setAccounts([]);
      } else {
        setAccountsData(data);
      }
    });
  };

  const makeAccountsArray = () => {
    var accountsTable = [];
    Object.keys(accountsData).forEach((account_id) => {
      var account = account_id.includes("total") ? false : account_id;
      var percentageColor =
        accountsData[account_id].label == "Total"
          ? "gray"
          : accountsData[account_id].label == "Disponible" ||
            !accountsData[account_id].available
          ? "#3b95f7"
          : null;
      accountsTable.push(
        <StatsCardAccount
          card={{
            available: accountsData[account_id].available,
            percentageColor: percentageColor,
            label: accountsData[account_id].label,
            data: accountsData[account_id].data,
            date: accountsData[account_id].date,
            color: accountsData[account_id].color,
            format: accountsData[account_id].format,
            symbol: accountsData[account_id].symbol,
            percentage:
              accountsData[account_id].available &&
              accountsData[account_id].label != "Disponible"
                ? accountsData[account_id].total_available_percentage
                : accountsData[account_id].total_percentage,
            variation_penultimate_month:
              accountsData[account_id].variation_penultimate_month,
            variation_antepenultimate_month:
              accountsData[account_id].variation_antepenultimate_month,
            account_id: account,
          }}
        />
      );
    });

    setAccounts(accountsTable);
  };

  React.useEffect(() => {
    if (!accountsData) return; //Dont create the array if there is no data

    makeAccountsArray();
  }, [accountsData]);

  return (
    <Row
      className="m-0"
      style={{ border: "1px solid rgba(0, 40, 100, 0.12)", overflow: "auto" }}
    >
      <br></br>
      {!accounts ? (
        <Col md={12} clasName="mt-2">
          <br></br>
          <h2>Loading...</h2>
        </Col>
      ) : accounts.length >= 3 ? (
        <Col md={12} clasName="mt-2" style={{ display: "flex", padding: "0px" }}>
          {accounts}
        </Col>
      ) : (
        <Col md={12} clasName="mt-2" style={{ padding: "0px" }}>
          <br></br>
          <h2 style={{ textAlign: "center" }}>Sin cuentas todavia</h2>
        </Col>
      )}
    </Row>
  );
}

export default StatsCardCustom;
