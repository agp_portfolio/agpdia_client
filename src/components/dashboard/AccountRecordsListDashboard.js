// @flow

import React from "react";
import { Redirect, useParams } from "react-router-dom";

import Infinite from "react-infinite";
import moment from "moment";
import ModalForm from "../ModalForm";
import { Container, Row, Col, Card, Button, ModalTitle } from "react-bootstrap";

//import { Card } from "tabler-react";
import fetchFunction from "../../helpers/fetchFuction";

import AccountRecordDashboard from "./AccountRecordDashboard";
import AccountRecordDate from "../accountRecords/AccountRecordDate";
import AccountRecordTotal from "../accountRecords/AccountRecordTotal";
import AccountRecordForm from "../accountRecords/AccountRecordForm";

import backButtonWithModal from "../../helpers/backButtonWithModal";
import { useHistory } from "react-router-dom";

function AccountRecordsList() {
  const history = useHistory();
  const [error, setError] = React.useState();
  const [modalTitle, setModalTitle] = React.useState("");
  const [records, setRecords] = React.useState(false);
  const [recordsData, setRecordsData] = React.useState();
  const [concepts, setConcepts] = React.useState();
  const [conceptsInfo, setConceptsInfo] = React.useState();
  const [accounts, setAccounts] = React.useState();
  const [vehicles, setVehicles] = React.useState();
  const [houses, setHouses] = React.useState();
  const [selectedItems, setSelectedItem] = React.useState([]);
  const [itemsToForm, setItemsToForm] = React.useState([]);
  const [allRecords, setAllRecords] = React.useState([]);
  const [prevProps, setPrevProps] = React.useState({});
  const [firstRender, setFirstRender] = React.useState(false);
  const [modalType, setModalType] = React.useState();

  var modalRef = React.useRef();

  React.useEffect(() => {
    fetchRecords();
  }, []);

  const fetchRecords = () => {
    var filter = {
      start_date: moment(new Date("2015-01-01")).toDate(),
      end_date: moment(),
    };
    setRecords(false);

    var [promise, abort] = fetchFunction(
      "accounts/aggs/records/list",
      "POST",
      filter
    );
    promise.then((data) => {
      if (!data) {
        setRecords({});
      } else {
        setRecordsData(data);
      }
    });
  };

  const makeRecordsArray = () => {
    var recordsTable = [];
    if (recordsData.daily) {
      recordsData.daily
        .filter((day) => moment(day.date) > moment().subtract(1, "month"))
        .forEach((day) => {
          recordsTable.push(
            <AccountRecordDate
              key={new Date(day.id).getTime()}
              props={{
                date: day.date,
                amount: day.dailyTotal,
              }}
            />
          );

          day.records.forEach((record) => {
            selectedItems.includes(record.id)
              ? (record.isChecked = true)
              : (record.isChecked = false);

            recordsTable.push(
              <AccountRecordDashboard
                key={record.id}
                props={record}
                recordsCheckBoxHandler={recordsCheckBoxHandler}
                // showModalHandler={showModalHandler}
              />
            );
          });
        });
    }
    setRecords(recordsTable);
  };
  // const dateHandler = (startDate, endDate) => {};
  // const recordsFilterHandler = (concepts, accounts, type, note) => {};

  const recordsCheckBoxHandler = (objectId, isChecked, relatedObjectId) => {
    if (isChecked) {
      var temp = [...selectedItems]; // Need to clone the object to prevent malfunction
      temp.push(objectId);
      if (relatedObjectId) temp.push(relatedObjectId);
      setSelectedItem(temp);
    } else {
      var temp = [...selectedItems]; // Need to clone the object to prevent malfunction
      temp.splice(temp.indexOf(objectId), 1);
      if (relatedObjectId) temp.splice(temp.indexOf(relatedObjectId), 1);
      setSelectedItem(temp);
    }
  };
  const handleClick = (e) => {
    if(window.innerWidth > 500 ){

      history.push("/account/records");
    }
  };

  React.useEffect(() => {
    if (!recordsData) return; //Dont create the array if there is no data

    makeRecordsArray();
  }, [recordsData]);

  return (
    <>
      <Card onClick={handleClick}>
        <Card.Body
          style={{
            padding: "0px 10px",
            background: "#f5f7fb",
            maxHeight: "500px",
            overflow: "auto",
          }}
        >
          {error && (
            <div className={"alert alert-" + error.type} role="alert">
              {error.message}
            </div>
          )}
          <Col lg={12} className="p-0">
            {!records ? (
              <>
                <br></br>
                <h2>Loading...</h2>
              </>
            ) : records.length ? (
              records
            ) : (
              <>
                <br></br>
                <h2 style={{ textAlign: "center" }}>
                  Sin registros el último mes
                </h2>
              </>
            )}
          </Col>
        </Card.Body>
      </Card>
    </>
  );
}

export default AccountRecordsList;
