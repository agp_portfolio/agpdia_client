// @flow

import React from "react";
import StatsCard from "./StatsCard";
import currencyFormater from "../../helpers/currencyFormater";
import numberFormater from "../../helpers/numberFormater";
import C3Chart from "react-c3js";
import { Container, Row, Col, Card, Button, ModalTitle } from "react-bootstrap";
import { useHistory } from "react-router-dom";

function StatsCardCustom({ card }) {
  const history = useHistory();
  const handleClick = (e) => {
    if (window.innerWidth > 500) {
      if (card.account_id) {
        history.push("/account/accounts/details/" + card.account_id);
      } else {
        history.push("/account/accounts/");
      }
    }
  };

  return (
    <Col
      xs={12}
      md={6}
      lg={4}
      onClick={handleClick}
      style={{ margin: "auto", marginTop: "20px" }}
    >
      <StatsCard
        layout={card && card.available ? 2 : 2}
        // blured={card && !card.available}
        percentageColor={card && card.percentageColor}
        movement={
          card && card.percentage
            ? Math.round((card.percentage + Number.EPSILON) * 100) / 100
            : 0
        }
        total={
          card &&
          currencyFormater(
            card.data[card.data.length - 1],
            card.format,
            card.symbol
          )
        }
        label={card && card.label}
        chart={
          <C3Chart
            style={{ height: "100%" }}
            padding={{
              bottom: -10,
              left: -1,
              right: -1,
            }}
            data={{
              names: {
                data1: card && card.label,
              },
              columns: card ? [["data1"].concat(card.data)] : [],
              type: "area",
            }}
            legend={{
              show: false,
            }}
            transition={{
              duration: 0,
            }}
            point={{
              show: false,
            }}
            tooltip={{
              format: {
                title: function (x) {
                  return card.date[x];
                },
                value: function (value, ratio, id) {
                  return currencyFormater(value, card.format, card.symbol);
                },
              },
            }}
            axis={{
              y: {
                padding: {
                  bottom: 0,
                },
                show: false,
                tick: {
                  outer: false,
                },
              },
              x: {
                padding: {
                  left: 0,
                  right: 0,
                },
                show: false,
              },
            }}
            color={{
              pattern: [card && card.color],
            }}
          />
        }
      />
    </Col>
  );
}

export default StatsCardCustom;
