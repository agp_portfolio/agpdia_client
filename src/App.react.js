import * as React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import {
  LoginPage,
  Error404,
  DashboardPage,
  AccountAccountsPage,
  AccountAccountsDetailsPage,
  AccountAnalyticsPage,
  AccountDashboardPage,
  AccountRecordsPage,
  ConfigurationDropdownsAccountingCategoriesPage,
  ConfigurationDropdownsAccountingConceptsPage,
  ConfigurationDropdownsHousesPage,
  ConfigurationDropdownsVehiclesPage,
  ConfigurationDropdownsInventoryLocationsPage,
  ConfigurationDropdownsInventoryGroupsPage,
  ConfigurationDropdownsInventoryInventoriesPage,
  ConfigurationDropdownsAccountAlertsPage,
  InventoryPage,
  ProfilePage,
  TravelPage,
} from "./pages";
import PrivateRoute from "./components/PrivateRoute";
// The order gives preference, at the bottom more important
import "bootstrap/dist/css/bootstrap.min.css";
import "tabler-react/dist/Tabler.css";
import "./App.css";

type Props = {||};

function App(props: Props): React.Node {
  return (
    <React.StrictMode>
      <Router>
        <Switch>
          <Route exact path="/login" component={LoginPage} />
          <Redirect exact from="/" to="/dashboard" />
          <PrivateRoute exact path="/dashboard" component={DashboardPage} />
          <PrivateRoute
            exact
            path="/account/dashboard"
            component={AccountDashboardPage}
          />
          <PrivateRoute
            exact
            path="/account/accounts"
            component={AccountAccountsPage}
          />
          <PrivateRoute
            exact
            path="/account/accounts/details/:id"
            component={AccountAccountsDetailsPage}
          />
          <PrivateRoute
            exact
            path="/account/records"
            component={AccountRecordsPage}
          />
          <PrivateRoute
            exact
            path="/account/analytics"
            component={AccountAnalyticsPage}
          />
          <PrivateRoute
            exact
            path="/inventory/dashboard"
            component={InventoryPage}
          />
          <PrivateRoute exact path="/travel/dashboard" component={TravelPage} />
          <PrivateRoute
            exact
            path="/configuration/dropdowns/accounting_categories"
            component={ConfigurationDropdownsAccountingCategoriesPage}
          />
          <PrivateRoute
            exact
            path="/configuration/dropdowns/accounting_concepts"
            component={ConfigurationDropdownsAccountingConceptsPage}
          />
          <PrivateRoute
            exact
            path="/configuration/dropdowns/houses"
            component={ConfigurationDropdownsHousesPage}
          />
          <PrivateRoute
            exact
            path="/configuration/dropdowns/vehicles"
            component={ConfigurationDropdownsVehiclesPage}
          />
          <PrivateRoute
            exact
            path="/configuration/dropdowns/inventory_locations"
            component={ConfigurationDropdownsInventoryLocationsPage}
          />
          <PrivateRoute
            exact
            path="/configuration/dropdowns/inventory_groups"
            component={ConfigurationDropdownsInventoryGroupsPage}
          />
          <PrivateRoute
            exact
            path="/configuration/dropdowns/inventory_inventories"
            component={ConfigurationDropdownsInventoryInventoriesPage}
          />
          <PrivateRoute
            exact
            path="/configuration/alerts/accounts"
            component={ConfigurationDropdownsAccountAlertsPage}
          />
          <PrivateRoute exact path="/profile" component={ProfilePage} />
          {/* <Route exact path="/register" component={RegisterPage} /> */}
          <Redirect to="/" />
          {/* <Route component={Error404} /> */}
        </Switch>
      </Router>
    </React.StrictMode>
  );
}

export default App;
