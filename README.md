This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Below you will find some information on how to perform common tasks.<br>
You can find the most recent version of this guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

This has as base [tabler-react](https://github.com/tabler/tabler-react)


## Scripts to deploy

# Deploy client aplication
 - Build the app with "npm run build" (in .env.production there are some customizable parameters, is important to use some for local and others for public deployment before the build)
 - Move the builded aplication to the desired location in the production server.
 - With "npm install -g serve" install in the final location server, in a nodejs enviroment the server nodedjs administrator (maybe as superuser)
 - Launch "serve -s PATH -l PORT -n" (With PATH as the absolute directory of the builded aplication, and with PORT as the port that is going to be listen)
 - You have it now.
